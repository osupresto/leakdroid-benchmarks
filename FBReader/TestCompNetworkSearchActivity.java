/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompNetworkSearchActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompNetworkSearchActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.NetworkSearchActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.NetworkSearchActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.NetworkSearchActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.NetworkSearchActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.NetworkSearchActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.NetworkSearchActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkSearchActivity => org.geometerplus.android.fbreader.network.NetworkSearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkSearchActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
