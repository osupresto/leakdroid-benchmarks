/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompPreferenceActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompPreferenceActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Settings");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.PreferenceActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Settings");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.PreferenceActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Settings");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.PreferenceActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Settings");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.PreferenceActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Settings");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.PreferenceActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Settings");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.PreferenceActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
