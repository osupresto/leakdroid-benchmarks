/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.geometerplus.zlibrary.ui.android.test;

import pai.AmplifyTestCase;
import pai.AmplifyUtils;

public class MainTestCase extends AmplifyTestCase {
	public MainTestCase() {
		super("org.geometerplus.zlibrary.ui.android", "org.geometerplus.android.fbreader.FBReader");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.sleep(2000);
	}
	
	public void testFBReader() {
		solo.assertCurrentActivity(getName(), "FBReader");
	}
	
	// NO HOME
	public void testLibraryActivity() {
		solo.clickOnMenuItem("Library");
		solo.sleep(1500);
//		solo.assertCurrentActivity(getName(), "LibraryActivity");
	}

	// NO HOME
	public void testBookInfoActivity() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Book info");
		solo.sleep(1500);
//		solo.assertCurrentActivity(getName(), "BookInfoActivity");
	}
	
	// NO HOME
	public void testEditBookInfoActivity() {
		AmplifyUtils.replay(solo, "/data/presto/EditBookInfoActivity_event", 6000);
		solo.sleep(1500);
	}
	
	// NO HOME
	public void testNetworkLibraryActivity() {
		solo.clickOnMenuItem("Network library");
		solo.sleep(1500);
	}
	
	// NO HOME
	public void testNetworkBookInfoActivity() {
		AmplifyUtils.replay(solo, "/data/presto/NetworkBookInfoActivity_event", 45000);
		solo.sleep(1500);
	}
	
	// NO HOME
	public void testTOCActivity() {
		solo.clickOnMenuItem("TOC");
		solo.sleep(1500);
	}
	
	// NO HOME
	public void testBookmarksActivity() {
		solo.clickOnMenuItem("Bookmarks");
		solo.sleep(1500);
	}
	
	// NO HOME
	public void testPreferenceActivity() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Settings");
		solo.sleep(3000);
	}
	
	// NO HOME
	public void testAddCustomCatalogActivity() {
		AmplifyUtils.replay(solo, "/data/presto/AddCustomCatalogActivity_event", 8000);
		solo.sleep(1500);
	}
}
