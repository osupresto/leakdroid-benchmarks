/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.geometerplus.zlibrary.ui.android.test;

import com.jayway.android.robotium.solo.Solo;

import android.view.KeyEvent;
import pai.AmplifyTestCase;
import pai.AmplifyUtils;
import pai.GenericFunctor;

public class SemanticTestCase extends AmplifyTestCase {
	public SemanticTestCase() {
		super("org.geometerplus.zlibrary.ui.android", "org.geometerplus.android.fbreader.FBReader");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.sleep(4000);
	}
	
	public void testNavigate() {
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/Navigate_event", 5000);
				solo.sleep(3000);
			}
		});
	}
	
	public void testSearchAndHide() {
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "data/presto/SearchAndHide_event", 8000);
			}
		});
	}
	
	public void testSearchAndBrowse() {
		solo.sendKey(KeyEvent.KEYCODE_N);
		solo.sendKey(KeyEvent.KEYCODE_E);
		solo.sendKey(KeyEvent.KEYCODE_W);
		solo.sendKey(Solo.ENTER);
		solo.sendKey(Solo.ENTER);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "data/presto/SearchAndBrowse_event", 5000);
			}
		});
	}
	
	public void testLibrarySearch() {
		solo.clickOnMenuItem("Library");
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/LibrarySearch_event", 8000);
			}
		});
	}
	
	public void testToggleFavorite() {
		AmplifyUtils.replay(solo, "/data/presto/ToggleFavorite_INIT_event", 3000);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/ToggleFavorite_event", 8000);
			}
		});
	}
	
	public void testBookInfoReload() {
	    solo.clickOnMenuItem("More");
		solo.clickOnText("Book info");
		solo.sleep(2000);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/BookInfo_event", 4000);
			}
		});
	}
	
	public void testCatalogReload() {
		solo.clickOnMenuItem("Network library");
		solo.sleep(5000);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/CatalogReload_event", 15000);
			}
		});
	}
	
	public void testNightDay() {
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				solo.clickOnMenuItem("Night");
				solo.sleep(1500);
				solo.clickOnMenuItem("Day");
				solo.sleep(1500);
			}
		});
	}
	
	public void testZooming() {
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				solo.clickOnMenuItem("More");
				solo.clickOnText("Zoom in");
				solo.sleep(1500);
				solo.clickOnMenuItem("More");
				solo.clickOnText("Zoom out");
				solo.sleep(1500);
			}
		});
	}
}
