/* 
 * BackButtonTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.geometerplus.zlibrary.ui.android.test;

import pai.AmplifyTestCase;
import pai.AmplifyUtils;
import pai.GenericFunctor;

public class BackButtonTestCase extends AmplifyTestCase {
	public BackButtonTestCase() {
		super("org.geometerplus.zlibrary.ui.android", "org.geometerplus.android.fbreader.FBReader");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.sleep(4000);
	}
	
	public void testFBReader() {
		solo.assertCurrentActivity(getName(), "FBReader");
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		      public void doIt(Object arg) {
		        AmplifyUtils.leaveAndBack(solo);
		        solo.assertCurrentActivity(getName(), "FBReader");
		      }
		});
	}

	public void testLibraryActivity() {
		solo.clickOnMenuItem("Library");
		solo.sleep(1500);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		      public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Library");
				solo.sleep(1500);
			}
		});
	}
	
	public void testBookInfoActivity() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Book info");
		solo.sleep(1500);
		
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("More");
				solo.clickOnText("Book info");
				solo.sleep(1500);
			}
		});
	}
	
	public void testEditBookInfoActivity() {
		AmplifyUtils.replay(solo, "/data/presto/EditBookInfoActivity_event", 6000);
		solo.sleep(1500);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				
				AmplifyUtils.replay(solo, "/data/presto/EditBookInfoActivity_BACK_event", 2000);
				solo.sleep(1000);
			}
		});
	}
	
	public void testNetworkLibraryActivity() {
		solo.clickOnMenuItem("Network library");
		solo.sleep(3000);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/NetworkLibraryActivity_BACK_event", 3000);
				solo.sleep(5000);
			}
		});
	}
	
	public void testTOCActivity() {
		solo.clickOnMenuItem("TOC");
		solo.sleep(1500);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("TOC");
				solo.sleep(1500);
			}
		});
	}
	
	public void testBookmarksActivity() {
		solo.clickOnMenuItem("Bookmarks");
		solo.sleep(1500);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/Bookmarks_BACK_event", 3000);
				solo.sleep(4500);
			}
		});
	}
	
	public void testPreferenceActivity() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Settings");
		solo.sleep(3000);
		
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("More");
				solo.clickOnText("Settings");
				solo.sleep(3000);
			}
		});
	}
	
	public void testAddCustomCatalogActivity() {
		AmplifyUtils.replay(solo, "/data/presto/AddCustomCatalogActivity_event", 8000);
		solo.sleep(1500);
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/AddCustomCatalogActivity_BACK_event", 2000);
				solo.sleep(1500);
			}
		});
	}
	
	public void testNetworkBookInfoActivity() {
		AmplifyUtils.replay(solo, "/data/presto/NetworkBookInfoActivity_event", 45000);
		solo.sleep(1500);
		
		specifyAmplifyFunctor(new GenericFunctor() {
			@Override
		    public void doIt(Object arg) {
				AmplifyUtils.replay(solo, "/data/presto/NetworkBookInfoActivity_BACK_event", 3000);
				solo.sleep(1500);
			}
		});
	}	
}
