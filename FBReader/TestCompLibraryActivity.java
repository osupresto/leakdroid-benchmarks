/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompLibraryActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompLibraryActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Library");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.library.LibraryActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
