#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript FBReader org.geometerplus.zlibrary.ui.android org.geometerplus.zlibrary.ui.android.test MainTestCase testFBReader,testLibraryActivity,testBookInfoActivity,testEditBookInfoActivity,testNetworkLibraryActivity,testNetworkBookInfoActivity,testTOCActivity,testBookmarksActivity,testPreferenceActivity,testAddCustomCatalogActivity rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript FBReader org.geometerplus.zlibrary.ui.android org.geometerplus.zlibrary.ui.android.test MainTestCase testFBReader home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript FBReader org.geometerplus.zlibrary.ui.android org.geometerplus.zlibrary.ui.android.test MainTestCase testFBReader,testLibraryActivity,testBookInfoActivity,testEditBookInfoActivity,testNetworkLibraryActivity,testNetworkBookInfoActivity,testTOCActivity,testBookmarksActivity,testPreferenceActivity,testAddCustomCatalogActivity power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript FBReader org.geometerplus.zlibrary.ui.android org.geometerplus.zlibrary.ui.android.test BackButtonTestCase testFBReader,testLibraryActivity,testBookInfoActivity,testEditBookInfoActivity,testNetworkLibraryActivity,testTOCActivity,testBookmarksActivity,testPreferenceActivity,testAddCustomCatalogActivity,testNetworkBookInfoActivity back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript FBReader org.geometerplus.zlibrary.ui.android org.geometerplus.zlibrary.ui.android.test SemanticTestCase testNavigate,testSearchAndHide,testSearchAndBrowse,testLibrarySearch,testToggleFavorite,testBookInfoReload,testCatalogReload,testNightDay,testZooming semantic.$N.action

