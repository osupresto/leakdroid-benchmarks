/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompImageViewActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompImageViewActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.image.ImageViewActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.image.ImageViewActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.image.ImageViewActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.image.ImageViewActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.image.ImageViewActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.image.ImageViewActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
