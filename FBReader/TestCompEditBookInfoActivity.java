/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompEditBookInfoActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompEditBookInfoActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Book info");
    solo.sleep(2000);
    solo.clickOnButton("Edit");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.EditBookInfoActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Book info");
    solo.sleep(2000);
    solo.clickOnButton("Edit");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.EditBookInfoActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Book info");
    solo.sleep(2000);
    solo.clickOnButton("Edit");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.EditBookInfoActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Book info");
    solo.sleep(2000);
    solo.clickOnButton("Edit");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.EditBookInfoActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Book info");
    solo.sleep(2000);
    solo.clickOnButton("Edit");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.EditBookInfoActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("More");
    solo.clickOnText("Book info");
    solo.sleep(2000);
    solo.clickOnButton("Edit");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.preferences.EditBookInfoActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
        // org.geometerplus.android.fbreader.preferences.EditBookInfoActivity => org.geometerplus.android.fbreader.preferences.EditBookInfoActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.preferences.EditBookInfoActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
