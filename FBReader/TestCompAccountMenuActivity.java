/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompAccountMenuActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompAccountMenuActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AccountMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
