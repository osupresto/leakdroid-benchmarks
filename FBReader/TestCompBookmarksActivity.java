/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompBookmarksActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompBookmarksActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
