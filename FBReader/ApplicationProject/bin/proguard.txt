# view AndroidManifest.xml #generated:95
-keep class org.geometerplus.android.fbreader.BookmarkEditActivity { <init>(...); }

# view AndroidManifest.xml #generated:89
-keep class org.geometerplus.android.fbreader.BookmarksActivity { <init>(...); }

# view AndroidManifest.xml #generated:75
-keep class org.geometerplus.android.fbreader.CancelActivity { <init>(...); }

# view AndroidManifest.xml #generated:23
-keep class org.geometerplus.android.fbreader.FBReader { <init>(...); }

# view AndroidManifest.xml #generated:88
-keep class org.geometerplus.android.fbreader.TOCActivity { <init>(...); }

# view AndroidManifest.xml #generated:17
-keep class org.geometerplus.android.fbreader.api.ApiService { <init>(...); }

# view AndroidManifest.xml #generated:10
-keep class org.geometerplus.android.fbreader.crash.FixBooksDirectoryActivity { <init>(...); }

# view AndroidManifest.xml #generated:76
-keep class org.geometerplus.android.fbreader.image.ImageViewActivity { <init>(...); }

# view AndroidManifest.xml #generated:77
-keep class org.geometerplus.android.fbreader.library.BookInfoActivity { <init>(...); }

# view AndroidManifest.xml #generated:78
-keep class org.geometerplus.android.fbreader.library.KillerCallback { <init>(...); }

# view AndroidManifest.xml #generated:85
-keep class org.geometerplus.android.fbreader.library.LibraryActivity { <init>(...); }

# view AndroidManifest.xml #generated:79
-keep class org.geometerplus.android.fbreader.library.LibrarySearchActivity { <init>(...); }

# view AndroidManifest.xml #generated:162
-keep class org.geometerplus.android.fbreader.network.AccountMenuActivity { <init>(...); }

# view AndroidManifest.xml #generated:154
-keep class org.geometerplus.android.fbreader.network.AddCustomCatalogActivity { <init>(...); }

# view AndroidManifest.xml #generated:152
-keep class org.geometerplus.android.fbreader.network.AuthenticationActivity { <init>(...); }

# view AndroidManifest.xml #generated:98
-keep class org.geometerplus.android.fbreader.network.BookDownloader { <init>(...); }

# view AndroidManifest.xml #generated:136
-keep class org.geometerplus.android.fbreader.network.BookDownloaderService { <init>(...); }

# view AndroidManifest.xml #generated:153
-keep class org.geometerplus.android.fbreader.network.BuyBooksActivity { <init>(...); }

# view AndroidManifest.xml #generated:171
-keep class org.geometerplus.android.fbreader.network.ListenerCallback { <init>(...); }

# view AndroidManifest.xml #generated:164
-keep class org.geometerplus.android.fbreader.network.NetworkBookInfoActivity { <init>(...); }

# view AndroidManifest.xml #generated:143
-keep class org.geometerplus.android.fbreader.network.NetworkLibraryActivity { <init>(...); }

# view AndroidManifest.xml #generated:137
-keep class org.geometerplus.android.fbreader.network.NetworkSearchActivity { <init>(...); }

# view AndroidManifest.xml #generated:163
-keep class org.geometerplus.android.fbreader.network.TopupMenuActivity { <init>(...); }

# view AndroidManifest.xml #generated:97
-keep class org.geometerplus.android.fbreader.preferences.EditBookInfoActivity { <init>(...); }

# view AndroidManifest.xml #generated:96
-keep class org.geometerplus.android.fbreader.preferences.PreferenceActivity { <init>(...); }

# view AndroidManifest.xml #generated:9
-keep class org.geometerplus.zlibrary.ui.android.library.BugReportActivity { <init>(...); }

# view AndroidManifest.xml #generated:8
-keep class org.geometerplus.zlibrary.ui.android.library.ZLAndroidApplication { <init>(...); }

# view res/layout/main.xml #generated:8
-keep class org.geometerplus.zlibrary.ui.android.view.ZLAndroidWidget { <init>(...); }

