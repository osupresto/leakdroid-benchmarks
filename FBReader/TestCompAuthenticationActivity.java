/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.view.View;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompAuthenticationActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompAuthenticationActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
