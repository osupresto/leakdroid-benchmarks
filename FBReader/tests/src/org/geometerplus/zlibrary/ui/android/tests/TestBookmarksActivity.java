/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.widget.LinearLayout;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBookmarksActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestBookmarksActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.BookmarksActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.BookmarksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_1 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.BookmarksActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.geometerplus.android.fbreader.BookmarksActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(4000);
    solo.sendKey(Solo.MENU);
    solo.clickOnMenuItem("Bookmarks");
    assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarksActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarksActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.geometerplus.android.fbreader.BookmarksActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
