/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.widget.LinearLayout;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCancelActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCancelActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.CancelActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.CancelActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.CancelActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.BookmarksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.TOCActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.TOCActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_1 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_1 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.CancelActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.CancelActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
