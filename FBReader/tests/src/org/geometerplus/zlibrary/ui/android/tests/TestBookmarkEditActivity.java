/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBookmarkEditActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestBookmarkEditActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarkEditActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarkEditActivity => org.geometerplus.android.fbreader.BookmarkEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarkEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarkEditActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarkEditActivity => org.geometerplus.android.fbreader.BookmarkEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarkEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarkEditActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.BookmarkEditActivity => org.geometerplus.android.fbreader.BookmarkEditActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarkEditActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
