/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.widget.LinearLayout;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestAuthenticationActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestAuthenticationActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.BookmarksActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.BookmarksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.CancelActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_2.isEnabled());
        solo.clickLongOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.FBReader
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.TOCActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_open);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_edit);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.book_info_button_reload);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00237() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00238() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00239() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00241() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00242() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00243() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00244() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00245() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00246() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00247() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00248() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00249() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00250() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00251() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00252() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00253() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00254() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00255() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00256() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00257() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00258() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00259() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00260() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00261() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00262() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00263() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00264() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00265() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00266() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00267() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00268() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00269() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00270() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00271() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00272() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00273() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00274() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00275() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AccountMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AccountMenuActivity.class);
        // org.geometerplus.android.fbreader.network.AccountMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00276() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00277() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00278() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00279() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00280() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00281() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00282() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00283() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00284() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00285() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00286() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00287() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00288() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00289() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00290() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00291() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00292() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00293() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00294() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00295() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00296() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00297() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00298() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00299() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00300() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00301() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00302() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00303() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00304() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00305() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00306() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00307() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00308() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00309() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00310() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00311() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00312() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00313() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00314() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00315() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00316() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00317() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00318() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00319() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00320() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00321() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00322() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00323() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00324() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00325() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00326() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00327() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00328() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00329() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00330() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00331() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00332() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00333() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00334() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00335() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00336() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00337() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00338() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00339() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00340() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00341() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00342() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00343() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00344() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00345() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00346() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00347() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00348() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00349() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00350() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00351() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00352() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00353() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00354() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00355() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00356() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00357() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_2 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_3 = VIEWS_2.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00358() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00359() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00360() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00361() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00362() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00363() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00364() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00365() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00366() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00367() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00368() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00369() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00370() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_1 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00371() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_1 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00372() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_1 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00373() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button2);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00374() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button3);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00375() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button0);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00376() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_1 = solo.getView(R.id.network_book_button1);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00377() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        ArrayList<LinearLayout> VIEWS_1 = solo.getCurrentViews(LinearLayout.class);
        LinearLayout VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00378() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00379() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00380() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00381() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00382() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00383() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00384() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00385() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00386() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00387() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00388() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00389() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00390() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00391() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00392() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00393() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00394() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00395() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00396() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00397() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00398() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00399() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00400() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00401() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00402() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00403() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00404() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00405() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00406() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00407() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00408() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00409() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00410() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00411() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00412() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00413() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00414() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00415() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00416() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00417() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00418() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00419() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00420() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00421() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00422() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00423() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00424() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00425() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00426() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00427() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00428() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00429() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00430() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00431() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00432() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00433() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00434() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00435() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00436() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00437() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00438() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00439() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00440() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00441() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00442() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00443() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00444() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00445() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00446() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00447() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00448() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00449() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00450() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00451() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        final View v_1 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00452() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00453() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00454() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00455() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00456() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00457() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00458() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00459() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00460() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00461() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00462() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00463() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00464() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00465() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00466() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00467() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00468() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00469() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00470() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00471() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00472() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00473() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00474() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00475() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00476() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00477() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00478() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00479() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00480() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00481() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00482() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00483() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00484() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00485() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00486() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00487() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00488() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00489() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00490() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00491() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00492() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00493() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00494() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00495() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00496() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00497() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00498() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00499() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00500() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00501() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00502() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00503() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00504() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00505() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00506() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00507() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00508() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00509() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00510() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00511() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00512() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00513() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00514() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00515() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00516() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00517() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00518() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00519() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00520() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00521() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00522() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00523() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00524() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00525() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00526() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00527() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00528() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00529() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00530() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00531() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00532() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00533() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00534() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00535() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00536() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00537() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00538() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00539() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00540() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00541() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00542() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00543() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00544() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00545() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00546() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00547() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00548() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00549() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00550() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00551() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00552() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00553() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00554() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00555() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00556() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00557() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00558() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00559() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00560() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00561() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00562() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00563() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00564() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00565() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00566() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00567() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00568() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00569() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        final View v_2 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00570() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }

  public void testNeutralCycle00571() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.AuthenticationActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
