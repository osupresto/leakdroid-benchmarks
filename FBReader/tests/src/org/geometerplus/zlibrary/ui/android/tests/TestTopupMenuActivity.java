/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestTopupMenuActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestTopupMenuActivity() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 1
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.BookmarksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.BookmarksActivity.class);
        // org.geometerplus.android.fbreader.BookmarksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.CancelActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.CancelActivity.class);
        // org.geometerplus.android.fbreader.CancelActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.TOCActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.TOCActivity.class);
        // org.geometerplus.android.fbreader.TOCActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.image.ImageViewActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.image.ImageViewActivity.class);
        // org.geometerplus.android.fbreader.image.ImageViewActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.BookInfoActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.BookInfoActivity.class);
        // org.geometerplus.android.fbreader.library.BookInfoActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.library.LibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.library.LibraryActivity.class);
        // org.geometerplus.android.fbreader.library.LibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.cancel_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.AuthenticationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.AuthenticationActivity.class);
        // org.geometerplus.android.fbreader.network.AuthenticationActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.BuyBooksActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.BuyBooksActivity.class);
        // org.geometerplus.android.fbreader.network.BuyBooksActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_1 = solo.getView(R.id.ok_button);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.NetworkBookInfoActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.NetworkBookInfoActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkBookInfoActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.NetworkLibraryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.NetworkLibraryActivity.class);
        // org.geometerplus.android.fbreader.network.NetworkLibraryActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => org.geometerplus.android.fbreader.preferences.PreferenceActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.preferences.PreferenceActivity.class);
        // org.geometerplus.android.fbreader.preferences.PreferenceActivity => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.goBack();
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        final View v_4 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(org.geometerplus.android.fbreader.network.TopupMenuActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.network.TopupMenuActivity, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.network.TopupMenuActivity => android.app.AlertDialog
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertDialog();
        // android.app.AlertDialog => org.geometerplus.android.fbreader.network.TopupMenuActivity
        solo.waitForDialogToClose(); // CANCEL DIALOG
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
