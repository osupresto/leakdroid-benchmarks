/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.geometerplus.zlibrary.ui.android.R;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompFBReader extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestCompFBReader() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(0x102001b);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, org.geometerplus.android.fbreader.FBReader.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        final View v_1 = solo.getView(R.id.main_view);
        assertTrue("ZLAndroidWidget: Not Enabled", v_1.isEnabled());
        solo.clickLongOnView(v_1); 
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // Priority: 0
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        Util.rotateOnce(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
        // org.geometerplus.android.fbreader.FBReader => org.geometerplus.android.fbreader.FBReader
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
