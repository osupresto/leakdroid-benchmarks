/*
 * This file is automatically created by Gator.
 */

package org.geometerplus.zlibrary.ui.android.tests;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestLeaks extends AmplifyTestCase {

  public TestLeaks() {
    super(org.geometerplus.android.fbreader.FBReader.class);
  }

  public void testNC00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.BookmarkEditActivity, of length 1
        // org.geometerplus.android.fbreader.BookmarkEditActivity => org.geometerplus.android.fbreader.BookmarkEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.geometerplus.android.fbreader.BookmarkEditActivity.class);
      }
    });
  }

  public void testNC02829() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(org.geometerplus.android.fbreader.FBReader.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.geometerplus.android.fbreader.FBReader, of length 2
        // org.geometerplus.android.fbreader.FBReader => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.geometerplus.android.fbreader.FBReader
        // Implicit Launch. BenchmarkName: FBReader
        assertActivity(org.geometerplus.android.fbreader.FBReader.class);
      }
    });
  }

  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    // assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    // assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }
}