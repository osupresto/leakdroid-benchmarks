/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.vudroid.test;

import pai.AmplifyTestCase;

public class MainTestCase extends AmplifyTestCase {
	private static final String PDF_FILE = "test.pdf";
	private static final String DJVU_FILE = "superhero.djvu";

	public MainTestCase() throws ClassNotFoundException {
		super("org.vudroid", "org.vudroid.core.MainBrowserActivity");
	}
	
	public void testMainBrowserActivity() {
		solo.assertCurrentActivity(getName(), "MainBrowserActivity");
	}
	
	public void testPdfViewerActivity() {
		setRotateDelay(3000);
		solo.clickOnText(PDF_FILE);
		solo.assertCurrentActivity(getName(), "PdfViewerActivity");
	}
	
	public void testDjvuViewerActivity() {
		setRotateDelay(5000);
		solo.clickOnText(DJVU_FILE);
		solo.assertCurrentActivity(getName(), "DjvuViewerActivity");
	}	
}
