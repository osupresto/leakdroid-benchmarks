/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.vudroid.test;

import android.view.KeyEvent;
import pai.AmplifyTestCase;
import pai.GenericFunctor;

public class SemanticTestCase extends AmplifyTestCase {
	private static final String PDF_FILE = "test.pdf";
	private static final String DJVU_FILE = "superhero.djvu";

	public SemanticTestCase() throws ClassNotFoundException {
		super("org.vudroid.core", "org.vudroid.core.MainBrowserActivity");
	}
	
	public void testNavigate() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("LOST.DIR");
				solo.waitForIdleSync();
				solo.clickInList(0);
				solo.waitForIdleSync();
			}
		});
	}
	
	public void testBrowseRecent() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("Recent");
				solo.waitForIdleSync();
				solo.clickOnText("Browse");
				solo.waitForIdleSync();
			}
		});
	}

	public void testPdfZooming() {
		solo.clickOnText(PDF_FILE);
		solo.sleep(5000);
		solo.waitForIdleSync();

		final int x = 120;
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.drag(x, x-5, 750, 750, 1);
				solo.waitForIdleSync();
				solo.sleep(1500);
				
				solo.drag(x, x+5, 750, 750, 1);
				solo.waitForIdleSync();
				solo.sleep(1500);
			}
		});
	}
	
	public void testDjvuZooming() {
		solo.clickOnText(DJVU_FILE);
		solo.sleep(5000);
		solo.waitForIdleSync();
		
		final int x = 120;
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.drag(x, x-5, 750, 750, 1);
				solo.waitForIdleSync();
				solo.sleep(1500);
				
				solo.drag(x, x+5, 750, 750, 1);
				solo.waitForIdleSync();
				solo.sleep(1500);
			}
		});
	}
	
	public void testPdfGotoPage() {
		solo.clickOnText(PDF_FILE);
		solo.sleep(5000);
		solo.waitForIdleSync();

		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Go to page");
				solo.clearEditText(0);
				solo.sendKey(KeyEvent.KEYCODE_5);
				solo.sendKey(KeyEvent.KEYCODE_ENTER);
				solo.waitForIdleSync();
				solo.sleep(5000);
				
				solo.clickOnMenuItem("Go to page");
				solo.clearEditText(0);
				solo.sendKey(KeyEvent.KEYCODE_2);
				solo.sendKey(KeyEvent.KEYCODE_ENTER);
				solo.waitForIdleSync();
				solo.sleep(5000);
			}
		});
	}
	
	public void testDjvuGotoPage() {
		solo.clickOnText(DJVU_FILE);
		solo.sleep(5000);
		solo.waitForIdleSync();
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Go to page");
				solo.clearEditText(0);
				solo.sendKey(KeyEvent.KEYCODE_5);
				solo.sendKey(KeyEvent.KEYCODE_ENTER);
				solo.waitForIdleSync();
				solo.sleep(5000);
				
				solo.clickOnMenuItem("Go to page");
				solo.clearEditText(0);
				solo.sendKey(KeyEvent.KEYCODE_2);
				solo.sendKey(KeyEvent.KEYCODE_ENTER);
				solo.waitForIdleSync();
				solo.sleep(5000);
			}
		});
	}
	
	public void testPdfToggleFullscreen() {
		solo.clickOnText(PDF_FILE);
		solo.sleep(5000);
		solo.waitForIdleSync();
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Full screen o*");
				solo.sleep(5000);
				solo.clickOnMenuItem("Full screen o*");
				solo.sleep(5000);
			}
		});
	}
	
	public void testDjvuToggleFullscreen() {
		solo.clickOnText(DJVU_FILE);
		solo.sleep(5000);
		solo.waitForIdleSync();
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Full screen o*");
				solo.sleep(5000);
				solo.clickOnMenuItem("Full screen o*");
				solo.sleep(5000);
			}
		});
	}
}
