/* 
 * BackButtonTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.vudroid.test;

import android.os.Debug;
import pai.AmplifyTestCase;
import pai.AmplifyUtils;
import pai.GenericFunctor;

public class BackButtonTestCase extends AmplifyTestCase {
	private static final String PDF_FILE = "test.pdf";
	private static final String DJVU_FILE = "superhero.djvu";

	public BackButtonTestCase() throws ClassNotFoundException {
		super("org.vudroid.core", "org.vudroid.core.MainBrowserActivity");
	}
	
	public void testMainBrowserActivity() {
		solo.assertCurrentActivity(getName(), "MainBrowserActivity");
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				AmplifyUtils.leaveAndBack(solo);
			}
		});
	}
	
	public void testPdfViewerActivity() {
		solo.clickOnText(PDF_FILE);
		solo.assertCurrentActivity(getName(), "PdfViewerActivity");
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.sleep(5000);
				solo.goBack();
				solo.clickOnText(PDF_FILE);
				solo.assertCurrentActivity(getName(), "PdfViewerActivity");
			}
		});
	}
	
	public void tearDown() throws Exception {
		Debug.stopMethodTracing();
		super.tearDown();
	}
	
	public void testDjvuViewerActivity() {
		solo.clickOnText(DJVU_FILE);
		solo.assertCurrentActivity(getName(), "DjvuViewerActivity");
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.sleep(6000);
				solo.waitForIdleSync();
				solo.assertCurrentActivity(getName(), "DjvuViewerActivity");
				solo.goBack();
				solo.clickOnText(DJVU_FILE);
			}
		});
	}
}
