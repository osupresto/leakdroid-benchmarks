#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript VuDroid org.vudroid org.vudroid.test MainTestCase testMainBrowserActivity,testPdfViewerActivity,testDjvuViewerActivity rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript VuDroid org.vudroid org.vudroid.test MainTestCase testMainBrowserActivity,testPdfViewerActivity,testDjvuViewerActivity home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript VuDroid org.vudroid org.vudroid.test MainTestCase testMainBrowserActivity,testPdfViewerActivity,testDjvuViewerActivity power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript VuDroid org.vudroid org.vudroid.test BackButtonTestCase testMainBrowserActivity,testPdfViewerActivity,testDjvuViewerActivity back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript VuDroid org.vudroid org.vudroid.test SemanticTestCase testNavigate,testBrowseRecent,testPdfZooming,testDjvuZooming,testPdfGotoPage,testDjvuGotoPage,testPdfToggleFullscreen,testDjvuToggleFullscreen semantic.$N.action

