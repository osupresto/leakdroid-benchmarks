/*
 * This file is automatically created by Gator.
 */

package org.vudroid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestLeaks extends AmplifyTestCase {

  int gotoPage = 0;
  private final String PDF = "test.pdf";
  private final String DJVU = "superhero.djvu";

  public TestLeaks() {
    super(org.vudroid.core.MainBrowserActivity.class);
  }

  public void testNeutralCycle00028() throws Exception {

    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {

    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {

    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clearEditText(0);
        solo.typeText(0, String.valueOf(((gotoPage++)%10)+1));
        solo.clickOnButton("Go to Page!");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  /*
   * ============================== Helpers ==============================
   */
  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
  /*
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
    */
  }

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(2000);
    /*
    assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());
    */
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    /*
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
    */
  }
}