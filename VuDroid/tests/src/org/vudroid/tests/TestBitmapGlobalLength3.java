/*
 * This file is automatically created by Gator.
 */

package org.vudroid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBitmapGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 5;
  private final int DJVU_IDX = 4;
  private final int DIR_IDX = 1;
  private final String GotoPageNum = "1";
  private final String PDF = "test.pdf";
  private final String DJVU = "superhero.djvu";
  public TestBitmapGlobalLength3() {
    super(org.vudroid.core.MainBrowserActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // select a DJVU file in RECENT list
        solo.clickOnText(DJVU); // click on a DJVU file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // select a PDF file in RECENT list
        solo.clickOnText(PDF); // click on a PDF file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // select a DJVU file in RECENT list
        solo.clickOnText(DJVU); // click on a DJVU file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // select a PDF file in RECENT list
        solo.clickOnText(PDF); // click on a PDF file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // select a DJVU file in RECENT list
        solo.clickOnText(DJVU); // click on a DJVU file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // select a PDF file in RECENT list
        solo.clickOnText(PDF); // click on a PDF file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // select a DJVU file in RECENT list
        solo.clickOnText(DJVU); // click on a DJVU file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // select a DJVU file in RECENT list
        solo.clickOnText(DJVU); // click on a DJVU file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // select a PDF file in RECENT list
        solo.clickOnText(PDF); // click on a PDF file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // select a PDF file in RECENT list
        solo.clickOnText(PDF); // click on a PDF file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // select a DJVU file in RECENT list
        solo.clickOnText(DJVU); // click on a DJVU file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // select a DJVU file in RECENT list
        solo.clickOnText(DJVU); // click on a DJVU file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // select a PDF file in RECENT list
        solo.clickOnText(PDF); // click on a PDF file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // select a PDF file in RECENT list
        solo.clickOnText(PDF); // click on a PDF file
        solo.goBack(); // go back
        solo.clickOnText("Recent"); // go to recent opened list
        solo.clickInList(1, 0); // click on the first item
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  public View getActionBarView() {
    // solo.sleep(2000);
    ArrayList<View> alViews = solo.getCurrentViews();
    for (View curView :alViews) {
      String className = curView.getClass().getName();
      if (className.endsWith("ActionBarContainer")) {
        return curView;
      }
    }
    return null;
  }

  private ArrayList<View> getActionBarItemsWithMenuButton() {
    ViewGroup ActionBarContainer = (ViewGroup) this.getActionBarView();
    ArrayList<View> ret = new ArrayList<View>();
    ViewGroup ActionMenuView = (ViewGroup) recursiveFindActionMenuView(ActionBarContainer);
    if (ActionMenuView == null) {
      // The ActionBar is empty. Should not happen
      return null;
    }
    for (int i = 0; i < ActionMenuView.getChildCount(); i++) {
      View curView = ActionMenuView.getChildAt(i);
      ret.add(curView);
    }
    return ret;
  }

  public ArrayList<View> getActionBarItems() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        ActionBarItems.remove(i);
        return ActionBarItems;
      }
    }
    return ActionBarItems;
  }

  public View getActionBarMenuButton() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        return curView;
      }
    }
    return null;
  }

  public View getActionBarItem(int index) {
    ArrayList<View> ActionBarItems = getActionBarItems();
    if (ActionBarItems == null) {
      // There is no ActionBar
      return null;
    }
    if (index < ActionBarItems.size()) {
      return ActionBarItems.get(index);
    } else {
      // Out of range
      return null;
    }
  }

  private View recursiveFindActionMenuView(View entryPoint) {
    String curClassName = "";
    curClassName = entryPoint.getClass().getName();
    if (curClassName.endsWith("ActionMenuView")) {
      return entryPoint;
    }
    // entryPoint is not an ActionMenuView
    if (entryPoint instanceof ViewGroup) {
      ViewGroup vgEntry = (ViewGroup)entryPoint;
      for ( int i = 0; i<vgEntry.getChildCount(); i ++) {
        View curView = vgEntry.getChildAt(i);
        View retView = recursiveFindActionMenuView(curView);

        if (retView != null) {
          // ActionMenuView was found
          return retView;
        }
      }
      // Still not found
      return null;
    } else {
      return null;
    }
  }

  public View getActionBarMenuItem(int index) {
    View ret = null;
    ArrayList<View> MenuItems = getActionBarMenuItems();
    if (MenuItems != null && index < MenuItems.size()) {
      ret = MenuItems.get(index);
    }
    return ret;
  }

  public ArrayList<View> getActionBarMenuItems() {
    ArrayList<View> MenuItems = new ArrayList<View>();
    ArrayList<View> curViews = solo.getCurrentViews();

    for (int i = 0; i < curViews.size(); i++) {
      View itemView = curViews.get(i);
      String className = itemView.getClass().getName();
      if (className.endsWith("ListMenuItemView")) {
        MenuItems.add(itemView);
      }
    }
    return MenuItems;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
  }
  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  @SuppressWarnings("unchecked")
  public void handleMenuItemByText(String title) {
    View v = findViewByText(title);
    if (null != v) {
      // Menu item in option menu (or on action bar if no menu poped)
      // assertTrue("MenuItem: Not Enabled.", v.isEnabled());
      solo.clickOnText(title);
    } else {
      boolean hasMore = solo.searchText("More");
      if (hasMore) {
        solo.clickOnMenuItem("More");
        handleMenuItemByText(title);
        return;
      }
      // Menu item on action bar
      Class<? extends View> cls = null;
      try {
        cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
      ArrayList<? extends View> views = solo.getCurrentViews(cls);
      if (!views.isEmpty()) {
        solo.sendKey(KeyEvent.KEYCODE_MENU); // Hide option menu
        assertTrue("Menu Not Closed", solo.waitForDialogToClose());
      }
      View actionBarView = getActionBarView();
      assertNotNull("Action Bar Not Found", actionBarView);
      boolean onActionBar = false;
      for (View abv : getActionBarItems()) {
        for (View iv : solo.getViews(abv)) {
          if (iv instanceof TextView) {
            if (((TextView) iv).getText().toString().matches(title)) {
              onActionBar = true;
              assertTrue("MenuItem: Not Clickable.", iv.isClickable());
              solo.clickOnView(iv);
              break;
            }
          }
        }
        if (onActionBar) break;
      }
      if (!onActionBar) {
        // In action bar menu
        boolean found = false;
        View abMenuButton = getActionBarMenuButton();
        assertNotNull("Action Bar Menu Button Not Found", abMenuButton);
        solo.clickOnView(abMenuButton);
        assertTrue("Action Bar Not Open", solo.waitForDialogToOpen());
        ArrayList<View> acBarMIs = getActionBarMenuItems();
        for (View item : acBarMIs) {
          for (View iv : solo.getViews(item)) {
            if (iv instanceof TextView) {
              if (((TextView) iv).getText().toString().matches(title)) {
                found = true;
                assertTrue("MenuItem: Not Clickable.", iv.isClickable());
                solo.clickOnView(iv);
                break;
              }
            }
          }
          if (found) break;
        }
        assertTrue("MenuItem: not found.", found);
      }
    }
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
