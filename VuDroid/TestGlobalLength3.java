/*
 * This file is automatically created by Gator.
 */

package org.vudroid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 5;
  private final int DJVU_IDX = 4;
  private final int DIR_IDX = 1;
  private final String GotoPageNum = "1";
  private final String PDF = "test.pdf";
  private final String DJVU = "superhero.djvu";
  public TestGlobalLength3() {
    super(org.vudroid.core.MainBrowserActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
        // Implicit Launch. BenchmarkName: VuDroid
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
        solo.clickOnText(DJVU); // click on a DJVU file
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.MainBrowserActivity
        solo.getCurrentActivity().finish(); // mock EXIT
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.core.MainBrowserActivity
        solo.goBack();
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
        solo.clickOnText(PDF); // click on a PDF file
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
