/*
 * This file is automatically created by Gator.
 */

package org.vudroid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestLocalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 5;
  private final int DJVU_IDX = 4;
  private final int DIR_IDX = 1;
  private final String GotoPageNum = "1";
  private final String PDF = "test.pdf";
  private final String DJVU = "superhero.djvu";
  public TestLocalLength3() {
    super(org.vudroid.core.MainBrowserActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 1
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 1
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 1
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 1
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 1
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 1
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 1
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 1
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 1
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 1
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 2
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 2
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.core.MainBrowserActivity, of length 3
        // Priority: 0
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        solo.clickInList(DIR_IDX, 0); // click on a directory
        solo.clickOnText("sdcard"); // back to home
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
        // org.vudroid.core.MainBrowserActivity => org.vudroid.core.MainBrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.core.MainBrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.djvudroid.DjvuViewerActivity
        solo.typeText(0, GotoPageNum);
        solo.clickOnButton("Go to Page!");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.djvudroid.DjvuViewerActivity
        solo.typeText(0, GotoPageNum);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    
    // ===> org.vudroid.djvudroid.DjvuViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.djvudroid.DjvuViewerActivity
    solo.clickOnText(DJVU); // click on a DJVU file
    assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.djvudroid.DjvuViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
        // org.vudroid.djvudroid.DjvuViewerActivity => org.vudroid.djvudroid.DjvuViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.djvudroid.DjvuViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.pdfdroid.PdfViewerActivity
        solo.typeText(0, GotoPageNum);
        solo.clickOnButton("Go to Page!");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.pdfdroid.PdfViewerActivity
        solo.typeText(0, GotoPageNum);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.core.GoToPageDialog
        solo.clickOnMenuItem("Go to page");
        assertDialog();
        // org.vudroid.core.GoToPageDialog => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        handleMenuItemByText("Full screen (on|off)");
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        solo.goBack();
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    
    // ===> org.vudroid.pdfdroid.PdfViewerActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.vudroid.core.MainBrowserActivity
    // Implicit Launch. BenchmarkName: VuDroid
    assertActivity(org.vudroid.core.MainBrowserActivity.class);
    // org.vudroid.core.MainBrowserActivity => org.vudroid.pdfdroid.PdfViewerActivity
    solo.clickOnText(PDF); // click on a PDF file
    assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.vudroid.pdfdroid.PdfViewerActivity, of length 3
        // Priority: 0
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        Util.rotateOnce(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
        // org.vudroid.pdfdroid.PdfViewerActivity => org.vudroid.pdfdroid.PdfViewerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.vudroid.pdfdroid.PdfViewerActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
