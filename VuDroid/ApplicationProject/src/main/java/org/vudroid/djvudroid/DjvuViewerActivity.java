package org.vudroid.djvudroid;

import org.vudroid.core.BaseViewerActivity;
import org.vudroid.core.DecodeService;
import org.vudroid.core.DecodeServiceBase;
import org.vudroid.djvudroid.codec.DjvuContext;

public class DjvuViewerActivity extends BaseViewerActivity
{
    @Override
    protected DecodeService createDecodeService()
    {
        return new DecodeServiceBase(new DjvuContext());
    	/**
    	 * Fix victor
    	 * */
    	/*context = new DjvuContext();
    	return new DecodeServiceBase(context);*/
    }

    /**
     * Fix victor
     * */
    /*DjvuContext context = null;
    
	protected void destroyDecodeService() {
		// TODO Auto-generated method stub
		context.interrupt();
	}*/
	
    
    /**
     * Fix victor
     * */
   /* @Override
    protected void onDestroy(){
    	super.onDestroy();
    	destroyDecodeService();
    }*/
}
