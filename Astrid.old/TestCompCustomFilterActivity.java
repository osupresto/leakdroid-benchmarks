/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompCustomFilterActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestCompCustomFilterActivity() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
