/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import com.timsu.astrid.R;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCustomFilterActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestCustomFilterActivity() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
