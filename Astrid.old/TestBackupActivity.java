/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.View;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBackupActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestBackupActivity() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
