/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.View;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBackupActivityLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestBackupActivityLength3() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Import Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        solo.clickOnButton("Export Tasks");
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
        // com.todoroo.astrid.backup.BackupActivity => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.backup.BackupActivity => android.app.AlertDialog
        solo.clickOnButton("Export Tasks");
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.backup.BackupActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(2000);
    assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
