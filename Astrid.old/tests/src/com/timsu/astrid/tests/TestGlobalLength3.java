/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import com.timsu.astrid.R;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestGlobalLength3() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.todoroo.astrid.activity.TaskListActivity
        // Implicit Launch. BenchmarkName: Astrid
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.EditPreferences, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.activity.TaskListActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.AlertDialog
        // Implicit Launch. BenchmarkName: Astrid
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.AlertDialog
        // Implicit Launch. BenchmarkName: Astrid
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00237() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.AlertDialog
        // Implicit Launch. BenchmarkName: Astrid
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00238() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.AlertDialog
        // Implicit Launch. BenchmarkName: Astrid
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00239() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.AlertDialog
        // Implicit Launch. BenchmarkName: Astrid
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.AlertDialog
        // Implicit Launch. BenchmarkName: Astrid
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00241() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.AlertDialog
        // Implicit Launch. BenchmarkName: Astrid
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00242() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00243() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00244() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00245() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00246() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00247() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00248() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00249() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00250() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00251() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00252() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Save Changes");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00253() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Don\'t Save");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00254() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.reminders.NotificationActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00255() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00256() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00257() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00258() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00259() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00260() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00261() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00262() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00263() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00264() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00265() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00266() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00267() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00268() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00269() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00270() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00271() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00272() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00273() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00274() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00275() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00276() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.AddOnActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00277() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00278() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00279() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00280() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00281() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.EditPreferences
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.EditPreferences.class);
        // com.todoroo.astrid.activity.EditPreferences => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00282() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00283() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Delete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00284() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Undelete Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00285() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        handleMenuItemByText("Purge Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00286() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00287() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00288() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00289() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00290() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00291() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00292() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00293() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00294() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00295() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00296() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00297() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00298() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00299() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00300() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00301() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00302() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskEditActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00303() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00304() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00305() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00306() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00307() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00308() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  public View getActionBarView() {
    // solo.sleep(2000);
    ArrayList<View> alViews = solo.getCurrentViews();
    for (View curView :alViews) {
      String className = curView.getClass().getName();
      if (className.endsWith("ActionBarContainer")) {
        return curView;
      }
    }
    return null;
  }

  private ArrayList<View> getActionBarItemsWithMenuButton() {
    ViewGroup ActionBarContainer = (ViewGroup) this.getActionBarView();
    ArrayList<View> ret = new ArrayList<View>();
    ViewGroup ActionMenuView = (ViewGroup) recursiveFindActionMenuView(ActionBarContainer);
    if (ActionMenuView == null) {
      // The ActionBar is empty. Should not happen
      return null;
    }
    for (int i = 0; i < ActionMenuView.getChildCount(); i++) {
      View curView = ActionMenuView.getChildAt(i);
      ret.add(curView);
    }
    return ret;
  }

  public ArrayList<View> getActionBarItems() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        ActionBarItems.remove(i);
        return ActionBarItems;
      }
    }
    return ActionBarItems;
  }

  public View getActionBarMenuButton() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        return curView;
      }
    }
    return null;
  }

  public View getActionBarItem(int index) {
    ArrayList<View> ActionBarItems = getActionBarItems();
    if (ActionBarItems == null) {
      // There is no ActionBar
      return null;
    }
    if (index < ActionBarItems.size()) {
      return ActionBarItems.get(index);
    } else {
      // Out of range
      return null;
    }
  }

  private View recursiveFindActionMenuView(View entryPoint) {
    String curClassName = "";
    curClassName = entryPoint.getClass().getName();
    if (curClassName.endsWith("ActionMenuView")) {
      return entryPoint;
    }
    // entryPoint is not an ActionMenuView
    if (entryPoint instanceof ViewGroup) {
      ViewGroup vgEntry = (ViewGroup)entryPoint;
      for ( int i = 0; i<vgEntry.getChildCount(); i ++) {
        View curView = vgEntry.getChildAt(i);
        View retView = recursiveFindActionMenuView(curView);

        if (retView != null) {
          // ActionMenuView was found
          return retView;
        }
      }
      // Still not found
      return null;
    } else {
      return null;
    }
  }

  public View getActionBarMenuItem(int index) {
    View ret = null;
    ArrayList<View> MenuItems = getActionBarMenuItems();
    if (MenuItems != null && index < MenuItems.size()) {
      ret = MenuItems.get(index);
    }
    return ret;
  }

  public ArrayList<View> getActionBarMenuItems() {
    ArrayList<View> MenuItems = new ArrayList<View>();
    ArrayList<View> curViews = solo.getCurrentViews();

    for (int i = 0; i < curViews.size(); i++) {
      View itemView = curViews.get(i);
      String className = itemView.getClass().getName();
      if (className.endsWith("ListMenuItemView")) {
        MenuItems.add(itemView);
      }
    }
    return MenuItems;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
  }
  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(2000);
    assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());
  }
  @SuppressWarnings("unchecked")
  public void handleMenuItemByText(String title) {
    View v = findViewByText(title);
    if (null != v) {
      // Menu item in option menu (or on action bar if no menu poped)
      // assertTrue("MenuItem: Not Enabled.", v.isEnabled());
      solo.clickOnText(title);
    } else {
      boolean hasMore = solo.searchText("More");
      if (hasMore) {
        solo.clickOnMenuItem("More");
        handleMenuItemByText(title);
        return;
      }
      // Menu item on action bar
      Class<? extends View> cls = null;
      try {
        cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
      ArrayList<? extends View> views = solo.getCurrentViews(cls);
      if (!views.isEmpty()) {
        solo.sendKey(KeyEvent.KEYCODE_MENU); // Hide option menu
        assertTrue("Menu Not Closed", solo.waitForDialogToClose());
      }
      View actionBarView = getActionBarView();
      assertNotNull("Action Bar Not Found", actionBarView);
      boolean onActionBar = false;
      for (View abv : getActionBarItems()) {
        for (View iv : solo.getViews(abv)) {
          if (iv instanceof TextView) {
            if (((TextView) iv).getText().toString().matches(title)) {
              onActionBar = true;
              assertTrue("MenuItem: Not Clickable.", iv.isClickable());
              solo.clickOnView(iv);
              break;
            }
          }
        }
        if (onActionBar) break;
      }
      if (!onActionBar) {
        // In action bar menu
        boolean found = false;
        View abMenuButton = getActionBarMenuButton();
        assertNotNull("Action Bar Menu Button Not Found", abMenuButton);
        solo.clickOnView(abMenuButton);
        assertTrue("Action Bar Not Open", solo.waitForDialogToOpen());
        ArrayList<View> acBarMIs = getActionBarMenuItems();
        for (View item : acBarMIs) {
          for (View iv : solo.getViews(item)) {
            if (iv instanceof TextView) {
              if (((TextView) iv).getText().toString().matches(title)) {
                found = true;
                assertTrue("MenuItem: Not Clickable.", iv.isClickable());
                solo.clickOnView(iv);
                break;
              }
            }
          }
          if (found) break;
        }
        assertTrue("MenuItem: not found.", found);
      }
    }
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
