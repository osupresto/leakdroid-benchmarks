/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import com.timsu.astrid.R;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestShortcutActivityLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestShortcutActivityLength3() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(2000);
    assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
