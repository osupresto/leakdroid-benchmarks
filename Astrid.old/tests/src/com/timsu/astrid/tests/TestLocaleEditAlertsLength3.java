/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestLocaleEditAlertsLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestLocaleEditAlertsLength3() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 1
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 1
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 1
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 2
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
        // com.todoroo.astrid.locale.LocaleEditAlerts => com.todoroo.astrid.locale.LocaleEditAlerts
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        solo.goBack();
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.locale.LocaleEditAlerts, of length 3
        // Priority: 0
        // com.todoroo.astrid.locale.LocaleEditAlerts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.todoroo.astrid.locale.LocaleEditAlerts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.locale.LocaleEditAlerts.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  public View getActionBarView() {
    // solo.sleep(2000);
    ArrayList<View> alViews = solo.getCurrentViews();
    for (View curView :alViews) {
      String className = curView.getClass().getName();
      if (className.endsWith("ActionBarContainer")) {
        return curView;
      }
    }
    return null;
  }

  private ArrayList<View> getActionBarItemsWithMenuButton() {
    ViewGroup ActionBarContainer = (ViewGroup) this.getActionBarView();
    ArrayList<View> ret = new ArrayList<View>();
    ViewGroup ActionMenuView = (ViewGroup) recursiveFindActionMenuView(ActionBarContainer);
    if (ActionMenuView == null) {
      // The ActionBar is empty. Should not happen
      return null;
    }
    for (int i = 0; i < ActionMenuView.getChildCount(); i++) {
      View curView = ActionMenuView.getChildAt(i);
      ret.add(curView);
    }
    return ret;
  }

  public ArrayList<View> getActionBarItems() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        ActionBarItems.remove(i);
        return ActionBarItems;
      }
    }
    return ActionBarItems;
  }

  public View getActionBarMenuButton() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        return curView;
      }
    }
    return null;
  }

  public View getActionBarItem(int index) {
    ArrayList<View> ActionBarItems = getActionBarItems();
    if (ActionBarItems == null) {
      // There is no ActionBar
      return null;
    }
    if (index < ActionBarItems.size()) {
      return ActionBarItems.get(index);
    } else {
      // Out of range
      return null;
    }
  }

  private View recursiveFindActionMenuView(View entryPoint) {
    String curClassName = "";
    curClassName = entryPoint.getClass().getName();
    if (curClassName.endsWith("ActionMenuView")) {
      return entryPoint;
    }
    // entryPoint is not an ActionMenuView
    if (entryPoint instanceof ViewGroup) {
      ViewGroup vgEntry = (ViewGroup)entryPoint;
      for ( int i = 0; i<vgEntry.getChildCount(); i ++) {
        View curView = vgEntry.getChildAt(i);
        View retView = recursiveFindActionMenuView(curView);

        if (retView != null) {
          // ActionMenuView was found
          return retView;
        }
      }
      // Still not found
      return null;
    } else {
      return null;
    }
  }

  public View getActionBarMenuItem(int index) {
    View ret = null;
    ArrayList<View> MenuItems = getActionBarMenuItems();
    if (MenuItems != null && index < MenuItems.size()) {
      ret = MenuItems.get(index);
    }
    return ret;
  }

  public ArrayList<View> getActionBarMenuItems() {
    ArrayList<View> MenuItems = new ArrayList<View>();
    ArrayList<View> curViews = solo.getCurrentViews();

    for (int i = 0; i < curViews.size(); i++) {
      View itemView = curViews.get(i);
      String className = itemView.getClass().getName();
      if (className.endsWith("ListMenuItemView")) {
        MenuItems.add(itemView);
      }
    }
    return MenuItems;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
  }
  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
