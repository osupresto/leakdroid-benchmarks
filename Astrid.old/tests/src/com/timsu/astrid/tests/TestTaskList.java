/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestTaskList extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestTaskList() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.timsu.astrid.activities.TaskList, of length 1
        // Priority: 0
        // com.timsu.astrid.activities.TaskList => com.timsu.astrid.activities.TaskList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.timsu.astrid.activities.TaskList.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.timsu.astrid.activities.TaskList, of length 1
        // Priority: 0
        // com.timsu.astrid.activities.TaskList => com.timsu.astrid.activities.TaskList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.timsu.astrid.activities.TaskList.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.timsu.astrid.activities.TaskList, of length 1
        // Priority: 0
        // com.timsu.astrid.activities.TaskList => com.timsu.astrid.activities.TaskList
        Util.rotateOnce(solo);
        assertActivity(com.timsu.astrid.activities.TaskList.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
