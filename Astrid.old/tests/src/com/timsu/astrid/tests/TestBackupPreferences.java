/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBackupPreferences extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestBackupPreferences() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupPreferences, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupPreferences => com.todoroo.astrid.backup.BackupPreferences
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupPreferences.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupPreferences, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupPreferences => com.todoroo.astrid.backup.BackupPreferences
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.backup.BackupPreferences.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Backups");
    assertActivity(com.todoroo.astrid.backup.BackupPreferences.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.backup.BackupPreferences, of length 1
        // Priority: 0
        // com.todoroo.astrid.backup.BackupPreferences => com.todoroo.astrid.backup.BackupPreferences
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.backup.BackupPreferences.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
