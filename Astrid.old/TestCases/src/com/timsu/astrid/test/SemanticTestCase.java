/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.timsu.astrid.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.GenericFunctor;

public class SemanticTestCase extends AmplifyTestCase {
	private static final String PKG_NAME = "com.todoroo.astrid.activity";
	private static final String CLS_NAME = "com.todoroo.astrid.activity.TaskListActivity";

	private static final String TASK_NAME = "XYZ";
	
	public SemanticTestCase() {
		super(PKG_NAME, CLS_NAME);
	}
	
	public void setUp() throws Exception {
		super.setUp();
		if (!solo.searchText("Abc")) {
			solo.clickOnEditText(0);
			solo.enterText(0, "Abc");
			solo.sendKey(Solo.ENTER);
		}
	}

	public void testAddRemoveTask() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnEditText(0);
				solo.enterText(0, TASK_NAME);
				solo.sendKey(Solo.ENTER);
				solo.clickLongOnText(TASK_NAME);
				solo.clickOnText("Delete Task");
				solo.clickOnButton("OK");
			}
			
		});
	}

	public void testMarkUnmarkTask() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnCheckBox(0);
				solo.sleep(1000);
				solo.clickOnCheckBox(0);
				solo.sleep(1000);
			}
			
		});
	}
	
	// Pre-Condition: a default task named Abc
	public void testShowHideButtons() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("Abc");
				solo.clickOnText("Abc");
			}
			
		});
	}
	
	public void testChangeSort() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Sort & Hidden");
				solo.clickOnButton("Always");
			}
			
		});
	}
	
	public void testAddRemoveFilterCriteria() {
		solo.clickOnImage(0);
		solo.clickOnText("Custom Filter...");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnButton("Add Criteria");
				solo.clickOnText("Due By...");
				solo.clickOnText("No Due Date");
				solo.clickLongOnText("Due By: No Due Date");
				solo.clickOnText("Delete Row");
			}
			
		});
	}
}
