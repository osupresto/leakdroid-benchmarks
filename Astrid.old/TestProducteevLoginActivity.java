/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestProducteevLoginActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestProducteevLoginActivity() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Producteev");
    solo.clickOnText("Log In & Synchronize!");
    assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.producteev.ProducteevLoginActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        solo.clickOnButton("Sign In");
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Producteev");
    solo.clickOnText("Log In & Synchronize!");
    assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.producteev.ProducteevLoginActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        solo.clickOnButton("Create New User");
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Producteev");
    solo.clickOnText("Log In & Synchronize!");
    assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.producteev.ProducteevLoginActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Producteev");
    solo.clickOnText("Log In & Synchronize!");
    assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.producteev.ProducteevLoginActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Producteev");
    solo.clickOnText("Log In & Synchronize!");
    assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.producteev.ProducteevLoginActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Producteev");
    solo.clickOnText("Log In & Synchronize!");
    assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.producteev.ProducteevLoginActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        solo.clickOnButton("Sign In");
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        solo.clickOnButton("Create New User");
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Settings");
    solo.clickOnText("Producteev");
    solo.clickOnText("Log In & Synchronize!");
    assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.producteev.ProducteevLoginActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        solo.clickOnButton("Create New User");
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
        // com.todoroo.astrid.producteev.ProducteevLoginActivity => com.todoroo.astrid.producteev.ProducteevLoginActivity
        solo.clickOnButton("Sign In");
        assertActivity(com.todoroo.astrid.producteev.ProducteevLoginActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
