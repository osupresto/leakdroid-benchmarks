#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript APV cx.hell.android.pdfview cx.hell.android.pdfview.test MainTestCase testChooseFileActivity,testOpenFileActivity,testAboutPDFViewActivity,testOptions rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript APV cx.hell.android.pdfview cx.hell.android.pdfview.test MainTestCase testChooseFileActivity,testOpenFileActivity,testAboutPDFViewActivity,testOptions home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript APV cx.hell.android.pdfview cx.hell.android.pdfview.test MainTestCase testChooseFileActivity,testOpenFileActivity,testAboutPDFViewActivity,testOptions power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript APV cx.hell.android.pdfview cx.hell.android.pdfview.test BackButtonTestCase testChooseFileActivity,testOpenFileActivity,testAboutPDFViewActivity,testOptions,testOpenBack,testOpen2OptionsBACK,testOpen2AboutBACK back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript APV cx.hell.android.pdfview cx.hell.android.pdfview.test SemanticTestCase testFindHide,testGoBackForth,testGoUpDown,testPrevNext,testRotateLeftRight,testZoomInOut semantic.$N.action

