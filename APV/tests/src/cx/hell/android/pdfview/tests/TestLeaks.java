/*
 * This file is automatically created by Gator.
 */

package cx.hell.android.pdfview.tests;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;

public class TestLeaks extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 1;
  private final String PDF_NAME = "test";
  private final String HOME_NAME = "Home";
  private final int HOME_IDX = 2;
  private final int DIR_IDX = 3;
  private final int RECENT_IDX = 1;
  public TestLeaks() {
    super(cx.hell.android.pdfview.ChooseFileActivity.class);
  }

  public void testNeutralCycle00040() throws Exception {

    // Start node is automatically triggered

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnText(PDF_NAME);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    /*
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
    */
  }
}