/*
 * This file is automatically created by Gator.
 */

package cx.hell.android.pdfview.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestInvalidIntraLocalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 1;
  private final int HOME_IDX = 2;
  private final int DIR_IDX = 3;
  private final int RECENT_IDX = 1;
  public TestInvalidIntraLocalLength3() {
    super(cx.hell.android.pdfview.ChooseFileActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 1
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.AboutPDFViewActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Set current folder as home");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Delete");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_4 = solo.getCurrentViews(Button.class);
        Button VIEW_5 = VIEWS_4.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_5);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        solo.waitForDialogToClose(); // CANCEL DIALOG
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        solo.waitForDialogToClose(); // CANCEL DIALOG
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_5 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_5 = solo.getCurrentViews(Button.class);
        Button VIEW_6 = VIEWS_5.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_6);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00237() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00238() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00239() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00241() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00242() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00243() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00244() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00245() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00246() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00247() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00248() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00249() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00250() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00251() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00252() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00253() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00254() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00255() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00256() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00257() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00258() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00259() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00260() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00261() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00262() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00263() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00264() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00265() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00266() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00267() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00268() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00269() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00270() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00271() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00272() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00273() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00274() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00275() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00276() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(2); // click ZoomUpButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00277() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00278() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00279() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00280() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00281() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00282() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00283() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00284() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00285() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00286() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00287() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00288() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00289() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00290() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00291() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00292() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00293() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00294() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00295() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00296() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00297() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00298() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00299() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00300() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00301() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00302() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00303() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00304() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00305() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00306() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00307() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00308() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00309() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00310() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00311() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00312() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00313() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00314() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00315() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00316() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00317() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00318() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.Dialog
        solo.clickOnMenuItem("Find...");
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00319() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.Dialog
        solo.clickOnMenuItem("Find...");
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00320() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00321() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00322() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00323() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00324() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00325() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00326() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00327() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00328() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00329() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00330() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00331() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00332() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00333() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00334() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00335() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00336() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00337() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00338() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00339() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00340() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00341() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00342() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00343() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00344() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00345() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00346() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00347() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00348() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00349() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00350() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00351() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00352() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00353() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00354() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00355() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00356() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00357() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00358() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00359() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00360() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00361() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00362() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00363() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00364() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00365() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00366() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00367() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00368() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00369() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00370() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00371() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00372() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00373() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00374() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00375() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00376() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00377() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00378() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00379() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00380() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00381() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00382() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00383() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00384() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00385() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00386() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00387() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00388() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00389() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00390() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00391() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00392() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00393() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00394() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00395() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00396() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00397() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00398() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00399() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00400() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00401() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00402() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00403() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00404() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00405() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00406() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00407() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00408() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00409() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00410() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00411() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00412() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00413() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00414() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00415() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00416() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00417() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00418() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00419() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00420() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00421() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00422() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00423() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00424() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00425() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00426() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00427() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00428() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00429() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00430() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00431() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00432() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00433() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00434() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00435() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00436() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00437() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00438() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00439() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00440() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00441() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00442() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00443() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00444() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00445() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00446() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00447() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00448() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00449() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00450() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00451() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00452() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00453() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00454() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00455() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00456() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00457() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00458() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00459() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00460() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00461() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00462() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00463() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00464() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00465() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00466() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00467() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00468() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00469() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00470() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00471() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00472() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00473() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00474() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00475() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00476() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00477() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00478() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00479() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00480() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00481() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00482() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00483() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00484() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00485() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00486() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00487() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00488() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.Dialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00489() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.Dialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00490() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.Dialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00491() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.Dialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00492() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00493() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00494() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00495() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00496() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00497() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00498() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00499() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00500() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00501() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00502() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00503() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00504() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00505() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00506() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00507() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00508() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00509() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00510() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00511() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00512() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00513() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00514() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00515() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00516() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00517() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00518() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00519() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00520() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00521() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00522() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00523() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00524() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00525() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00526() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00527() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00528() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00529() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00530() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00531() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00532() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00533() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00534() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_3 = solo.getCurrentViews(Button.class);
        Button VIEW_4 = VIEWS_3.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_4);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00535() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00536() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00537() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00538() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00539() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00540() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00541() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00542() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00543() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00544() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00545() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00546() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnMenuItem("Rotate left");
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00547() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00548() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00549() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00550() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00551() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00552() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00553() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00554() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00555() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00556() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00557() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00558() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00559() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00560() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00561() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00562() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00563() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00564() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00565() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00566() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00567() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00568() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(0); // click ZoomDownButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00569() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00570() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00571() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00572() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00573() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00574() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00575() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00576() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00577() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.Options
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
