/*
 * This file is automatically created by Gator.
 */

package cx.hell.android.pdfview.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 1;
  private final int HOME_IDX = 2;
  private final int DIR_IDX = 3;
  private final int RECENT_IDX = 1;
  public TestGlobalLength3() {
    super(cx.hell.android.pdfview.ChooseFileActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
        solo.clickOnMenuItem("About");
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    // ===> cx.hell.android.pdfview.AboutPDFViewActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
    solo.clickOnMenuItem("About");
    assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.AboutPDFViewActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
        solo.clickOnMenuItem("About");
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
        solo.clickOnMenuItem("About");
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.Options
        solo.clickOnMenuItem("Options");
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Open...");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.AboutPDFViewActivity
        solo.clickOnMenuItem("About");
        assertActivity(cx.hell.android.pdfview.AboutPDFViewActivity.class);
        // cx.hell.android.pdfview.AboutPDFViewActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickOnMenuItem("Open...");
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.Options
        solo.clickOnMenuItem("Options");
        assertActivity(cx.hell.android.pdfview.Options.class);
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickOnImageButton(1); // click ZoomWidthButton
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
        // solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_UP);
        // solo.sendKey(KeyEvent.KEYCODE_DPAD_DOWN);
        //... (see more in PagesView#onKey)
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.OpenFileActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.Options
        solo.clickOnMenuItem("Options");
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    // ===> cx.hell.android.pdfview.Options
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => android.view.Menu
    solo.sendKey(KeyEvent.KEYCODE_MENU);
    assertMenu();
    // android.view.Menu => cx.hell.android.pdfview.Options
    solo.clickOnMenuItem("Options");
    assertActivity(cx.hell.android.pdfview.Options.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.Options, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.Options => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => cx.hell.android.pdfview.Options
        solo.clickOnMenuItem("Options");
        assertActivity(cx.hell.android.pdfview.Options.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
