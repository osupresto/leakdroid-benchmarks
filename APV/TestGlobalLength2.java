/*
 * This file is automatically created by Gator.
 */

package cx.hell.android.pdfview.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength2 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 1;
  private final int HOME_IDX = 2;
  private final int DIR_IDX = 3;
  private final int RECENT_IDX = 1;
  public TestGlobalLength2() {
    super(cx.hell.android.pdfview.ChooseFileActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
        // Implicit Launch. BenchmarkName: APV
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
        solo.clickInList(PDF_IDX, 0);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
