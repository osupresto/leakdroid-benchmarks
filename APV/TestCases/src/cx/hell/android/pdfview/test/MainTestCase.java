/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package cx.hell.android.pdfview.test;

import pai.AmplifyTestCase;

public class MainTestCase extends AmplifyTestCase implements APVTestConstants {
	public MainTestCase() throws Exception {
		super(PKG_NAME, CLS_NAME);
	}

	public void testChooseFileActivity() {
		solo.assertCurrentActivity(getName(), "ChooseFileActivity");
	}
	
	public void testOpenFileActivity() {
		solo.clickOnText(PDF_FILE);
		solo.assertCurrentActivity(getName(), "OpenFileActivity");
	}
	
	public void testAboutPDFViewActivity() {
		solo.clickOnMenuItem("About");
		solo.assertCurrentActivity(getName(), "AboutPDFViewActivity");
	}
	
	public void testOptions() {
		solo.clickOnMenuItem("Options");
		solo.assertCurrentActivity(getName(), "Options");
	}	
}
