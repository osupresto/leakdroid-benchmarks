/* 
 * APVTestConstants.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package cx.hell.android.pdfview.test;

public interface APVTestConstants {
	String PKG_NAME = "cx.hell.android.pdfview";
	String CLS_NAME = "cx.hell.android.pdfview.ChooseFileActivity";
	String PDF_FILE = "test";	
}
