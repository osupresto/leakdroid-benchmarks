/* 
 * BackButtonTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package cx.hell.android.pdfview.test;

import pai.AmplifyTestCase;
import pai.AmplifyUtils;
import pai.GenericFunctor;

public class BackButtonTestCase extends AmplifyTestCase implements APVTestConstants {	
	
	public BackButtonTestCase() throws Exception {
		super(PKG_NAME, CLS_NAME);
	}
	
	public void testChooseFileActivity() {
		solo.assertCurrentActivity(getName(), "ChooseFileActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				AmplifyUtils.leaveAndBack(solo);
				solo.assertCurrentActivity(getName(), "ChooseFileActivity");
			}
		});
	}
	
	public void testOpenFileActivity() {
		solo.clickOnText(PDF_FILE);
		solo.assertCurrentActivity(getName(), "OpenFileActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText(PDF_FILE);
				solo.assertCurrentActivity(getName(), "OpenFileActivity");
			}			
		});
	}

  public void testOpen2AboutBACK() {
		solo.clickOnText(PDF_FILE);
    solo.clickOnMenuItem("More");
    solo.clickOnText("About");
    solo.assertCurrentActivity(getName(), "AboutPDFViewActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("More");
			    solo.clickOnText("About");
				solo.assertCurrentActivity(getName(), "AboutPDFViewActivity");
			}
		});
	}

	public void testAboutPDFViewActivity() {
		solo.clickOnMenuItem("About");
		solo.assertCurrentActivity(getName(), "AboutPDFViewActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("About");
				solo.assertCurrentActivity(getName(), "AboutPDFViewActivity");
			}
		});
	}

	public void testOpen2OptionsBACK() {
		solo.clickOnText(PDF_FILE);
		solo.clickOnMenuItem("Options");
		solo.assertCurrentActivity(getName(), "Options");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Options");
				solo.assertCurrentActivity(getName(), "Options");
			}
		});
	}
	
	public void testOptions() {
		solo.clickOnMenuItem("Options");
		solo.assertCurrentActivity(getName(), "Options");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Options");
				solo.assertCurrentActivity(getName(), "Options");
			}
		});
	}
	
	// "open..."/back
	public void testOpenBack() {
		solo.clickOnText(PDF_FILE);
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Open...");
				solo.waitForActivity("ChooseFileActivity");
				solo.goBack();
			}
		});
	}
}
