/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package cx.hell.android.pdfview.test;

import pai.AmplifyTestCase;
import pai.GenericFunctor;

public class SemanticTestCase extends AmplifyTestCase implements APVTestConstants {	
	public SemanticTestCase() throws Exception {
		super(PKG_NAME, CLS_NAME);
	}
	
	// go up/down directory hierarchy
	public void testGoUpDown() {
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("\\.\\.");
				solo.clickOnText("sdcard");
			}
		});
	}

	// go up/down directory hierarchy and set as default folder
	public void testGoUpDownSetDefault() {
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("\\.\\.");
				solo.clickOnMenuItem("Set current folder as home");
				solo.clickOnText("sdcard");
				solo.clickOnMenuItem("Set current folder as home");
			}
		});
	}

	// zoom in/out
	public void testZoomInOut() {
		solo.clickOnText(PDF_FILE);
		solo.clickOnImageButton(1);
		solo.waitForIdleSync();
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnImageButton(0);
				solo.waitForIdleSync();
				solo.clickOnImageButton(2);
				solo.waitForIdleSync();
			}
		});
	}
	
	// go back'n'forth
	public void testGoBackForth() {
		solo.clickOnText(PDF_FILE);
		solo.clickOnImageButton(1);
		solo.waitForIdleSync();
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Go to page...");
				solo.clearEditText(0);
				solo.enterText(0, "1");
				solo.clickOnButton("Go");
				solo.waitForIdleSync();
				
				solo.clickOnMenuItem("Go to page...");
				solo.clearEditText(0);
				solo.enterText(0, "2");
				solo.clickOnButton("Go");
				solo.waitForIdleSync();
			}
		});
	}
	
	// rotate left/right
	public void testRotateLeftRight() {
		solo.clickOnText(PDF_FILE);
		solo.clickOnImageButton(1);
		solo.waitForIdleSync();
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Rotate left");
				solo.waitForIdleSync();
				solo.clickOnMenuItem("Rotate right");
				solo.waitForIdleSync();
			}
		});
	}
	
	// find/hide
	public void testFindHide() {
		solo.clickOnText(PDF_FILE);
		solo.clickOnImageButton(1);
		solo.waitForIdleSync();
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("More");
				solo.clickOnText("Find...");
				solo.enterText(0, "context");
				solo.clickOnButton("Find");
				solo.waitForIdleSync();
				solo.clickOnButton("Hide");
			}
		});
		
	}
	
	// find prev/next
	public void testPrevNext() {
		solo.clickOnText(PDF_FILE);
		solo.clickOnImageButton(1);
		solo.waitForIdleSync();
		solo.clickOnMenuItem("More");
		solo.clickOnText("Find...");
		solo.enterText(0, "context");
		solo.clickOnButton("Find");
		solo.waitForIdleSync();
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				
				solo.clickOnButton("Prev");
				solo.waitForIdleSync();
				solo.clickOnButton("Next");
				solo.waitForIdleSync();
			}
		});
	}
}
