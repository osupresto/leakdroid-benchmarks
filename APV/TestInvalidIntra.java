/*
 * This file is automatically created by Gator.
 */

package cx.hell.android.pdfview.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.widget.Button;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestInvalidIntra extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final int PDF_IDX = 1;
  private final int HOME_IDX = 2;
  private final int DIR_IDX = 3;
  private final int RECENT_IDX = 1;
  public TestInvalidIntra() {
    super(cx.hell.android.pdfview.ChooseFileActivity.class);
  }

  public void testInvalidIntraCycle00001() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 2
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00002() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00003() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00004() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00005() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00006() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00007() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00008() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        Util.rotateOnce(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00009() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
        // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.ChooseFileActivity
        solo.clickInList(HOME_IDX, 0); // select a non-PDF item
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00010() throws Exception {
    
    // Start node is automatically triggered
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.ChooseFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.ChooseFileActivity => android.view.ContextMenu
        solo.clickLongInList(PDF_IDX, 0); // click on a PDF file
        // solo.clickLongInList(HOME_IDX, 0); // or click on HOME
        // solo.clickLongInList(RECENT_IDX, 0); // or click on RECENT FILES
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => cx.hell.android.pdfview.ChooseFileActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00011() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        Util.rotateOnce(solo);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        final View v_3 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00012() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.AlertDialog
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        Util.rotateOnce(solo);
        assertDialog();
        // android.app.AlertDialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00013() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.Dialog
        Util.rotateOnce(solo);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        // TODO
        ArrayList<Button> VIEWS_1 = solo.getCurrentViews(Button.class);
        Button VIEW_2 = VIEWS_1.get(0); // MAKE SURE IT INDEXES THE VIEW EXPECTED"
        solo.clickOnView(VIEW_2);
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00014() throws Exception {
    
    // ===> cx.hell.android.pdfview.OpenFileActivity
    // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => cx.hell.android.pdfview.ChooseFileActivity
    // Implicit Launch. BenchmarkName: APV
    assertActivity(cx.hell.android.pdfview.ChooseFileActivity.class);
    // cx.hell.android.pdfview.ChooseFileActivity => cx.hell.android.pdfview.OpenFileActivity
    solo.clickInList(PDF_IDX, 0);
    assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> cx.hell.android.pdfview.OpenFileActivity, of length 3
        // Priority: 0
        // cx.hell.android.pdfview.OpenFileActivity => android.app.Dialog
        solo.sendKey(KeyEvent.KEYCODE_SEARCH);
        assertDialog();
        // android.app.Dialog => android.app.Dialog
        Util.rotateOnce(solo);
        assertDialog();
        // android.app.Dialog => cx.hell.android.pdfview.OpenFileActivity
        solo.goBack();
        assertActivity(cx.hell.android.pdfview.OpenFileActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
