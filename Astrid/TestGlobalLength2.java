/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.View;
import android.content.Intent;
import com.timsu.astrid.R;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength2 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestGlobalLength2() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.extendedAddButton));
    assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskEditActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.todoroo.astrid.activity.TaskListActivity
        // Implicit Launch. BenchmarkName: Astrid
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnButton("View");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.core.CustomFilterActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.TaskEditActivity
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.activity.FilterListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
