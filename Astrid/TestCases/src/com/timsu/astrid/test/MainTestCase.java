/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.timsu.astrid.test;

import pai.AmplifyTestCase;

public class MainTestCase extends AmplifyTestCase {
	private static final String PKG_NAME = "com.todoroo.astrid.activity";
	private static final String CLS_NAME = "com.todoroo.astrid.activity.TaskListActivity";

	public MainTestCase() {
		super(PKG_NAME, CLS_NAME);
	}
	
	public void testTaskListActivity() {
		solo.assertCurrentActivity(getName(), "TaskListActivity");
	}
	
	// TODO: this is time-sensitive. It's tricky to launch, and
	// let's deal with it later.
	public void testShortcutActivity() {
		// TODO
		solo.assertCurrentActivity(getName(), "ShortcutActivity");
	}
	
	public void testFilterListActivity() {
		solo.clickOnImage(0);
		solo.assertCurrentActivity(getName(), "FilterListActivity");
	}
	
	public void testTaskEditActivity() {
		solo.clickOnImageButton(0);
		solo.assertCurrentActivity(getName(), "TaskEditActivity");
	}
	
	public void testAddOnActivity() {
		solo.clickOnMenuItem("Add-ons");
		solo.assertCurrentActivity(getName(), "AddOnActivity");
	}
	
	public void testEditPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.assertCurrentActivity(getName(), "EditPreferences");
	}
	
	// TODO: this requires very tricky replay. Let's do it later
	public void testWidgetConfigActivity() {
		// TODO		
		solo.assertCurrentActivity(getName(), "WidgetConfigActivity");
	}
	
	public void testDefaultsPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("New Task Defaults");
		solo.assertCurrentActivity(getName(), "DefaultsPreferences");
	}

	public void testCustomFilterActivity() {
		solo.clickOnImage(0);
		solo.clickOnText("Custom Filter...");
		solo.assertCurrentActivity(getName(), "CustomFilterActivity");
	}

	// NOTE: this is a dialog that can be trigger when the BroadcastReceiver
	// CustomFilterExposer is triggered, which is time-sensitive
	public void testCustomFilterExposer$DeleteActivity() {
		// TODO
		solo.assertCurrentActivity(getName(), "CustomFilterExposer$DeleteActivity");
	}
	
	public void testBackupPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("Backups");
		solo.assertCurrentActivity(getName(), "BackupPreferences");
	}
	
	// NOTE: time-sensitive, BroadcastReceiver
	public void testLocaleEditAlerts() {
		// TODO
		solo.assertCurrentActivity(getName(), "LocaleEditAlerts");
	}
	
	public void testReminderPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("Reminder Settings");
		solo.assertCurrentActivity(getName(), "ReminderPreferences");
	}
	
	// TODO: tray launch. Do it later
	public void testNotificationActivity() {
		// TODO
		solo.assertCurrentActivity(getName(), "NotificationActivity");
	}
	
	public void testProducteevPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("Producteev");
		solo.assertCurrentActivity(getName(), "ProducteevPreferences");
	}
	
	public void testProducteevLoginActivity() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("Producteev");
		solo.clickOnText("Log In & Synchronize!");
		solo.assertCurrentActivity(getName(), "ProducteevLoginActivity");
	}	
}
