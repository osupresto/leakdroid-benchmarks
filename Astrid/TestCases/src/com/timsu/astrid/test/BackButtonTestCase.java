/* 
 * BackButtonTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.timsu.astrid.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.AmplifyUtils;
import pai.GenericFunctor;

public class BackButtonTestCase extends AmplifyTestCase {
	private static final String PKG_NAME = "com.todoroo.astrid.activity";
	private static final String CLS_NAME = "com.todoroo.astrid.activity.TaskListActivity";

	public BackButtonTestCase() {
		super(PKG_NAME, CLS_NAME);
	}
	
	public void testTaskListActivity() {
		solo.assertCurrentActivity(getName(), "TaskListActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				AmplifyUtils.leaveAndBack(solo);				
				solo.assertCurrentActivity(getName(), "TaskListActivity");
			}			
		});
	}
	
	public void testFilterListActivity() {
		solo.clickOnImage(0);
		solo.assertCurrentActivity(getName(), "FilterListActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnImage(0);
				solo.assertCurrentActivity(getName(), "FilterListActivity");
			}
		});
	}
	
	public void testFilterListActivitySearchAndBack() {
		solo.clickOnImage(0);
		solo.assertCurrentActivity(getName(), "FilterListActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("Search...");
				solo.clickOnEditText(0);
				solo.enterText(0, "Abc");
				solo.sendKey(Solo.ENTER);
				solo.goBack();
			}
		});
	}
	
	public void testTaskEditActivity() {
		
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnImageButton(0);
				solo.assertCurrentActivity(getName(), "TaskEditActivity");
				solo.goBack();
			}
		});
	}
	
	public void testAddOnActivity() {
		solo.clickOnMenuItem("Add-ons");
		solo.assertCurrentActivity(getName(), "AddOnActivity");
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Add-ons");
				solo.assertCurrentActivity(getName(), "AddOnActivity");
			}
		});
	}
	
	public void testEditPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.assertCurrentActivity(getName(), "EditPreferences");
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Settings");
				solo.assertCurrentActivity(getName(), "EditPreferences");
			}
		});
	}
	
	public void testDefaultsPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("New Task Defaults");
		solo.assertCurrentActivity(getName(), "DefaultsPreferences");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("New Task Defaults");
				solo.assertCurrentActivity(getName(), "DefaultsPreferences");
			}
		});
	}
	
	public void testCustomFilterActivity() {
		solo.clickOnImage(0);
		solo.clickOnText("Custom Filter...");
		solo.assertCurrentActivity(getName(), "CustomFilterActivity");
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Custom Filter...");
				solo.assertCurrentActivity(getName(), "CustomFilterActivity");
			}
		});
	}
	
	public void testCustomFilterActivityViewAndBack() {
		solo.clickOnImage(0);
		solo.clickOnText("Custom Filter...");
		solo.assertCurrentActivity(getName(), "CustomFilterActivity");
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnButton("View");
				solo.goBack();
			}
		});
	}
	
	
	
	public void testBackupPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("Backups");
		solo.assertCurrentActivity(getName(), "BackupPreferences");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Backups");
				solo.assertCurrentActivity(getName(), "BackupPreferences");
			}
		});
	}
	
	public void testReminderPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("Reminder Settings");
		solo.assertCurrentActivity(getName(), "ReminderPreferences");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Reminder Settings");
				solo.assertCurrentActivity(getName(), "ReminderPreferences");
			}
		});
	}
	
	public void testProducteevPreferences() {
		solo.clickOnMenuItem("Settings");
		solo.clickOnText("Producteev");
		solo.assertCurrentActivity(getName(), "ProducteevPreferences");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Producteev");
				solo.assertCurrentActivity(getName(), "ProducteevPreferences");
			}
		});
	}
	
	public void testProducteevLoginActivity() {
		solo.clickOnMenuItem("Settings");
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("Producteev");
				solo.clickOnText("Log In & Synchronize!");
				solo.assertCurrentActivity(getName(), "ProducteevLoginActivity");
				solo.goBack();
			}
		});
	}
}
