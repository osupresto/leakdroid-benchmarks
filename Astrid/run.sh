#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript Astrid com.timsu.astrid com.timsu.astrid.test MainTestCase testTaskListActivity,testFilterListActivity,testTaskEditActivity,testAddOnActivity,testEditPreferences,testDefaultsPreferences,testCustomFilterActivity,testBackupPreferences,testReminderPreferences,testProducteevPreferences,testProducteevLoginActivity rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript Astrid com.timsu.astrid com.timsu.astrid.test MainTestCase testTaskListActivity,testFilterListActivity,testTaskEditActivity,testAddOnActivity,testEditPreferences,testDefaultsPreferences,testCustomFilterActivity,testBackupPreferences,testReminderPreferences,testProducteevPreferences,testProducteevLoginActivity home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript Astrid com.timsu.astrid com.timsu.astrid.test MainTestCase testTaskListActivity,testFilterListActivity,testTaskEditActivity,testAddOnActivity,testEditPreferences,testDefaultsPreferences,testCustomFilterActivity,testBackupPreferences,testReminderPreferences,testProducteevPreferences,testProducteevLoginActivity power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript Astrid com.timsu.astrid com.timsu.astrid.test BackButtonTestCase testTaskListActivity,testFilterListActivity,testFilterListActivitySearchAndBack,testTaskEditActivity,testAddOnActivity,testEditPreferences,testDefaultsPreferences,testCustomFilterActivity,testCustomFilterActivityViewAndBack,testBackupPreferences,testReminderPreferences,testProducteevPreferences,testProducteevLoginActivity back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript Astrid com.timsu.astrid com.timsu.astrid.test SemanticTestCase testAddRemoveTask,testMarkUnmarkTask,testShowHideButtons,testChangeSort,testAddRemoveFilterCriteria semantic.$N.action

