/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompAddOnActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestCompAddOnActivity() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Add-ons");
    assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.AddOnActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
        // com.todoroo.astrid.activity.AddOnActivity => com.todoroo.astrid.activity.AddOnActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.AddOnActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
