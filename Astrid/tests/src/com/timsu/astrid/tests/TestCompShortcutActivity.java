/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompShortcutActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestCompShortcutActivity() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.ShortcutActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
