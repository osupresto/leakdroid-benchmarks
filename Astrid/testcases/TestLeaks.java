/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;

public class TestLeaks extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestLeaks() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testNeutralCycle04864() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskEditActivity
        solo.clickOnMenuItem("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskEditActivity.class);
        // com.todoroo.astrid.activity.TaskEditActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  /*
   * ============================== Helpers ==============================
   */
  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    // Class<? extends View> cls = null;
    // try {
    //   cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    // } catch (ClassNotFoundException e) {
    //   e.printStackTrace();
    // }
    // ArrayList<? extends View> views = solo.getCurrentViews(cls);
    // assertTrue("Menu not open.", !views.isEmpty());
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    // assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    // assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }
}