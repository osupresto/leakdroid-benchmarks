/*
 * This file is automatically created by Gator.
 */

package com.timsu.astrid.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.widget.TextView;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import com.timsu.astrid.R;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestInvalidIntra extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String TASK_NAME = "Testing";;
  public TestInvalidIntra() {
    super(com.todoroo.astrid.activity.TaskListActivity.class);
  }

  public void testInvalidIntraCycle00001() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 1
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00006() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00007() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00008() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00009() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00010() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00011() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00012() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00013() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00014() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00015() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00016() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00017() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00018() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00019() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00020() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00021() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00022() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00023() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00024() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00025() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00026() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00027() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00028() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00029() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00030() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00031() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 2
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // TODO
        Activity act_4 = solo.getCurrentActivity();
        Intent intent_5 = new Intent(act_4, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        act_4.startActivity(intent_5);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_5 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_4 = solo.getCurrentActivity();
        Intent intent_5 = new Intent(act_4, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        act_4.startActivity(intent_5);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_6 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_6.isEnabled());
        solo.clickOnView(v_6); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_4 = solo.getCurrentActivity();
        Intent intent_5 = new Intent(act_4, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        act_4.startActivity(intent_5);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_4 = solo.getCurrentActivity();
        Intent intent_5 = new Intent(act_4, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        act_4.startActivity(intent_5);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.FilterListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.FilterListActivity => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
        // com.todoroo.astrid.activity.FilterListActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.FilterListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.FilterListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.FilterListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00077() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00078() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00079() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00080() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00081() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.ShortcutActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.ShortcutActivity => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
        // com.todoroo.astrid.activity.ShortcutActivity => android.app.AlertDialog
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.ShortcutActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.ShortcutActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.ShortcutActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00082() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00083() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00084() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00085() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00086() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00087() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00088() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00089() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00090() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00091() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00092() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00093() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00094() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00095() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00096() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00097() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00098() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00099() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00100() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00101() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00102() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00103() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00104() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00105() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00106() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00107() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00108() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00109() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00110() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00111() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00112() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00113() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00114() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00115() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00116() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00117() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00118() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00119() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00120() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00121() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00122() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00123() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00124() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00125() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00126() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00127() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00128() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00129() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00130() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00131() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00132() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00133() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00134() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00135() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00136() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00137() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00138() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00139() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00140() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00141() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00142() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00143() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00144() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00145() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00146() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00147() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00148() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00149() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00150() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00151() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00152() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00153() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00154() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00155() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00156() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00157() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00158() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00159() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00160() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00161() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00162() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00163() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00164() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00165() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00166() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00167() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00168() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00169() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00170() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00171() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00172() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00173() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00174() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00175() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00176() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00177() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00178() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00179() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00180() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00181() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00182() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00183() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_3 = solo.getCurrentActivity();
        Intent intent_4 = new Intent(act_3, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        act_3.startActivity(intent_4);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00184() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00185() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00186() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00187() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00188() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00189() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00190() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00191() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00192() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00193() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00194() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00195() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00196() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00197() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00198() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00199() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00200() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00201() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00202() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00203() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00204() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00205() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00206() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00207() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00208() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00209() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00210() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00211() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00212() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00213() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00214() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00215() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00216() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00217() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00218() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00219() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00220() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00221() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00222() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00223() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00224() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00225() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00226() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00227() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00228() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00229() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00230() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00231() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00232() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00233() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00234() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00235() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00236() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00237() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00238() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_5 = solo.getCurrentActivity();
        Intent intent_6 = new Intent(act_5, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_6.setAction(...);
        // intent_6.setData(...);
        // intent_6.setType(...);
        // intent_6.setFlags(...);
        act_5.startActivity(intent_6);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00239() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00240() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00241() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00242() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00243() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00244() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00245() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00246() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00247() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00248() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00249() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00250() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00251() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00252() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00253() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00254() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00255() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00256() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00257() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00258() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00259() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00260() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00261() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00262() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00263() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnMenuItem("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00264() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00265() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00266() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00267() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00268() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00269() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00270() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00271() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00272() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00273() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00274() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00275() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00276() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00277() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00278() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00279() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00280() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00281() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00282() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00283() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00284() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00285() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00286() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00287() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00288() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00289() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00290() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00291() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00292() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00293() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00294() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00295() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00296() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00297() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00298() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00299() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00300() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00301() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00302() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00303() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00304() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00305() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00306() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00307() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00308() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00309() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00310() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00311() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00312() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00313() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00314() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00315() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00316() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00317() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00318() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00319() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00320() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00321() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00322() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00323() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00324() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00325() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00326() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00327() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00328() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00329() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00330() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00331() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00332() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00333() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00334() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00335() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00336() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00337() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00338() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00339() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00340() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00341() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00342() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00343() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00344() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00345() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00346() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00347() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00348() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00349() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00350() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00351() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00352() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00353() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00354() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00355() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00356() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00357() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00358() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00359() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00360() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00361() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00362() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00363() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00364() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00365() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00366() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00367() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_3 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00368() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00369() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00370() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00371() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00372() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00373() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00374() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00375() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00376() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00377() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00378() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00379() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00380() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00381() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00382() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00383() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00384() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00385() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_3 = solo.getCurrentActivity();
        final Intent intent_4 = new Intent();
        // TODO
        // intent_4.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_4.setAction(...);
        // intent_4.setData(...);
        // intent_4.setType(...);
        // intent_4.setFlags(...);
        int ReqCode_5 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_3.startActivityForResult(intent_4, ReqCode_5);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_3 = solo.getCurrentActivity();
        act_3.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_6 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_6.isEnabled());
        solo.clickOnView(v_6); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00386() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00387() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00388() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00389() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00390() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00391() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_4 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00392() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00393() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00394() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00395() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00396() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00397() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_5 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_5.isEnabled());
        solo.clickOnView(v_5); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00398() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00399() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00400() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00401() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_5 = solo.getCurrentActivity();
        Intent intent_6 = new Intent(act_5, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_6.setAction(...);
        // intent_6.setData(...);
        // intent_6.setType(...);
        // intent_6.setFlags(...);
        act_5.startActivity(intent_6);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00402() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00403() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00404() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00405() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00406() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00407() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00408() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00409() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_4 = solo.getCurrentActivity();
        Intent intent_5 = new Intent(act_4, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        act_4.startActivity(intent_5);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_6 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_6.isEnabled());
        solo.clickOnView(v_6); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00410() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00411() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00412() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00413() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00414() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00415() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00416() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00417() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00418() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00419() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00420() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00421() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00422() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00423() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00424() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00425() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00426() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00427() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00428() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00429() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00430() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00431() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00432() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00433() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00434() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00435() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00436() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00437() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00438() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00439() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00440() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00441() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00442() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00443() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00444() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00445() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00446() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00447() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00448() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00449() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00450() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00451() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00452() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00453() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00454() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00455() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00456() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00457() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00458() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00459() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00460() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00461() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00462() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00463() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        solo.clickOnMenuItem("Edit Task");
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00464() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00465() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00466() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00467() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00468() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00469() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00470() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00471() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00472() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00473() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00474() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00475() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00476() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00477() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00478() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00479() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00480() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00481() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00482() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00483() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00484() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00485() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00486() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00487() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00488() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00489() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00490() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00491() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00492() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00493() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00494() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00495() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00496() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00497() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00498() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00499() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00500() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00501() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00502() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00503() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00504() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.extendedAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00505() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00506() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00507() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00508() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00509() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00510() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00511() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00512() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00513() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00514() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00515() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00516() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00517() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00518() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00519() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00520() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00521() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00522() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00523() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.activity.TaskListActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00524() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00525() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00526() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00527() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.activity.TaskListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00528() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00529() throws Exception {
    // TODO(hailong): hard coded
    assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.activity.TaskListActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
        // com.todoroo.astrid.activity.TaskListActivity => com.todoroo.astrid.activity.TaskListActivity
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.activity.TaskListActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00530() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00531() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00532() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00533() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00534() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00535() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00536() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00537() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00538() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00539() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00540() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00541() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnView(solo.getView(R.id.back));
    solo.clickOnText("Custom Filter...");
    assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.core.CustomFilterActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.core.CustomFilterActivity => com.todoroo.astrid.core.CustomFilterActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
        // com.todoroo.astrid.core.CustomFilterActivity => android.app.AlertDialog
        solo.clickOnButton("View");
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.core.CustomFilterActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.core.CustomFilterActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00542() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00543() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_2 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00544() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00545() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00546() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00547() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00548() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00549() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.reminders.NotificationActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00550() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00551() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00552() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00553() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00554() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00555() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00556() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00557() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00558() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00559() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        final View v_2 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_2.isEnabled());
        solo.clickOnView(v_2); 
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00560() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00561() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00562() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00563() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        Activity act_2 = solo.getCurrentActivity();
        Intent intent_3 = new Intent(act_2, com.todoroo.astrid.reminders.NotificationActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        act_2.startActivity(intent_3);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00564() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Activity act_2 = solo.getCurrentActivity();
        final Intent intent_3 = new Intent();
        // TODO
        // intent_3.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_3.setAction(...);
        // intent_3.setData(...);
        // intent_3.setType(...);
        // intent_3.setFlags(...);
        int ReqCode_4 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_2.startActivityForResult(intent_3, ReqCode_4);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_2 = solo.getCurrentActivity();
        act_2.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00565() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00566() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00567() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00568() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00569() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00570() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00571() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00572() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00573() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00574() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00575() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00576() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00577() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00578() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.reminders.NotificationActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00579() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.reminders.NotificationActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_3 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_3.isEnabled());
        solo.clickOnView(v_3); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00580() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.reminders.NotificationActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00581() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00582() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_4 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00583() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00584() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00585() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00586() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00587() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00588() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00589() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00590() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        final View v_1 = solo.getView(R.id.quickAddButton);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00591() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00592() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00593() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00594() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        Activity act_1 = solo.getCurrentActivity();
        Intent intent_2 = new Intent(act_1, com.todoroo.astrid.reminders.NotificationActivity.class);
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        act_1.startActivity(intent_2);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00595() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00596() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00597() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00598() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00599() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00600() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00601() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00602() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00603() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00604() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        solo.goBack();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00605() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.app.AlertDialog
        final View v_1 = solo.getView(R.id.back);
        assertTrue("ImageView: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertDialog();
        // android.app.AlertDialog => com.todoroo.astrid.reminders.NotificationActivity
        Util.rotateOnce(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }

  public void testInvalidIntraCycle00606() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.todoroo.astrid.reminders.NotificationActivity, of length 3
        // Priority: 0
        // com.todoroo.astrid.reminders.NotificationActivity => com.todoroo.astrid.reminders.NotificationActivity
        solo.scrollDown();
        solo.scrollUp();
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
        // com.todoroo.astrid.reminders.NotificationActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.todoroo.astrid.reminders.NotificationActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.todoroo.astrid.reminders.NotificationActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
