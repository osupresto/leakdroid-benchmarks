/**
 *
 */
package org.connectbot.service;

/**
 * @author Kenny Root
 * 
 */
public interface FontSizeChangedListener {

	/**
	 * @param size
	 *            new font size
	 */
	void onFontSizeChanged(float size);
}
