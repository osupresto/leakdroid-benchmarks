/*
 * This file is automatically created by Gator.
 */

package org.connectbot.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength2 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password = "prestoosu";
  public TestGlobalLength2() {
    super(org.connectbot.HostListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 2
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 2
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
