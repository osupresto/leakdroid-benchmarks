package org.connectbot.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.widget.TextView;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestLeaks extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String host = "presto@acadia.cse.ohio-state.edu";
  private final String password = "presto";
  public TestLeaks() {
    super(org.connectbot.HostListActivity.class);
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.connectbot.ConsoleActivity, of length 1
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.connectbot.HostListActivity, of length 1
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle01525() throws Exception {

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostEditorActivity
        solo.clickOnText("Edit host");
        assertActivity(org.connectbot.HostEditorActivity.class);
        // org.connectbot.HostEditorActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle01806() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        solo.clickOnText("Change password");
        assertDialog();
        solo.enterText(0,"123");
        solo.enterText(1,"123");
        solo.enterText(2,"123");
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_504 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_504.isEnabled());
        solo.clickOnView(v_504);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  /*
   * ============================== Helpers ==============================
   */
  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    // Class<? extends View> cls = null;
    // try {
    //   cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    // } catch (ClassNotFoundException e) {
    //   e.printStackTrace();
    // }
    // ArrayList<? extends View> views = solo.getCurrentViews(cls);
    // assertTrue("Menu not open.", !views.isEmpty());
  }
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(2000);
    /*
    assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());
    */
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    // assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    // assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }
}