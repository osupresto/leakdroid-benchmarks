/*
 * This file is automatically created by Gator.
 */

package org.connectbot.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBinderLocalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password = "RealPassword";
  public TestBinderLocalLength3() {
    super(org.connectbot.HostListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 1
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 1
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 1
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 1
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 1
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 1
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 1
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 1
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 1
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 1
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 1
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 1
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 2
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 2
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00237() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00238() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00239() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00241() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00242() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00243() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00244() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Copy");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00245() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Paste");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00246() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00247() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00248() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00249() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00250() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00251() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00252() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00253() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00254() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00255() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00256() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00257() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00258() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00259() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00260() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00261() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00262() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00263() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00264() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00265() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00266() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00267() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00268() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00269() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00270() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00271() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00272() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00273() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00274() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00275() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00276() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00277() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00278() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00279() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00280() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Copy");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00281() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Paste");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00282() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00283() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00284() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00285() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00286() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00287() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00288() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00289() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00290() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00291() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00292() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00293() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00294() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00295() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00296() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00297() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00298() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00299() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00300() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00301() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00302() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00303() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00304() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00305() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00306() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00307() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00308() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00309() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00310() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00311() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00312() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00313() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00314() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00315() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00316() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Copy");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00317() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Paste");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00318() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00319() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00320() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00321() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00322() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00323() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00324() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Copy");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00325() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Copy");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00326() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Copy");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00327() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Paste");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00328() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Paste");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00329() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Paste");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00330() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00331() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00332() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00333() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00334() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00335() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00336() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00337() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00338() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00339() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00340() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00341() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00342() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00343() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00344() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00345() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00346() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00347() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.AlertDialog
        handleMenuItemByText("Force Size");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00348() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00349() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Copy");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00350() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        handleMenuItemByText("Paste");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00351() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00352() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00353() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00354() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00355() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00356() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00357() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00358() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00359() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00360() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00361() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00362() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00363() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00364() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00365() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00366() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00367() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00368() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00369() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00370() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00371() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00372() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00373() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00374() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00375() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00376() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00377() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00378() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00379() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00380() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00381() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00382() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00383() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00384() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00385() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00386() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00387() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00388() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00389() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00390() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00391() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00392() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00393() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00394() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00395() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00396() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00397() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00398() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00399() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00400() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00401() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00402() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00403() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00404() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00405() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00406() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00407() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00408() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00409() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00410() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00411() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00412() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00413() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00414() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00415() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00416() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00417() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00418() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00419() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00420() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00421() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00422() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00423() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00424() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00425() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00426() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00427() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00428() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00429() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00430() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00431() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00432() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00433() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00434() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00435() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00436() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00437() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00438() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00439() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00440() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00441() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00442() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00443() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00444() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00445() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00446() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00447() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00448() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00449() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00450() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00451() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00452() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00453() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00454() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00455() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00456() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00457() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00458() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00459() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00460() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00461() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00462() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00463() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00464() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00465() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00466() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00467() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00468() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00469() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00470() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00471() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00472() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00473() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00474() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00475() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00476() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00477() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00478() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00479() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00480() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00481() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00482() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00483() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00484() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00485() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00486() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00487() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00488() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00489() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00490() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00491() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00492() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        handleMenuItemByText("Disconnect");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00493() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00494() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00495() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by color");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00496() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by name");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00497() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00498() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00499() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00500() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00501() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00502() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00503() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00504() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00505() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00506() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00507() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00508() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00509() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00510() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00511() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00512() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00513() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00514() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00515() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00516() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00517() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00518() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00519() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00520() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00521() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00522() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00523() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00524() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00525() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00526() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00527() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00528() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00529() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00530() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        handleMenuItemByText("Disconnect");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00531() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00532() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00533() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by color");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00534() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by name");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00535() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00536() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00537() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00538() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00539() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00540() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00541() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00542() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00543() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00544() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00545() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00546() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00547() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00548() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00549() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00550() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00551() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00552() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00553() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00554() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00555() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00556() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00557() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00558() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00559() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00560() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00561() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00562() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00563() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00564() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00565() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00566() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00567() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00568() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        handleMenuItemByText("Disconnect");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00569() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00570() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00571() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by color");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00572() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by name");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00573() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00574() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00575() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00576() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        handleMenuItemByText("Disconnect");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00577() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        handleMenuItemByText("Disconnect");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00578() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        handleMenuItemByText("Disconnect");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00579() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00580() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00581() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00582() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00583() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00584() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00585() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00586() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00587() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00588() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00589() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        handleMenuItemByText("Disconnect");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00590() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00591() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00592() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        handleMenuItemByText("Delete host");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00593() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by color");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00594() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by color");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00595() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by color");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00596() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by name");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00597() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by name");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00598() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by name");
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00599() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00600() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00601() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00602() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00603() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00604() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00605() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00606() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00607() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00608() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00609() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00610() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00611() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00612() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00613() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00614() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00615() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00616() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by color");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00617() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        handleMenuItemByText("Sort by name");
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00618() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00619() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00620() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00621() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00622() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00623() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00624() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00625() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00626() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00627() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00628() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00629() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00630() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00631() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00632() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00633() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00634() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00635() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00636() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00637() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00638() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00639() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00640() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00641() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00642() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00643() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00644() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00645() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00646() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00647() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00648() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00649() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00650() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00651() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00652() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00653() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00654() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00655() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00656() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00657() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00658() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00659() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00660() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00661() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00662() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00663() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00664() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00665() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00666() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00667() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00668() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00669() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00670() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00671() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00672() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        handleMenuItemByText("Edit port forward");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00673() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        handleMenuItemByText("Delete port forward");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00674() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00675() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00676() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00677() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00678() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00679() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00680() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00681() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00682() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00683() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00684() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00685() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.AlertDialog
        handleMenuItemByText("Add port forward");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PortForwardListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00686() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.goBack();
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00687() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00688() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00689() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00690() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00691() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00692() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00693() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00694() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00695() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00696() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00697() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00698() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00699() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00700() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00701() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00702() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00703() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00704() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00705() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00706() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Load key on start");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00707() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy public key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00708() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy private key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00709() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00710() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00711() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00712() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00713() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00714() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00715() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00716() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00717() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00718() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00719() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00720() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00721() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00722() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00723() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00724() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Load key on start");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00725() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy public key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00726() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy private key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00727() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00728() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00729() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00730() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00731() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00732() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00733() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00734() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00735() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00736() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00737() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00738() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00739() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00740() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00741() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00742() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Load key on start");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00743() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy public key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00744() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy private key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00745() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00746() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00747() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00748() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00749() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00750() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00751() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00752() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00753() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00754() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00755() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00756() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Load key on start");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00757() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Load key on start");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00758() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Load key on start");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00759() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy public key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00760() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy public key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00761() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy public key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00762() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy private key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00763() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy private key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00764() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy private key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00765() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00766() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00767() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00768() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00769() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00770() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00771() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00772() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00773() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Load key on start");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00774() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy public key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00775() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        handleMenuItemByText("Copy private key");
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00776() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00777() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.view.ContextMenu
        // Press POWER button
        Util.powerAndBack(solo);
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00778() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00779() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        handleMenuItemByText("Change password");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00780() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        handleMenuItemByText("Delete key");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00781() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00782() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00783() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00784() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00785() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00786() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00787() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00788() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00789() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00790() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00791() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00792() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00793() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00794() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00795() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00796() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.app.AlertDialog
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertDialog();
        // android.app.AlertDialog => android.app.AlertDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00797() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00798() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00799() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00800() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00801() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00802() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00803() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00804() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00805() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00806() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00807() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
        // org.connectbot.PubkeyListActivity => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00808() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.AlertDialog
        handleMenuItemByText("Import");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00809() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.AlertDialog
        handleMenuItemByText("Import");
        assertDialog();
        // android.app.AlertDialog => org.connectbot.PubkeyListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00810() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        solo.goBack();
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00811() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }

  public void testNeutralCycle00812() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Manage Pubkeys");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PubkeyListActivity, of length 3
        // org.connectbot.PubkeyListActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.connectbot.PubkeyListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.PubkeyListActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  public View getActionBarView() {
    // solo.sleep(2000);
    ArrayList<View> alViews = solo.getCurrentViews();
    for (View curView :alViews) {
      String className = curView.getClass().getName();
      if (className.endsWith("ActionBarContainer")) {
        return curView;
      }
    }
    return null;
  }

  private ArrayList<View> getActionBarItemsWithMenuButton() {
    ViewGroup ActionBarContainer = (ViewGroup) this.getActionBarView();
    ArrayList<View> ret = new ArrayList<View>();
    ViewGroup ActionMenuView = (ViewGroup) recursiveFindActionMenuView(ActionBarContainer);
    if (ActionMenuView == null) {
      // The ActionBar is empty. Should not happen
      return null;
    }
    for (int i = 0; i < ActionMenuView.getChildCount(); i++) {
      View curView = ActionMenuView.getChildAt(i);
      ret.add(curView);
    }
    return ret;
  }

  public ArrayList<View> getActionBarItems() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        ActionBarItems.remove(i);
        return ActionBarItems;
      }
    }
    return ActionBarItems;
  }

  public View getActionBarMenuButton() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        return curView;
      }
    }
    return null;
  }

  public View getActionBarItem(int index) {
    ArrayList<View> ActionBarItems = getActionBarItems();
    if (ActionBarItems == null) {
      // There is no ActionBar
      return null;
    }
    if (index < ActionBarItems.size()) {
      return ActionBarItems.get(index);
    } else {
      // Out of range
      return null;
    }
  }

  private View recursiveFindActionMenuView(View entryPoint) {
    String curClassName = "";
    curClassName = entryPoint.getClass().getName();
    if (curClassName.endsWith("ActionMenuView")) {
      return entryPoint;
    }
    // entryPoint is not an ActionMenuView
    if (entryPoint instanceof ViewGroup) {
      ViewGroup vgEntry = (ViewGroup)entryPoint;
      for ( int i = 0; i<vgEntry.getChildCount(); i ++) {
        View curView = vgEntry.getChildAt(i);
        View retView = recursiveFindActionMenuView(curView);

        if (retView != null) {
          // ActionMenuView was found
          return retView;
        }
      }
      // Still not found
      return null;
    } else {
      return null;
    }
  }

  public View getActionBarMenuItem(int index) {
    View ret = null;
    ArrayList<View> MenuItems = getActionBarMenuItems();
    if (MenuItems != null && index < MenuItems.size()) {
      ret = MenuItems.get(index);
    }
    return ret;
  }

  public ArrayList<View> getActionBarMenuItems() {
    ArrayList<View> MenuItems = new ArrayList<View>();
    ArrayList<View> curViews = solo.getCurrentViews();

    for (int i = 0; i < curViews.size(); i++) {
      View itemView = curViews.get(i);
      String className = itemView.getClass().getName();
      if (className.endsWith("ListMenuItemView")) {
        MenuItems.add(itemView);
      }
    }
    return MenuItems;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
  }
  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(2000);
    assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());
  }
  @SuppressWarnings("unchecked")
  public void handleMenuItemByText(String title) {
    View v = findViewByText(title);
    if (null != v) {
      // Menu item in option menu (or on action bar if no menu poped)
      // assertTrue("MenuItem: Not Enabled.", v.isEnabled());
      solo.clickOnText(title);
    } else {
      boolean hasMore = solo.searchText("More");
      if (hasMore) {
        solo.clickOnMenuItem("More");
        handleMenuItemByText(title);
        return;
      }
      // Menu item on action bar
      Class<? extends View> cls = null;
      try {
        cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
      ArrayList<? extends View> views = solo.getCurrentViews(cls);
      if (!views.isEmpty()) {
        solo.sendKey(KeyEvent.KEYCODE_MENU); // Hide option menu
        assertTrue("Menu Not Closed", solo.waitForDialogToClose());
      }
      View actionBarView = getActionBarView();
      assertNotNull("Action Bar Not Found", actionBarView);
      boolean onActionBar = false;
      for (View abv : getActionBarItems()) {
        for (View iv : solo.getViews(abv)) {
          if (iv instanceof TextView) {
            if (((TextView) iv).getText().toString().matches(title)) {
              onActionBar = true;
              assertTrue("MenuItem: Not Clickable.", iv.isClickable());
              solo.clickOnView(iv);
              break;
            }
          }
        }
        if (onActionBar) break;
      }
      if (!onActionBar) {
        // In action bar menu
        boolean found = false;
        View abMenuButton = getActionBarMenuButton();
        assertNotNull("Action Bar Menu Button Not Found", abMenuButton);
        solo.clickOnView(abMenuButton);
        assertTrue("Action Bar Not Open", solo.waitForDialogToOpen());
        ArrayList<View> acBarMIs = getActionBarMenuItems();
        for (View item : acBarMIs) {
          for (View iv : solo.getViews(item)) {
            if (iv instanceof TextView) {
              if (((TextView) iv).getText().toString().matches(title)) {
                found = true;
                assertTrue("MenuItem: Not Clickable.", iv.isClickable());
                solo.clickOnView(iv);
                break;
              }
            }
          }
          if (found) break;
        }
        assertTrue("MenuItem: not found.", found);
      }
    }
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
