#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript ConnectBot org.connectbot org.connectbot.test MainTestCase testHostListActivity,testPubkeyListActivity,testGeneratePubkeyActivity,testSettingsActivity,testHostEditorActivity,testPortForwardListActivity,testConsoleActivity,testHelpActivity,testHelpTopicActivity rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript ConnectBot org.connectbot org.connectbot.test MainTestCase testHostListActivity,testPubkeyListActivity,testGeneratePubkeyActivity,testSettingsActivity,testHostEditorActivity,testPortForwardListActivity,testConsoleActivity,testHelpActivity,testHelpTopicActivity home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript ConnectBot org.connectbot org.connectbot.test MainTestCase testHostListActivity,testPubkeyListActivity,testGeneratePubkeyActivity,testSettingsActivity,testHostEditorActivity,testPortForwardListActivity,testConsoleActivity,testHelpActivity,testHelpTopicActivity power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript ConnectBot org.connectbot org.connectbot.test BackButtonTestCase testHostListActivity,testPubkeyListActivity,testGeneratePubkeyActivity,testSettingsActivity,testHostEditorActivity,testPortForwardListActivity,testConsoleActivity,testHelpActivity,testHelpTopicActivity back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript ConnectBot org.connectbot org.connectbot.test SemanticTestCase testZoomInOut,testConnectDisconnect,testAddDeleteHost,testAddDeletePortForward,testAddDeletePubkey semantic.$N.action

