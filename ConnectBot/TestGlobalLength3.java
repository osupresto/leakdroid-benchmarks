/*
 * This file is automatically created by Gator.
 */

package org.connectbot.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password = "prestoosu";
  public TestGlobalLength3() {
    super(org.connectbot.HostListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 2
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 2
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.clickOnMenuItem("Port Forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // Priority: 0
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnMenuItem("Help");
    solo.clickOnButton("Hints");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HelpTopicActivity, of length 3
        // Priority: 0
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpTopicActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HelpTopicActivity.class);
        // org.connectbot.HelpTopicActivity => org.connectbot.HelpActivity
        solo.goBack();
        assertActivity(org.connectbot.HelpActivity.class);
        // org.connectbot.HelpActivity => org.connectbot.HelpTopicActivity
        solo.clickOnButton("(Hints|Keyboard)");
        assertActivity(org.connectbot.HelpTopicActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit host");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostEditorActivity, of length 3
        // Priority: 0
        // org.connectbot.HostEditorActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostEditorActivity
        solo.clickOnMenuItem("Edit host");
        assertActivity(org.connectbot.HostEditorActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostEditorActivity
        solo.clickOnMenuItem("Edit host");
        assertActivity(org.connectbot.HostEditorActivity.class);
        // org.connectbot.HostEditorActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.clickOnMenuItem("Edit port forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // Priority: 0
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // Priority: 0
        // org.connectbot.PortForwardListActivity => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        solo.clickOnMenuItem("Port Forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // Priority: 0
        // org.connectbot.PortForwardListActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        solo.clickOnMenuItem("Edit port forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
