/*
 * This file is automatically created by Gator.
 */

package org.connectbot.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBinderGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password = "RealPassword";
  public TestBinderGlobalLength3() {
    super(org.connectbot.HostListActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit host");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostEditorActivity, of length 3
        // org.connectbot.HostEditorActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostEditorActivity
        handleMenuItemByText("Edit host");
        assertActivity(org.connectbot.HostEditorActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 2
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 2
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickInList(1);
    solo.typeText(0, password);
    solo.sendKey(Solo.ENTER);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.ConsoleActivity, of length 3
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        handleMenuItemByText("Port Forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("No");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        solo.clickOnButton("Yes");
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // TODO: TOUCH THE SCREEN
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.ConsoleActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.ConsoleActivity
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.HostEditorActivity
        handleMenuItemByText("Edit host");
        assertActivity(org.connectbot.HostEditorActivity.class);
        // org.connectbot.HostEditorActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        handleMenuItemByText("Edit port forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
        // org.connectbot.PortForwardListActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // TODO
        solo.sendKey(0); // SPECIFY THE KEY
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.HostListActivity, of length 3
        // org.connectbot.HostListActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.connectbot.HostListActivity
        // Implicit Launch. BenchmarkName: ConnectBot
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => org.connectbot.HostListActivity
        Util.rotateOnce(solo);
        assertActivity(org.connectbot.HostListActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.ConsoleActivity
        solo.goBack();
        assertActivity(org.connectbot.ConsoleActivity.class);
        // org.connectbot.ConsoleActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.connectbot.PortForwardListActivity
        handleMenuItemByText("Port Forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.clickLongInList(1);
    solo.clickOnText("Edit port forwards");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.connectbot.PortForwardListActivity, of length 3
        // org.connectbot.PortForwardListActivity => org.connectbot.HostListActivity
        solo.goBack();
        assertActivity(org.connectbot.HostListActivity.class);
        // org.connectbot.HostListActivity => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => org.connectbot.PortForwardListActivity
        handleMenuItemByText("Edit port forwards");
        assertActivity(org.connectbot.PortForwardListActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  public View getActionBarView() {
    // solo.sleep(2000);
    ArrayList<View> alViews = solo.getCurrentViews();
    for (View curView :alViews) {
      String className = curView.getClass().getName();
      if (className.endsWith("ActionBarContainer")) {
        return curView;
      }
    }
    return null;
  }

  private ArrayList<View> getActionBarItemsWithMenuButton() {
    ViewGroup ActionBarContainer = (ViewGroup) this.getActionBarView();
    ArrayList<View> ret = new ArrayList<View>();
    ViewGroup ActionMenuView = (ViewGroup) recursiveFindActionMenuView(ActionBarContainer);
    if (ActionMenuView == null) {
      // The ActionBar is empty. Should not happen
      return null;
    }
    for (int i = 0; i < ActionMenuView.getChildCount(); i++) {
      View curView = ActionMenuView.getChildAt(i);
      ret.add(curView);
    }
    return ret;
  }

  public ArrayList<View> getActionBarItems() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        ActionBarItems.remove(i);
        return ActionBarItems;
      }
    }
    return ActionBarItems;
  }

  public View getActionBarMenuButton() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        return curView;
      }
    }
    return null;
  }

  public View getActionBarItem(int index) {
    ArrayList<View> ActionBarItems = getActionBarItems();
    if (ActionBarItems == null) {
      // There is no ActionBar
      return null;
    }
    if (index < ActionBarItems.size()) {
      return ActionBarItems.get(index);
    } else {
      // Out of range
      return null;
    }
  }

  private View recursiveFindActionMenuView(View entryPoint) {
    String curClassName = "";
    curClassName = entryPoint.getClass().getName();
    if (curClassName.endsWith("ActionMenuView")) {
      return entryPoint;
    }
    // entryPoint is not an ActionMenuView
    if (entryPoint instanceof ViewGroup) {
      ViewGroup vgEntry = (ViewGroup)entryPoint;
      for ( int i = 0; i<vgEntry.getChildCount(); i ++) {
        View curView = vgEntry.getChildAt(i);
        View retView = recursiveFindActionMenuView(curView);

        if (retView != null) {
          // ActionMenuView was found
          return retView;
        }
      }
      // Still not found
      return null;
    } else {
      return null;
    }
  }

  public View getActionBarMenuItem(int index) {
    View ret = null;
    ArrayList<View> MenuItems = getActionBarMenuItems();
    if (MenuItems != null && index < MenuItems.size()) {
      ret = MenuItems.get(index);
    }
    return ret;
  }

  public ArrayList<View> getActionBarMenuItems() {
    ArrayList<View> MenuItems = new ArrayList<View>();
    ArrayList<View> curViews = solo.getCurrentViews();

    for (int i = 0; i < curViews.size(); i++) {
      View itemView = curViews.get(i);
      String className = itemView.getClass().getName();
      if (className.endsWith("ListMenuItemView")) {
        MenuItems.add(itemView);
      }
    }
    return MenuItems;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
  }
  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  @SuppressWarnings("unchecked")
  public void handleMenuItemByText(String title) {
    View v = findViewByText(title);
    if (null != v) {
      // Menu item in option menu (or on action bar if no menu poped)
      // assertTrue("MenuItem: Not Enabled.", v.isEnabled());
      solo.clickOnText(title);
    } else {
      boolean hasMore = solo.searchText("More");
      if (hasMore) {
        solo.clickOnMenuItem("More");
        handleMenuItemByText(title);
        return;
      }
      // Menu item on action bar
      Class<? extends View> cls = null;
      try {
        cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
      ArrayList<? extends View> views = solo.getCurrentViews(cls);
      if (!views.isEmpty()) {
        solo.sendKey(KeyEvent.KEYCODE_MENU); // Hide option menu
        assertTrue("Menu Not Closed", solo.waitForDialogToClose());
      }
      View actionBarView = getActionBarView();
      assertNotNull("Action Bar Not Found", actionBarView);
      boolean onActionBar = false;
      for (View abv : getActionBarItems()) {
        for (View iv : solo.getViews(abv)) {
          if (iv instanceof TextView) {
            if (((TextView) iv).getText().toString().matches(title)) {
              onActionBar = true;
              assertTrue("MenuItem: Not Clickable.", iv.isClickable());
              solo.clickOnView(iv);
              break;
            }
          }
        }
        if (onActionBar) break;
      }
      if (!onActionBar) {
        // In action bar menu
        boolean found = false;
        View abMenuButton = getActionBarMenuButton();
        assertNotNull("Action Bar Menu Button Not Found", abMenuButton);
        solo.clickOnView(abMenuButton);
        assertTrue("Action Bar Not Open", solo.waitForDialogToOpen());
        ArrayList<View> acBarMIs = getActionBarMenuItems();
        for (View item : acBarMIs) {
          for (View iv : solo.getViews(item)) {
            if (iv instanceof TextView) {
              if (((TextView) iv).getText().toString().matches(title)) {
                found = true;
                assertTrue("MenuItem: Not Clickable.", iv.isClickable());
                solo.clickOnView(iv);
                break;
              }
            }
          }
          if (found) break;
        }
        assertTrue("MenuItem: not found.", found);
      }
    }
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
