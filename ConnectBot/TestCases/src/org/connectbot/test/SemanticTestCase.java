/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.connectbot.test;

import android.view.KeyEvent;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.GenericFunctor;

public class SemanticTestCase extends AmplifyTestCase {
	public SemanticTestCase() {
		super("org.connectbot", "org.connectbot.HostListActivity");
	}
	// zoom in/out
	public void testZoomInOut() {
		final String uri = "presto@acadia.cse.ohio-state.edu";	// this should be a real one
		clickOrCreateClick(uri);
		
		solo.assertCurrentActivity(getName(), "ConsoleActivity");
		solo.enterText(0, "1");
		solo.sendKey(Solo.ENTER);
		// END COPY of testConsoleActivity()
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {				
				solo.sendKey(KeyEvent.KEYCODE_VOLUME_UP);
				solo.sendKey(KeyEvent.KEYCODE_VOLUME_DOWN);
			}
		});
	}
	// connect/disconnect
	public void testConnectDisconnect() {
		final String uri = "presto@acadia.cse.ohio-state.edu";	// this should be a real one
		clickOrCreateClick(uri);
		
		solo.assertCurrentActivity(getName(), "ConsoleActivity");
		// END COPY of testConsoleActivity()
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {				
				solo.clickOnMenuItem("Disconnect");
				solo.clearEditText(0);
				solo.clickOnText(uri);
			}
		});
	}
	
	// add/delete host
	public void testAddDeleteHost() {
		final String uri = "a@b.c";
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnEditText(0);
				solo.enterText(0, uri);
				solo.sendKey(Solo.ENTER);
				solo.clickOnButton("Yes");
				
				solo.clearEditText(0);
				solo.clickLongOnText(uri);
				solo.clickOnText("Delete host");
				solo.clickOnButton("Yes, delete");
			}
		});		
	}
	
	// add/delete port forward
	public void testAddDeletePortForward() {
		String uri = "user@host.domain";
		clickOrCreateClick(uri);

		solo.clickOnButton("Yes");		
		solo.clearEditText(0);
		solo.clickLongOnText(uri);
		solo.clickOnText("Edit port forwards");
		solo.assertCurrentActivity(getName(), "PortForwardListActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Add port forward");
				solo.typeText(0, "abc");
				solo.typeText(1, "143");
				solo.typeText(2, "localhost:143");
				solo.clickOnButton("Create port forward");
				
				solo.clickLongOnText("abc");
				solo.clickOnText("Delete port forward");
				solo.clickOnText("Yes, delete");
			}
		});
	}
	
	// add/delete pubkey
	public void testAddDeletePubkey() {
		solo.clickOnMenuItem("Manage Pubkeys");
		
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Generate");		
				
				solo.enterText(0, "abc");
				solo.clickOnButton("Save");
				for (int i = 0; i < 10; i++) {
					solo.clickOnScreen(100, 290);
					solo.clickOnScreen(120, 300);
				}
				solo.waitForActivity("PubkeyListActivity");
				solo.clickLongOnText("abc");
				solo.clickOnText("Delete key");
				solo.clickOnText("Yes, delete");
			}
		});
		
	}
	
	private void clickOrCreateClick(String uri) {
        if (solo.searchText(uri)) {
            solo.clickOnText(uri);
        } else {
            solo.clickOnEditText(0);
            solo.enterText(0, uri);
            solo.sendKey(Solo.ENTER);
        }
    }
}
