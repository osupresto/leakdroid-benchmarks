/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.connectbot.test;

import pai.AmplifyTestCase;

import com.jayway.android.robotium.solo.Solo;

import android.annotation.TargetApi;

@TargetApi(3)
public class MainTestCase extends AmplifyTestCase {
	public static final String LOG_TAG = "PRESTO";

	public MainTestCase() {
		super("org.connectbot", "org.connectbot.HostListActivity");
	}

	public void testHostListActivity() {		
		solo.assertCurrentActivity(getName(), "HostListActivity");		
	}

	public void testPubkeyListActivity() {
		solo.clickOnMenuItem("Manage Pubkeys");
		solo.assertCurrentActivity(getName(), "PubkeyListActivity");		
	}
	
	public void testGeneratePubkeyActivity() {
		solo.clickOnMenuItem("Manage Pubkeys");
		solo.clickOnMenuItem("Generate");
		solo.assertCurrentActivity(getName(), "GeneratePubkeyActivity");		
	}
	
	public void testSettingsActivity() {
		solo.clickOnMenuItem("Settings");
		solo.assertCurrentActivity(getName(), "SettingsActivity");		
	}
	
	public void testHostEditorActivity() {		
		// setup
		String uri = "user@host.domain";
		clickOrCreateClick(uri);
		
		solo.assertCurrentActivity(getName(), "ConsoleActivity");		
		solo.clickOnButton("Yes");
		solo.assertCurrentActivity(getName(), "HostListActivity");
		
		// the key
		solo.clearEditText(0);
		solo.clickLongOnText(uri);
		solo.clickOnText("Edit host");
		solo.assertCurrentActivity(getName(), "HostEditorActivity");
	}
	
	public void testPortForwardListActivity() {
		// setup
		String uri = "user@host.domain";
		clickOrCreateClick(uri);

		solo.clickOnButton("Yes");

		// the key
		solo.clearEditText(0);
		solo.clickLongOnText(uri);
		solo.clickOnText("Edit port forwards");
		solo.assertCurrentActivity(getName(), "PortForwardListActivity");
	}
	
	public void testConsoleActivity() {
		String uri = "user@host.domain";
		clickOrCreateClick(uri);
		
		solo.assertCurrentActivity(getName(), "ConsoleActivity");
	}
	
	public void testHelpActivity() {
		solo.clickOnMenuItem("Help");
		solo.assertCurrentActivity(getName(), "HelpActivity");
	}
	
	public void testHelpTopicActivity() {
		solo.clickOnMenuItem("Help");
		solo.clickOnButton("Hints");
		solo.assertCurrentActivity(getName(), "HelpTopicActivity");
	}
	
	void clickOrCreateClick(String uri) {
		if (solo.searchText(uri)) {
			solo.clickOnText(uri);
		} else {
			solo.clickOnEditText(0);
			solo.enterText(0, uri);
			solo.sendKey(Solo.ENTER);
		}
	}
}
