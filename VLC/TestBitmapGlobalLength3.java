/*
 * This file is automatically created by Gator.
 */

package org.videolan.vlc.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.View;
import org.videolan.vlc.R;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBitmapGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestBitmapGlobalLength3() {
    super(org.videolan.vlc.gui.MainActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.Dialog
        // Implicit Launch. BenchmarkName: VLC
        assertDialog();
        // android.app.Dialog => org.videolan.vlc.gui.MainActivity
        solo.clickOnButton("OK");
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.Dialog
        // Implicit Launch. BenchmarkName: VLC
        assertDialog();
        // android.app.Dialog => org.videolan.vlc.gui.MainActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(2000);
    assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
