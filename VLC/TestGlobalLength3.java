/*
 * This file is automatically created by Gator.
 */

package org.videolan.vlc.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.view.View;
import org.videolan.vlc.R;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestGlobalLength3() {
    super(org.videolan.vlc.gui.MainActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.Dialog
        // Implicit Launch. BenchmarkName: VLC
        assertDialog();
        // android.app.Dialog => org.videolan.vlc.gui.MainActivity
        solo.clickOnButton("OK");
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => android.app.Dialog
        // Implicit Launch. BenchmarkName: VLC
        assertDialog();
        // android.app.Dialog => org.videolan.vlc.gui.MainActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        final View v_1 = solo.getView(R.id.player_overlay_backward);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        final View v_1 = solo.getView(R.id.player_overlay_backward);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        final View v_1 = solo.getView(R.id.player_overlay_backward);
        assertTrue("ImageButton: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 3
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
