#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript VLC org.videolan.vlc org.videolan.vlc.test MainTestCase testMainActivity,testSearchActivity,testAboutActivity,testPreferencesActivity,testBrowserActivity,testMediaInfoActivity,testVideoPlayerActivity,testAudioPlayerActivity old_rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript VLC org.videolan.vlc org.videolan.vlc.test MainTestCase testMainActivity,testSearchActivity,testAboutActivity,testPreferencesActivity,testBrowserActivity,testMediaInfoActivity,testVideoPlayerActivity,testAudioPlayerActivity home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript VLC org.videolan.vlc org.videolan.vlc.test MainTestCase testMainActivity,testSearchActivity,testAboutActivity,testPreferencesActivity,testBrowserActivity,testMediaInfoActivity,testVideoPlayerActivity,testAudioPlayerActivity power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript VLC org.videolan.vlc org.videolan.vlc.test BackButtonTestCase testMainActivity,testSearchActivity,testAboutActivity,testPreferencesActivity,testBrowserActivity,testMediaInfoActivity,testVideoPlayerActivity,testAudioPlayerActivity,testSearch2AudioBack,testSearch2VideoBack,testMediaInfo2VideoBack back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript VLC org.videolan.vlc org.videolan.vlc.test SemanticTestCase testSearchClear,testMainRefresh,testMainChangeSort,testMainChangeDisplay,testAboutChangeTab,testMainChangeTab semantic.$N.action

