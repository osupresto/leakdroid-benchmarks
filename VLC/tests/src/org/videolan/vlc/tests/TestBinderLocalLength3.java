/*
 * This file is automatically created by Gator.
 */

package org.videolan.vlc.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.view.View;
import org.videolan.vlc.R;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBinderLocalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestBinderLocalLength3() {
    super(org.videolan.vlc.gui.MainActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 1
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 1
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 1
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 1
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 1
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 1
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 1
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 1
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 1
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 1
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 1
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 1
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
