/*
 * This file is automatically created by Gator.
 */

package org.videolan.vlc.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.videolan.vlc.R;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBitmapLocalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestBitmapLocalLength3() {
    super(org.videolan.vlc.gui.MainActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    solo.clickOnText("Directories");
    assertActivity(org.videolan.vlc.gui.BrowserActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.BrowserActivity, of length 1
        // org.videolan.vlc.gui.BrowserActivity => org.videolan.vlc.gui.BrowserActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.BrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    solo.clickOnText("Directories");
    assertActivity(org.videolan.vlc.gui.BrowserActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.BrowserActivity, of length 1
        // org.videolan.vlc.gui.BrowserActivity => org.videolan.vlc.gui.BrowserActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.BrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    solo.clickOnText("Directories");
    assertActivity(org.videolan.vlc.gui.BrowserActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.BrowserActivity, of length 1
        // org.videolan.vlc.gui.BrowserActivity => org.videolan.vlc.gui.BrowserActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.BrowserActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 1
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickLongOnText("small test video*");
    solo.clickOnText("Information");
    assertActivity(org.videolan.vlc.gui.video.MediaInfoActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.MediaInfoActivity, of length 1
        // org.videolan.vlc.gui.video.MediaInfoActivity => org.videolan.vlc.gui.video.MediaInfoActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.video.MediaInfoActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
