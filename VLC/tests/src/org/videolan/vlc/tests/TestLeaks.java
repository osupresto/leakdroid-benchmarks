package org.videolan.vlc.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import org.videolan.vlc.R;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestLeaks extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestLeaks() {
    super(org.videolan.vlc.gui.MainActivity.class);
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("About");
    assertActivity(org.videolan.vlc.gui.AboutActivity.class);



    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.videolan.vlc.gui.AboutActivity, of length 1
        // org.videolan.vlc.gui.AboutActivity => org.videolan.vlc.gui.AboutActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.AboutActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    solo.typeText(0, "space");
    solo.sendKey(KeyEvent.KEYCODE_ENTER);
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        // solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        solo.clickOnText("small test video");
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00687() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    solo.typeText(0, "space");
    solo.sendKey(KeyEvent.KEYCODE_ENTER);
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        solo.clickOnText("small test video");
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00688() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    solo.typeText(0, "space");
    solo.sendKey(KeyEvent.KEYCODE_ENTER);
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        solo.clickOnText("small test video");
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

   /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(3000);
    // assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    // assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }
}