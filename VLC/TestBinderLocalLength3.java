/*
 * This file is automatically created by Gator.
 */

package org.videolan.vlc.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import org.videolan.vlc.R;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestBinderLocalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestBinderLocalLength3() {
    super(org.videolan.vlc.gui.MainActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 1
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 1
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 1
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 1
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 1
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 1
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 1
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 1
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 1
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 1
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 1
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 1
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 2
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 2
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 2
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 2
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 2
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 2
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 3
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
        // org.videolan.vlc.gui.MainActivity => org.videolan.vlc.gui.MainActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 3
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 3
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 3
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 3
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 3
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnMenuItem("Preferences");
    assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.PreferencesActivity, of length 3
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
        // org.videolan.vlc.gui.PreferencesActivity => org.videolan.vlc.gui.PreferencesActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.PreferencesActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        handleMenuItemByText("Clear search history");
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00237() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00238() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00239() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00241() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00242() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00243() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00244() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00245() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00246() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00247() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00248() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00249() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00250() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00251() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00252() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00253() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00254() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00255() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00256() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00257() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00258() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00259() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00260() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00261() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        handleMenuItemByText("Clear search history");
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00262() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00263() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00264() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00265() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00266() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00267() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00268() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00269() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00270() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00271() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00272() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00273() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00274() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00275() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00276() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00277() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00278() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00279() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00280() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00281() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00282() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00283() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00284() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00285() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00286() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00287() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00288() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00289() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00290() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00291() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00292() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00293() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00294() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00295() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        handleMenuItemByText("Clear search history");
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00296() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00297() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00298() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00299() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        handleMenuItemByText("Clear search history");
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00300() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        handleMenuItemByText("Clear search history");
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00301() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        handleMenuItemByText("Clear search history");
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00302() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00303() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00304() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00305() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00306() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00307() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00308() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00309() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00310() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00311() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00312() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.typeText(0, "some text");
        solo.sendKey(KeyEvent.KEYCODE_ENTER);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00313() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00314() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00315() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00316() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00317() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00318() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.SearchActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00319() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        handleMenuItemByText("Clear search history");
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00320() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00321() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00322() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 3
        // org.videolan.vlc.gui.SearchActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => org.videolan.vlc.gui.SearchActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00323() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00324() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00325() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00326() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00327() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00328() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00329() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00330() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00331() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00332() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00333() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00334() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00335() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00336() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00337() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00338() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00339() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00340() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00341() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00342() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00343() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00344() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00345() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00346() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00347() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00348() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00349() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00350() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00351() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00352() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00353() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00354() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00355() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00356() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00357() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00358() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00359() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00360() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00361() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00362() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00363() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00364() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00365() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00366() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00367() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00368() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00369() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00370() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00371() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00372() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00373() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00374() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00375() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00376() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00377() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00378() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00379() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00380() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00381() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00382() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00383() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00384() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00385() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00386() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00387() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00388() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00389() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00390() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00391() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00392() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00393() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00394() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00395() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00396() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00397() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00398() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00399() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00400() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00401() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00402() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00403() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00404() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00405() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00406() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00407() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00408() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00409() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00410() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00411() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00412() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00413() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00414() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00415() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00416() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00417() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00418() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00419() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00420() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00421() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00422() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00423() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00424() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00425() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00426() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00427() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00428() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00429() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00430() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00431() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00432() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00433() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00434() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.setProgressBar(0, 10); // MAKE SURE THESE ARE THE BAR AND VALUE EXPECTED
      }
    });
  }

  public void testNeutralCycle00435() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00436() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 3
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        Util.rotateOnce(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  public View getActionBarView() {
    // solo.sleep(2000);
    ArrayList<View> alViews = solo.getCurrentViews();
    for (View curView :alViews) {
      String className = curView.getClass().getName();
      if (className.endsWith("ActionBarContainer")) {
        return curView;
      }
    }
    return null;
  }

  private ArrayList<View> getActionBarItemsWithMenuButton() {
    ViewGroup ActionBarContainer = (ViewGroup) this.getActionBarView();
    ArrayList<View> ret = new ArrayList<View>();
    ViewGroup ActionMenuView = (ViewGroup) recursiveFindActionMenuView(ActionBarContainer);
    if (ActionMenuView == null) {
      // The ActionBar is empty. Should not happen
      return null;
    }
    for (int i = 0; i < ActionMenuView.getChildCount(); i++) {
      View curView = ActionMenuView.getChildAt(i);
      ret.add(curView);
    }
    return ret;
  }

  public ArrayList<View> getActionBarItems() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        ActionBarItems.remove(i);
        return ActionBarItems;
      }
    }
    return ActionBarItems;
  }

  public View getActionBarMenuButton() {
    ArrayList<View> ActionBarItems = getActionBarItemsWithMenuButton();
    if (ActionBarItems == null) {
      return null;
    }
    for (int i = 0; i < ActionBarItems.size(); i++) {
      View curView = ActionBarItems.get(i);
      String className = curView.getClass().getName();
      if (className.endsWith("OverflowMenuButton")) {
        return curView;
      }
    }
    return null;
  }

  public View getActionBarItem(int index) {
    ArrayList<View> ActionBarItems = getActionBarItems();
    if (ActionBarItems == null) {
      // There is no ActionBar
      return null;
    }
    if (index < ActionBarItems.size()) {
      return ActionBarItems.get(index);
    } else {
      // Out of range
      return null;
    }
  }

  private View recursiveFindActionMenuView(View entryPoint) {
    String curClassName = "";
    curClassName = entryPoint.getClass().getName();
    if (curClassName.endsWith("ActionMenuView")) {
      return entryPoint;
    }
    // entryPoint is not an ActionMenuView
    if (entryPoint instanceof ViewGroup) {
      ViewGroup vgEntry = (ViewGroup)entryPoint;
      for ( int i = 0; i<vgEntry.getChildCount(); i ++) {
        View curView = vgEntry.getChildAt(i);
        View retView = recursiveFindActionMenuView(curView);

        if (retView != null) {
          // ActionMenuView was found
          return retView;
        }
      }
      // Still not found
      return null;
    } else {
      return null;
    }
  }

  public View getActionBarMenuItem(int index) {
    View ret = null;
    ArrayList<View> MenuItems = getActionBarMenuItems();
    if (MenuItems != null && index < MenuItems.size()) {
      ret = MenuItems.get(index);
    }
    return ret;
  }

  public ArrayList<View> getActionBarMenuItems() {
    ArrayList<View> MenuItems = new ArrayList<View>();
    ArrayList<View> curViews = solo.getCurrentViews();

    for (int i = 0; i < curViews.size(); i++) {
      View itemView = curViews.get(i);
      String className = itemView.getClass().getName();
      if (className.endsWith("ListMenuItemView")) {
        MenuItems.add(itemView);
      }
    }
    return MenuItems;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(2000);
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());
  }
  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  @SuppressWarnings("unchecked")
  public void handleMenuItemByText(String title) {
    View v = findViewByText(title);
    if (null != v) {
      // Menu item in option menu (or on action bar if no menu poped)
      // assertTrue("MenuItem: Not Enabled.", v.isEnabled());
      solo.clickOnText(title);
    } else {
      boolean hasMore = solo.searchText("More");
      if (hasMore) {
        solo.clickOnMenuItem("More");
        handleMenuItemByText(title);
        return;
      }
      // Menu item on action bar
      Class<? extends View> cls = null;
      try {
        cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
      } catch (ClassNotFoundException e) {
        e.printStackTrace();
      }
      ArrayList<? extends View> views = solo.getCurrentViews(cls);
      if (!views.isEmpty()) {
        solo.sendKey(KeyEvent.KEYCODE_MENU); // Hide option menu
        assertTrue("Menu Not Closed", solo.waitForDialogToClose());
      }
      View actionBarView = getActionBarView();
      assertNotNull("Action Bar Not Found", actionBarView);
      boolean onActionBar = false;
      for (View abv : getActionBarItems()) {
        for (View iv : solo.getViews(abv)) {
          if (iv instanceof TextView) {
            if (((TextView) iv).getText().toString().matches(title)) {
              onActionBar = true;
              assertTrue("MenuItem: Not Clickable.", iv.isClickable());
              solo.clickOnView(iv);
              break;
            }
          }
        }
        if (onActionBar) break;
      }
      if (!onActionBar) {
        // In action bar menu
        boolean found = false;
        View abMenuButton = getActionBarMenuButton();
        assertNotNull("Action Bar Menu Button Not Found", abMenuButton);
        solo.clickOnView(abMenuButton);
        assertTrue("Action Bar Not Open", solo.waitForDialogToOpen());
        ArrayList<View> acBarMIs = getActionBarMenuItems();
        for (View item : acBarMIs) {
          for (View iv : solo.getViews(item)) {
            if (iv instanceof TextView) {
              if (((TextView) iv).getText().toString().matches(title)) {
                found = true;
                assertTrue("MenuItem: Not Clickable.", iv.isClickable());
                solo.clickOnView(iv);
                break;
              }
            }
          }
          if (found) break;
        }
        assertTrue("MenuItem: not found.", found);
      }
    }
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
  }

}
