/*
 * This file is automatically created by Gator.
 */

package org.videolan.vlc.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import org.videolan.vlc.R;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength2 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  public TestGlobalLength2() {
    super(org.videolan.vlc.gui.MainActivity.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.MainActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.MainActivity => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => org.videolan.vlc.gui.MainActivity
        // Implicit Launch. BenchmarkName: VLC
        assertActivity(org.videolan.vlc.gui.MainActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnView(solo.getView(R.id.ml_menu_search));
    assertActivity(org.videolan.vlc.gui.SearchActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.SearchActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Audio");
    solo.clickOnText("Death Grips");
    solo.clickOnText("Get Got");
    assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.audio.AudioPlayerActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.audio.AudioPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.audio.AudioPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.audio.AudioPlayerActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnText("OK");
    solo.clickOnText("Video");
    solo.clickOnText("small test video*");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> org.videolan.vlc.gui.video.VideoPlayerActivity, of length 2
        // Priority: 0
        // org.videolan.vlc.gui.video.VideoPlayerActivity => org.videolan.vlc.gui.SearchActivity
        solo.goBack();
        assertActivity(org.videolan.vlc.gui.SearchActivity.class);
        // org.videolan.vlc.gui.SearchActivity => org.videolan.vlc.gui.video.VideoPlayerActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(org.videolan.vlc.gui.video.VideoPlayerActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
