/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.videolan.vlc.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;

public class MainTestCase extends AmplifyTestCase {
	public MainTestCase() {
		super("org.videolan.vlc", "org.videolan.vlc.gui.MainActivity");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.clickOnButton("OK");
		
		solo.sendKey(Solo.MENU);
		if (solo.searchText("Media library")) {
			solo.clickOnText("Media library");
		} else {
			solo.goBack();
		}
	}
	
	public void testMainActivity() {
		solo.assertCurrentActivity(getName(), "MainActivity");
	}
	
	// NOTE: CompatErrorActivity is a fallback activity. Let's ignore it for now.
	
	public void testSearchActivity() {
		solo.clickOnImage(1);
		solo.assertCurrentActivity(getName(), "SearchActivity");
	}
	
	public void testAboutActivity() {
		solo.clickOnMenuItem("About");
		solo.assertCurrentActivity(getName(), "AboutActivity");
	}
	
	public void testPreferencesActivity() {
		solo.clickOnMenuItem("Preference");
		solo.assertCurrentActivity(getName(), "PreferencesActivity");
	}
	
	public void testBrowserActivity() {
		solo.clickOnMenuItem("Preference");
		solo.clickOnText("Directories");
		solo.assertCurrentActivity(getName(), "BrowserActivity");
	}
	
	// NOTE: VideoActivityGroup no longer exist

	// NOTE: VideoListFragment is a Fragment, and ignore fragments for now.

	public void testMediaInfoActivity() {
		solo.clickOnImage(3);
		solo.clickLongInList(1);
		solo.clickOnText("Information");
		solo.assertCurrentActivity(getName(), "MediaInfoActivity");
	}
	
	// NOTE: in rotation, the video pauses and resumes playing.
	// Another issue is that, when we do a large number of reps, the
	// video would have finished playing and the execution would go
	// back to a previous activity.
	public void testVideoPlayerActivity() {
		solo.clickOnImage(3);
		solo.clickInList(1);
		solo.sleep(3000);
		solo.assertCurrentActivity(getName(), "VideoPlayerActivity");
	}
	
	// NOTE: ignore fragments for now (AudioBrowserFragment, AudioListFragment).
		
	// NOTE: AudioActivityGroup no longer exist.

	// NOTE: after a few reps, it goes back automatically to a previous activity.
	// And rotation would stop when that happens. Let's think about what to do
	// for this later.
	public void testAudioPlayerActivity() {
		solo.clickOnImage(4);
		solo.clickInList(1);
		solo.clickInList(1);
		solo.assertCurrentActivity(getName(), "AudioPlayerActivity");
	}	
}
