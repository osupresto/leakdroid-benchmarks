/* 
 * BackButtonTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.videolan.vlc.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.GenericFunctor;

public class BackButtonTestCase extends AmplifyTestCase {
	public BackButtonTestCase() {
		super("org.videolan.vlc", "org.videolan.vlc.gui.MainActivity");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.clickOnButton("OK");
		
		solo.sendKey(Solo.MENU);
		if (solo.searchText("Media library")) {
			solo.clickOnText("Media library");
		} else {
			solo.goBack();
		}
	}

	public void testMainActivity() {
		solo.assertCurrentActivity(getName(), "MainActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.assertCurrentActivity(getName(), "MainActivity");
			}
			
		});
	}
	
	public void testSearchActivity() {
		solo.clickOnImage(1);
		solo.assertCurrentActivity(getName(), "SearchActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnImage(1);
				solo.assertCurrentActivity(getName(), "SearchActivity");
			}
			
		});
	}
	
	public void testAboutActivity() {
		solo.clickOnMenuItem("About");
		solo.assertCurrentActivity(getName(), "AboutActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("About");
				solo.assertCurrentActivity(getName(), "AboutActivity");
			}
			
		});
	}
	
	public void testPreferencesActivity() {
		solo.clickOnMenuItem("Preference");
		solo.assertCurrentActivity(getName(), "PreferencesActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Preference");
				solo.assertCurrentActivity(getName(), "PreferencesActivity");
			}
			
		});
	}
	
	public void testBrowserActivity() {
		solo.clickOnMenuItem("Preference");
		solo.clickOnText("Directories");
		solo.assertCurrentActivity(getName(), "BrowserActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Directories");
				solo.assertCurrentActivity(getName(), "BrowserActivity");
			}
			
		});
	}
	
	public void testMediaInfoActivity() {
		solo.clickOnImage(3);
		solo.clickLongInList(1);
		solo.clickOnText("Information");
		solo.assertCurrentActivity(getName(), "MediaInfoActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();				
				solo.clickLongInList(1);
				solo.clickOnText("Information");
				solo.assertCurrentActivity(getName(), "MediaInfoActivity");
			}
			
		});
	}
	
	public void testVideoPlayerActivity() {
		solo.clickOnImage(3);
		solo.clickInList(1);
		solo.sleep(3000);
		solo.assertCurrentActivity(getName(), "VideoPlayerActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickInList(1);
				solo.sleep(3000);
				solo.assertCurrentActivity(getName(), "VideoPlayerActivity");
			}
			
		});
	}
	
	public void testAudioPlayerActivity() {
		solo.clickOnImage(4);
		solo.clickInList(1);
		solo.clickInList(1);
		solo.assertCurrentActivity(getName(), "AudioPlayerActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickInList(1);
				solo.assertCurrentActivity(getName(), "AudioPlayerActivity");
			}
			
		});
	}
	
	// NOTE: don't run because it ANRs, but included in counting
	public void testSearch2AudioBack() {
		solo.clickOnImage(1);
		solo.clickOnEditText(0);
		solo.enterText(0, "test");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("test.mp3");
				solo.sleep(500);
				solo.goBack();
			}
		});
	}

	public void testSearch2VideoBack() {
		solo.clickOnImage(1);
		solo.clickOnEditText(0);
		solo.enterText(0, "test");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("test.mp4");
				solo.sleep(2000);
				solo.goBack();
			}
		});
	}
	
	public void testMediaInfo2VideoBack() {
		solo.clickOnImage(3);
		solo.clickLongInList(1);
		solo.clickOnText("Information");
		solo.sleep(2500);
		solo.assertCurrentActivity(getName(), "MediaInfoActivity");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnImageButton(0);
				solo.sleep(2000);
				solo.goBack();
			}
			
		});
	}
}
