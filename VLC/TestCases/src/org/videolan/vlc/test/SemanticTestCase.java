/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package org.videolan.vlc.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.GenericFunctor;

public class SemanticTestCase extends AmplifyTestCase {
	public SemanticTestCase() {
		super("org.videolan.vlc", "org.videolan.vlc.gui.MainActivity");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.clickOnButton("OK");
		
		solo.sendKey(Solo.MENU);
		if (solo.searchText("Media library")) {
			solo.clickOnText("Media library");
		} else {
			solo.goBack();
		}
	}

	public void testSearchClear() {
		solo.clickOnImage(1);
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Clear search history");
			}
		});
	}
	
	public void testMainRefresh() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Refresh");
			}
		});
	}
	
	public void testMainChangeSort() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Sort by�");
				solo.clickOnText("Name");
				solo.clickOnMenuItem("Sort by�");
				solo.clickOnText("Length");
			}
		});
	}
	
	public void testMainChangeDisplay() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Directories");
				solo.clickOnMenuItem("Media library");
			}
		});
	}
	
	public void testAboutChangeTab() {
		solo.clickOnMenuItem("About");
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnText("Licence");
				solo.sleep(1500);
				solo.clickOnText("About");
				solo.sleep(1500);
			}
		});
	}
	
	public void testMainChangeTab() {
		this.specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnImage(3);
				solo.sleep(500);
				solo.clickOnImage(4);
				solo.sleep(500);
			}
		});
	}	
}
