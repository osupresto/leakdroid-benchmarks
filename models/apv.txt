# Number of nodes
4
# Node: name, label
n1,ChooseFileActivity
n2,OpenFileActivity
n3,Options
n4,AboutPDFViewActivity
# Start activity
n1
# Number of edges
20
# Edge: src node name, tgt node name, Transition
# Transition: widget->action [; widget->action ...]
n1,n2,list:0->select:?
n2,n1,btn:BACK->click
n1,n1,list:0->select:?,1
n2,n2,image:0->click,2
n2,n2,image:2->click,2
n2,n2,menu:More->click;text:Find...->click;textBox:0->enter:?;btn:Find->click,1
n2,n2,menu:More->click;text:Find...->click;textBox:0->enter:?;btn:Find->click;btn:Prev->click;btn:Next->click,1
n2,n2,menu:More->click;text:Find...->click;textBox:0->enter:?;btn:Find->click;btn:Hide->click,1
n2,n2,menu:Go to page...->click;textBox:0->enter:?;btn:Go->click,1
n2,n2,menu:Rotate left->click,3
n2,n2,menu:Rotate right->click,3
n1,n3,menu:Options->click
n3,n1,btn:BACK->click
n1,n4,menu:About->click
n4,n1,btn:BACK->click
n2,n3,menu:Options->click
n3,n2,btn:BACK->click
n2,n4,menu:More->click;text:About->click
n4,n2,btn:BACK->click
n1,n1,menu:Set current folder as home->click,1
