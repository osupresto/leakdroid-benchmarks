/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.fsck.k9.test;

import pai.AmplifyTestCase;
import pai.GenericFunctor;

import com.jayway.android.robotium.solo.Solo;

public class SemanticTestCase extends AmplifyTestCase {

	private static final String EMAIL = "temp.presto@gmail.com";
	private static final String PWD = "wikipedia";
	
	private static final String TEST_EMAIL = "android.presto@gmail.com";
//	private static final String TEST_PWD = "connectbot";
	
	public SemanticTestCase() {
		super("com.fsck.k9", "com.android.email.activity.Welcome");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.sendKey(Solo.MENU);
		if (!solo.searchText("Accounts")) {
			solo.sendKey(Solo.MENU);
			solo.clickLongOnText(EMAIL);
			solo.clickOnText("Remove account");
			solo.clickOnButton("OK");
			solo.clickOnText(TEST_EMAIL);
		} else {
			solo.sendKey(Solo.MENU);
		}
		solo.sleep(2000);
	}
	
	//
	public void testMessageComposeDiscard() {
		
		//solo.assertCurrentActivity(getName(), "MessageCompose");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Compose");
				solo.clickOnMenuItem("Discard");
			}
		});
	}
	
	public void testAccountsMessageComposeDiscard() {
		solo.clickOnMenuItem("Accounts");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Compose");
				solo.clickOnMenuItem("Discard");
			}
		});
	}

	public void testReverseSort() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Reverse sort");
				solo.clickOnMenuItem("Reverse sort");
			}
		});
	}
	
	public void testChangeSortType() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Sort by...");
				solo.clickOnText("Date");
				solo.clickOnMenuItem("Sort by...");
				solo.clickOnText("Subject");
			}
		});
	}
	
	public void testListFolders() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("More");
				solo.clickOnText("List folders");
			}
		});
	}
	
	public void testEmptyTrash() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("More");
				solo.clickOnText("Empty Trash");
			}
		});
	}
	
	public void testCompact() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("More");
				solo.clickOnText("Compact");
				solo.sleep(2000);
			}
		});
	}
	
	public void testCheckMail() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Check mail");
				solo.sleep(5000);
			}
		});
	}
	
	public void testMarkAllAsRead() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.sleep(2000);
				solo.clickLongOnText("Inbox");
				solo.clickInList(0);
				solo.clickOnButton(0);
			}
		});
	}
	
	public void testAccountListAndAccount() {
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Accounts");
				solo.sleep(1000);
				solo.clickInList(0);
				solo.sleep(1000);
			}
		});
	}
	
	public void testAddRemoveAccount() {
		solo.clickOnMenuItem("Accounts");
		
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Add account");
				solo.enterText(0, EMAIL);
				solo.enterText(1, PWD);
				solo.clickOnButton("Next");
				solo.waitForActivity("AccountSetupNames");
				solo.clickOnButton("Done");
				solo.clickOnMenuItem("Accounts");
				solo.clickLongOnText(EMAIL);
				solo.clickOnText("Remove account");
				solo.clickOnButton(0);
			}
		});
	}
}
