/* 
 * BackButtonTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.fsck.k9.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.AmplifyUtils;
import pai.GenericFunctor;

public class BackButtonTestCase extends AmplifyTestCase {
	private static final String EMAIL = "temp.presto@gmail.com";
	private static final String PWD = "wikipedia";
	
	private static final String TEST_EMAIL = "android.presto@gmail.com";
//	private static final String TEST_PWD = "connectbot";
	
	public BackButtonTestCase() {
		super("com.fsck.k9", "com.android.email.activity.Welcome");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.sendKey(Solo.MENU);
		if (!solo.searchText("Accounts")) {
			solo.sendKey(Solo.MENU);
			while (solo.searchText(EMAIL)) {
				solo.clickLongOnText(EMAIL);
				solo.clickOnText("Remove account");
				solo.clickOnButton("OK");
			}
			solo.clickOnText(TEST_EMAIL);
		} else {
			solo.sendKey(Solo.MENU);
		}
		solo.sleep(2000);
	}
	
	public void testAccountSetupBasics() {
		solo.clickOnMenuItem("Accounts");
		solo.clickOnMenuItem("Add account");
		solo.assertCurrentActivity(getName(), "AccountSetupBasics");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Add account");
				solo.assertCurrentActivity(getName(), "AccountSetupBasics");
			}
		});
	}

	public void testAccountSetupAccountType() {
		solo.clickOnMenuItem("Accounts");
		solo.clickOnMenuItem("Add account");
		solo.enterText(0, EMAIL);
		solo.enterText(1, PWD);
		solo.clickOnButton("Manual setup");
		solo.assertCurrentActivity(getName(), "AccountSetupAccountType");
		
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Add account");
				solo.enterText(0, EMAIL);
				solo.enterText(1, PWD);
				solo.clickOnButton("Manual setup");
				solo.assertCurrentActivity(getName(), "AccountSetupAccountType");
			}
		});
	}
	
	public void testAccountSetupIncoming() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.clickOnText("Incoming settings");
		solo.assertCurrentActivity(getName(), "AccountSetupIncoming");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Incoming settings");
				solo.assertCurrentActivity(getName(), "AccountSetupIncoming");
			}
		});
	}
	
	public void testAccountSetupComposition() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.clickOnText("Composing messages");
		solo.assertCurrentActivity(getName(), "AccountSetupComposition");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Composing messages");
				solo.assertCurrentActivity(getName(), "AccountSetupComposition");
			}
		});
	}
	
	public void testAccountSetupOutgoing() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.clickOnText("Outgoing settings");
		solo.assertCurrentActivity(getName(), "AccountSetupOutgoing");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText("Outgoing settings");
				solo.assertCurrentActivity(getName(), "AccountSetupOutgoing");
			}
		});
	}
	
	public void testAccountSetupOptions() {
		solo.clickOnMenuItem("Accounts");
		
		final GenericFunctor suffix = new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Add account");
				solo.enterText(0, EMAIL);
				solo.enterText(1, PWD);
				solo.clickOnButton("Manual setup");
				solo.clickOnButton("POP3 account");
				solo.clickOnButton("Next");
				solo.clickOnButton("Continue");
				solo.clickOnButton("Next");
				solo.clickOnButton("Continue");
				solo.assertCurrentActivity(getName(), "AccountSetupOptions");
			}
		};

		suffix.doIt(solo);
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				suffix.doIt(arg);
			}
		});
	}

	public void testAccountSetupNames() {
		solo.clickOnMenuItem("Accounts");
		final GenericFunctor suffix = new GenericFunctor() {
			public void doIt(Object arg) {				
				solo.clickOnMenuItem("Add account");
				solo.enterText(0, EMAIL);
				solo.enterText(1, PWD);
				solo.clickOnButton("Next");
				solo.waitForActivity("AccountSetupNames");
				solo.assertCurrentActivity(getName(), "AccountSetupNames");
			}
		};
		suffix.doIt(solo);
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				suffix.doIt(arg);
			}
		});
	}
	
	public void testChooseFolder() {
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.ENTER);
		final GenericFunctor suffix = new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("More");
				solo.clickOnText("Copy");
				solo.assertCurrentActivity(getName(), "ChooseFolder");
			}
		};
		suffix.doIt(solo);
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				suffix.doIt(arg);
			}
		});
	}

	public void testAccountSettings() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.assertCurrentActivity(getName(), "AccountSettings");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("More");
				solo.clickOnText("Account settings");
				solo.assertCurrentActivity(getName(), "AccountSettings");
			}
		});
	}
	
	public void testFolderSettings() {
		solo.clickLongOnText("Inbox");
		solo.clickOnText("Folder settings");
		solo.assertCurrentActivity(getName(), "FolderSettings");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickLongOnText("Inbox");
				solo.clickOnText("Folder settings");
				solo.assertCurrentActivity(getName(), "FolderSettings");
			}
		});
	}
	
	public void testAccounts() {
		solo.clickOnMenuItem("Accounts");
		solo.assertCurrentActivity(getName(), "Accounts");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				AmplifyUtils.leaveAndBack(solo);
				solo.clickOnMenuItem("Accounts");
				solo.assertCurrentActivity(getName(), "Accounts");
			}
		});
	}
	
	public void testFolderMessageList() {
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.ENTER);
		solo.goBack();
		
		solo.assertCurrentActivity(getName(), "FolderMessageList");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				AmplifyUtils.leaveAndBack(solo);
				solo.assertCurrentActivity(getName(), "FolderMessageList");
			}
		});
	}
	
	public void testMessageView() {
		
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.ENTER);
		solo.assertCurrentActivity(getName(), "MessageView");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.sendKey(Solo.ENTER);
				solo.assertCurrentActivity(getName(), "MessageView");
			}
		});
	}
	
	public void testMessageCompose() {
		solo.clickOnMenuItem("Compose");
		solo.assertCurrentActivity(getName(), "MessageCompose");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnMenuItem("Compose");
				solo.assertCurrentActivity(getName(), "MessageCompose");
			}
		});
	}
	
	public void testAccountSetupChecking() {
		solo.clickOnMenuItem("Accounts");
		solo.clickOnMenuItem("Add account");
		solo.enterText(0, EMAIL);
		solo.enterText(1, PWD);
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnButton("Next");
				solo.goBack();
			}
		});
	}
	public void testAccountsIncomingSettings() {
		solo.clickOnMenuItem("Accounts");
		
		
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Add account");
				solo.enterText(0, EMAIL);
				solo.enterText(1, PWD);
				solo.clickOnButton("Manual setup");
				solo.clickOnButton(0);
				solo.assertCurrentActivity(getName(), "AccountSetupIncoming");
				solo.goBack();
			}
		});
	}
	
	public void testAccountsOutgoingSettings() {
		solo.clickOnMenuItem("Accounts");
		
		
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Add account");
				solo.enterText(0, EMAIL);
				solo.enterText(1, PWD);
				solo.clickOnButton("Manual setup");
				solo.clickOnButton(0);
				solo.clickOnButton("Next");
				solo.clickOnButton("Continue");
				solo.assertCurrentActivity(getName(), "AccountSetupOutgoing");
				solo.goBack();
			}
		});
	}
	
	public void testAccountsMessageCompose() {
		solo.clickOnMenuItem("Accounts");
		specifyAmplifyFunctor(new GenericFunctor() {
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Compose");
				solo.goBack();
			}
		});
	}
}
