/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.fsck.k9.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;

/*
 *  Pre-requisite:
 *  		* Setup and only setup the account android.presto@gmail.com/connectbot
 *  		* View that account at least once before any test.
 *  		* If we create new accounts in the test, delete them.
 */

public class MainTestCase extends AmplifyTestCase {

	private static final String EMAIL = "temp.presto@gmail.com";
	private static final String PWD = "wikipedia";
	
	private static final String TEST_EMAIL = "android.presto@gmail.com";
//	private static final String TEST_PWD = "connectbot";
	
	public MainTestCase() {
		super("com.fsck.k9", "com.android.email.activity.Welcome");
	}
	
	public void setUp() throws Exception {
		super.setUp();
		solo.sendKey(Solo.MENU);
		if (!solo.searchText("Accounts")) {
			solo.sendKey(Solo.MENU);
			solo.clickLongOnText(EMAIL);
			solo.clickOnText("Remove account");
			solo.clickOnButton("OK");
			solo.clickOnText(TEST_EMAIL);
		} else {
			solo.sendKey(Solo.MENU);
		}
		solo.sleep(2000);
	}
	
	// NOTE: this is a wrapper that should be excluded.
//	public void testWelcome() {
//		solo.assertCurrentActivity(getName(), "Welcome");
//	}
	
	public void testAccountSetupBasics() {
		solo.clickOnMenuItem("Accounts");
		solo.assertCurrentActivity(getName(), "Accounts");
		solo.clickOnMenuItem("Add account");
		solo.assertCurrentActivity(getName(), "AccountSetupBasics");
	}

	public void testAccountSetupAccountType() {
		solo.clickOnMenuItem("Accounts");
		solo.assertCurrentActivity(getName(), "Accounts");
		solo.clickOnMenuItem("Add account");
		solo.assertCurrentActivity(getName(), "AccountSetupBasics");
		solo.enterText(0, EMAIL);
		solo.enterText(1, PWD);
		solo.clickOnButton("Manual setup");
		solo.assertCurrentActivity(getName(), "AccountSetupAccountType");
	}
	
	public void testAccountSetupIncoming() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.assertCurrentActivity(getName(), "AccountSettings");
		solo.clickOnText("Incoming settings");
		solo.assertCurrentActivity(getName(), "AccountSetupIncoming");
	}
	
	public void testAccountSetupComposition() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.assertCurrentActivity(getName(), "AccountSettings");
		solo.clickOnText("Composing messages");
		solo.assertCurrentActivity(getName(), "AccountSetupComposition");
	}
	
	public void testAccountSetupOutgoing() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.assertCurrentActivity(getName(), "AccountSettings");
		solo.clickOnText("Outgoing settings");
		solo.assertCurrentActivity(getName(), "AccountSetupOutgoing");
	}
	
	public void testAccountSetupOptions() {
		solo.clickOnMenuItem("Accounts");
		solo.assertCurrentActivity(getName(), "Accounts");
		solo.clickOnMenuItem("Add account");
		solo.assertCurrentActivity(getName(), "AccountSetupBasics");
		solo.enterText(0, EMAIL);
		solo.enterText(1, PWD);
		solo.clickOnButton("Manual setup");
		solo.assertCurrentActivity(getName(), "AccountSetupAccountType");
		solo.clickOnButton("POP3 account");
		solo.clickOnButton("Next");
		solo.clickOnButton("Continue");
		solo.clickOnButton("Next");
		solo.clickOnButton("Continue");
		solo.assertCurrentActivity(getName(), "AccountSetupOptions");
	}
	
	public void testAccountSetupNames() {
		solo.clickOnMenuItem("Accounts");
		solo.assertCurrentActivity(getName(), "Accounts");
		solo.clickOnMenuItem("Add account");
		solo.assertCurrentActivity(getName(), "AccountSetupBasics");
		solo.enterText(0, EMAIL);
		solo.enterText(1, PWD);
		solo.clickOnButton("Next");
		solo.assertCurrentActivity(getName(), "AccountSetupCheckSettings");
		solo.waitForActivity("AccountSetupNames");
		solo.assertCurrentActivity(getName(), "AccountSetupNames");
	}
	
	public void testChooseFolder() {
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.ENTER);
		solo.assertCurrentActivity(getName(), "MessageView");
		solo.clickOnMenuItem("More");
		solo.clickOnText("Copy");
		solo.assertCurrentActivity(getName(), "ChooseFolder");
	}
	
	// NOTE: this is transient. So discard it for amplification
//	public void testAccountSetupCheckSettings() {
//		solo.clickOnMenuItem("Accounts");
//		solo.assertCurrentActivity(getName(), "Accounts");
//		solo.clickOnMenuItem("Add account");
//		solo.assertCurrentActivity(getName(), "AccountSetupBasics");
//		solo.enterText(0, EMAIL);
//		solo.enterText(1, PWD);
//		solo.clickOnButton("Next");
//		solo.assertCurrentActivity(getName(), "AccountSetupCheckSettings");
//	}
	
	public void testAccountSettings() {
		solo.clickOnMenuItem("More");
		solo.clickOnText("Account settings");
		solo.assertCurrentActivity(getName(), "AccountSettings");
	}
	
	public void testFolderSettings() {
		solo.clickLongOnText("Inbox");
		solo.clickOnText("Folder settings");
		solo.assertCurrentActivity(getName(), "FolderSettings");
	}

	public void testAccounts() {
		solo.clickOnMenuItem("Accounts");
		solo.assertCurrentActivity(getName(), "Accounts");
	}
	
	public void testFolderMessageList() {
		this.setRotateDelay(3000);
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.ENTER);
		solo.assertCurrentActivity(getName(), "MessageView");
		solo.goBack();
		
		solo.assertCurrentActivity(getName(), "FolderMessageList");
	}
	
	public void testMessageView() {
		
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.DOWN);
		solo.sendKey(Solo.ENTER);
		solo.assertCurrentActivity(getName(), "MessageView");
	}
	
	public void testMessageCompose() {
		solo.clickOnMenuItem("Compose");
		solo.assertCurrentActivity(getName(), "MessageCompose");
	}
}
