/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import com.fsck.k9.R;
import android.view.View;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompChooseFolder extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestCompChooseFolder() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.MessageView
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.MessageView
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.MessageView
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.MessageView
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 3
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
