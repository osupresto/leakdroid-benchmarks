/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompAccountSetupCheckSettings extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestCompAccountSetupCheckSettings() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
