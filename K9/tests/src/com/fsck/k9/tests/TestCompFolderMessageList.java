/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompFolderMessageList extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestCompFolderMessageList() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.ChooseFolder
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Sort by...");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Reverse sort");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("List folders");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Sort by...");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Reverse sort");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("List folders");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Sort by...");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Reverse sort");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("List folders");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.MessageCompose
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.MessageCompose
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.MessageCompose
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00237() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00238() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00239() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00241() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00242() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Sort by...");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00243() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Sort by...");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00244() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Sort by...");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00245() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Reverse sort");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00246() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Reverse sort");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00247() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Reverse sort");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00248() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("List folders");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00249() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("List folders");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00250() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("List folders");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00251() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00252() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00253() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00254() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00255() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00256() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00257() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00258() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00259() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00260() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00261() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00262() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00263() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00264() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Sort by...");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00265() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Reverse sort");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00266() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("List folders");
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00267() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00268() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00269() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 3
        // Priority: 0
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
