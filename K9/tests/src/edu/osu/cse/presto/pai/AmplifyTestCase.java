/* 
 * AmplifyTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package edu.osu.cse.presto.pai;

import com.robotium.solo.Solo;

import android.os.Debug;
import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

@SuppressWarnings("rawtypes")
public class AmplifyTestCase extends ActivityInstrumentationTestCase2 {
	private static final String LOG_TAG = "AmplifyTestCase";
	protected Solo solo;
	private AmplifyAction amplifyAction;	
	private Thread timerThread;

	private boolean mtdProfile = false;
	protected void turnOnMtdProfile() { mtdProfile = true; }

	@SuppressWarnings({ "unchecked" })
	public AmplifyTestCase(String appId, Class cls) {
		super(appId, cls);
	    amplifyAction = new AmplifyAction(500);
	}

	public void specifyAmplifyFunctor(GenericFunctor func) {
		amplifyAction.setFunc(func);
		solo.sleep(2000);
	}

	public void setUp() throws Exception {
		Runtime runtime = Runtime.getRuntime();
		long max = runtime.maxMemory();
		Log.i("PRESTO", "Dalvik Heap Size: " + max);
		solo = new Solo(this.getInstrumentation(), this.getActivity());
		timerThread = new Thread() {
			public void run() {
				try {
					Thread.sleep(10 * 3600 * 1000);	// run at most 10 hours
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				Log.w(LOG_TAG, "Test case taking too long to finish.");				
				throw new TimeoutException();
			}
		};
		timerThread.start();
	}

	public void tearDown() throws Exception {
		this.getInstrumentation().waitForIdleSync();
		//		solo.waitForIdleSync();
		String cmd = CommandExecutor.STAT + " BEGIN";
		CommandExecutor.execute(cmd);
		if (mtdProfile) Debug.startMethodTracing();
		amplifyAction.execute(solo);
		if (mtdProfile) Debug.stopMethodTracing();

		cmd = CommandExecutor.STAT + " END";
		CommandExecutor.execute(cmd);

		solo.finishOpenedActivities();
		super.tearDown();
	}

	private static Class getClass(String cls) {
		try {
			return Class.forName(cls);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
