/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompWelcome extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestCompWelcome() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 2
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 2
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 2
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 2
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 2
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 2
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 3
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 3
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 3
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 3
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 3
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Welcome, of length 3
        // Priority: 0
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
        // com.android.email.activity.Welcome => com.android.email.activity.Welcome
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Welcome.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
