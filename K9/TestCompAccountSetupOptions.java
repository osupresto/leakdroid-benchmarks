/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompAccountSetupOptions extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestCompAccountSetupOptions() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOptions
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
