/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.widget.TextView;
import android.view.KeyEvent;
import com.fsck.k9.R;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestAccounts extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestAccounts() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 1
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 1
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 1
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 1
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Debug
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Clear pending actions (danger!)");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Compose");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Debug
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Clear pending actions (danger!)");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Compose");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Debug
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Debug
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Debug
        solo.clickOnView(solo.getView(R.id.debug_logging));
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Debug
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Debug
        solo.clickOnView(solo.getView(R.id.sensitive_logging));
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.Accounts
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupAccountType
        solo.clickOnButton("Manual setup");
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.Accounts
        solo.clickOnButton("POP3 account");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupAccountType
        solo.clickOnButton("Manual setup");
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.Accounts
        solo.clickOnButton("IMAP account");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupAccountType
        solo.clickOnButton("Manual setup");
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.Accounts
        solo.clickOnButton("WebDav(Exchange) account");
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupAccountType
        solo.clickOnButton("Manual setup");
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupNames
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupNames.class);
        // com.android.email.activity.setup.AccountSetupNames => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => android.app.AlertDialog
        solo.clickOnButton("Next");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Empty Trash");
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Compact");
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Clear pending actions (danger!)");
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Clear all data (danger!)");
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.Accounts
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Open");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.FolderMessageList
        solo.clickOnMenuItem("Open");
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.setup.AccountSettings
        solo.clickOnMenuItem("Account settings");
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        solo.clickOnMenuItem("Remove account");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        solo.clickOnMenuItem("Remove account");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        final View v_1 = solo.getView(0x102001a);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        solo.clickOnMenuItem("Remove account");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => android.app.AlertDialog
        solo.clickOnMenuItem("Remove account");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Check mail");
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.clickOnMenuItem("Compose");
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.Accounts
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.MessageCompose
        solo.clickOnMenuItem("Compose");
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnMenuItem("Add account");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnMenuItem("Add account");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.AlertDialog
        solo.clickOnMenuItem("About");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.AlertDialog
        solo.clickOnMenuItem("About");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 3
        // Priority: 0
        // com.android.email.activity.Accounts => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.app.AlertDialog
        solo.clickOnMenuItem("About");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.Accounts
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
