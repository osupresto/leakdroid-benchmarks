#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript K9 com.fsck.k9 com.fsck.k9.test MainTestCase testAccountSetupBasics,testAccountSetupAccountType,testAccountSetupIncoming,testAccountSetupComposition,testAccountSetupOutgoing,testAccountSetupOptions,testAccountSetupNames,testChooseFolder,testAccountSettings,testFolderSettings,testAccounts,testFolderMessageList,testMessageView,testMessageCompose rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript K9 com.fsck.k9 com.fsck.k9.test MainTestCase testAccountSetupBasics,testAccountSetupAccountType,testAccountSetupIncoming,testAccountSetupComposition,testAccountSetupOutgoing,testAccountSetupOptions,testAccountSetupNames,testChooseFolder,testAccountSettings,testFolderSettings,testAccounts,testFolderMessageList,testMessageView,testMessageCompose home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript K9 com.fsck.k9 com.fsck.k9.test MainTestCase testAccountSetupBasics,testAccountSetupAccountType,testAccountSetupIncoming,testAccountSetupComposition,testAccountSetupOutgoing,testAccountSetupOptions,testAccountSetupNames,testChooseFolder,testAccountSettings,testFolderSettings,testAccounts,testFolderMessageList,testMessageView,testMessageCompose power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript K9 com.fsck.k9 com.fsck.k9.test BackButtonTestCase testAccountSetupBasics,testAccountSetupAccountType,testAccountSetupIncoming,testAccountSetupComposition,testAccountSetupOutgoing,testAccountSetupOptions,testAccountSetupNames,testChooseFolder,testAccountSettings,testFolderSettings,testAccounts,testFolderMessageList,testMessageView,testMessageCompose,testAccountSetupChecking,testAccountsIncomingSettings,testAccountsOutgoingSettings,testAccountsMessageCompose back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript K9 com.fsck.k9 com.fsck.k9.test SemanticTestCase testMessageComposeDiscard,testAccountsMessageComposeDiscard,testReverseSort,testChangeSortType,testListFolders,testEmptyTrash,testCompact,testCheckMail,testMarkAllAsRead,testAccountListAndAccount,testAddRemoveAccount semantic.$N.action

