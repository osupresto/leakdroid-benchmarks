/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestAccountSettings extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestAccountSettings() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => android.view.ContextMenu
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertMenu();
        // android.view.ContextMenu => com.android.email.activity.setup.AccountSettings
        solo.clickOnMenuItem("Account settings");
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.android.email.activity.setup.AccountSettings
        solo.clickOnMenuItem("Account settings");
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
}
