/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import com.fsck.k9.R;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength2 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestGlobalLength2() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    assertActivity(com.android.email.activity.Accounts.class);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Accounts, of length 2
        // Priority: 0
        // com.android.email.activity.Accounts => com.android.email.activity.Debug
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Debug.class);
        // com.android.email.activity.Debug => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickLongInList(3);
    solo.clickOnText("Move");
    assertActivity(com.android.email.activity.ChooseFolder.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.ChooseFolder, of length 2
        // Priority: 0
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.sendKey(KeyEvent.KEYCODE_D);
    solo.sendKey(KeyEvent.KEYCODE_E);
    solo.sendKey(KeyEvent.KEYCODE_B);
    solo.sendKey(KeyEvent.KEYCODE_U);
    solo.sendKey(KeyEvent.KEYCODE_G);
    assertActivity(com.android.email.activity.Debug.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.Debug, of length 2
        // Priority: 0
        // com.android.email.activity.Debug => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.Debug
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Debug.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.Accounts
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.FolderMessageList
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.FolderMessageList, of length 2
        // Priority: 0
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_C);
    assertActivity(com.android.email.activity.MessageCompose.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.MessageCompose, of length 2
        // Priority: 0
        // com.android.email.activity.MessageCompose => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_C);
    assertActivity(com.android.email.activity.MessageCompose.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.MessageCompose, of length 2
        // Priority: 0
        // com.android.email.activity.MessageCompose => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.MessageCompose
        solo.clickOnButton("Reply");
        assertActivity(com.android.email.activity.MessageCompose.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_C);
    assertActivity(com.android.email.activity.MessageCompose.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.MessageCompose, of length 2
        // Priority: 0
        // com.android.email.activity.MessageCompose => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
        // com.android.email.activity.MessageView => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickInList(2);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.MessageView, of length 2
        // Priority: 0
        // com.android.email.activity.MessageView => com.android.email.activity.MessageCompose
        solo.clickOnButton("Reply");
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickInList(2);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.MessageView, of length 2
        // Priority: 0
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickInList(2);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.MessageView, of length 2
        // Priority: 0
        // com.android.email.activity.MessageView => com.android.email.activity.ChooseFolder
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.MessageView
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.MessageView.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.clickInList(2);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.MessageView, of length 2
        // Priority: 0
        // com.android.email.activity.MessageView => com.android.email.activity.MessageCompose
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.MessageCompose.class);
        // com.android.email.activity.MessageCompose => com.android.email.activity.MessageView
        solo.goBack();
        assertActivity(com.android.email.activity.MessageView.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    solo.clickOnButton("Manual setup");
    assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupAccountType, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("POP3 account");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupAccountType
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    solo.clickOnButton("Manual setup");
    assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupAccountType, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("POP3 account");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupAccountType
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupBasics, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupBasics, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupBasics, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        solo.goBack();
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupBasics, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.Accounts
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.Accounts.class);
        // com.android.email.activity.Accounts => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupBasics, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupNames
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupNames.class);
        // com.android.email.activity.setup.AccountSetupNames => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.ChooseFolder
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.ChooseFolder.class);
        // com.android.email.activity.ChooseFolder => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupAccountType
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("POP3 account");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupAccountType
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupAccountType.class);
        // com.android.email.activity.setup.AccountSetupAccountType => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("POP3 account");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, this.email);
    solo.typeText(1, this.password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("POP3 account");
    assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupIncoming, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong: hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    assertActivity(com.android.email.activity.setup.AccountSetupNames.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupNames, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupNames => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupNames
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupNames.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, "foo@bar.com");
    solo.typeText(1, "foobar");
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");
    solo.clickOnButton("Next");
    solo.clickOnButton("Continue");  
    assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOptions, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clearEditText(0);
    solo.typeText(0, email);
    solo.clearEditText(1);
    solo.typeText(1, password);
    solo.clearEditText(2);
    solo.typeText(2, imap);
    solo.clickOnText("None");
    solo.clickOnText("SSL \\(always\\)");
    solo.clickOnButton("Next");
    assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOutgoing, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clearEditText(0);
    solo.typeText(0, email);
    solo.clearEditText(1);
    solo.typeText(1, password);
    solo.clearEditText(2);
    solo.typeText(2, imap);
    solo.clickOnText("None");
    solo.clickOnText("SSL \\(always\\)");
    solo.clickOnButton("Next");
    assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOutgoing, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clearEditText(0);
    solo.typeText(0, email);
    solo.clearEditText(1);
    solo.typeText(1, password);
    solo.clearEditText(2);
    solo.typeText(2, imap);
    solo.clickOnText("None");
    solo.clickOnText("SSL \\(always\\)");
    solo.clickOnButton("Next");
    assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOutgoing, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clearEditText(0);
    solo.typeText(0, email);
    solo.clearEditText(1);
    solo.typeText(1, password);
    solo.clearEditText(2);
    solo.typeText(2, imap);
    solo.clickOnText("None");
    solo.clickOnText("SSL \\(always\\)");
    solo.clickOnButton("Next");
    assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOutgoing, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_Q);
    solo.clickOnMenuItem("Add account");
    solo.typeText(0, email);
    solo.typeText(1, password);
    solo.clickOnButton("Manual setup");
    solo.clickOnButton("IMAP account");
    solo.clearEditText(0);
    solo.typeText(0, email);
    solo.clearEditText(1);
    solo.typeText(1, password);
    solo.clearEditText(2);
    solo.typeText(2, imap);
    solo.clickOnText("None");
    solo.clickOnText("SSL \\(always\\)");
    solo.clickOnButton("Next");
    assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupOutgoing, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOptions
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOptions.class);
        // com.android.email.activity.setup.AccountSetupOptions => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
