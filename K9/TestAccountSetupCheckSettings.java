/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import com.fsck.k9.R;
import android.view.View;
import android.content.Intent;
import android.app.Activity;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestAccountSetupCheckSettings extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestAccountSetupCheckSettings() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => android.app.AlertDialog
        solo.clickOnButton("Next");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.setup.AccountSetupCheckSettings
        final View v_4 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupBasics
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => android.app.AlertDialog
        solo.clickOnButton("Next");
        assertDialog();
        // android.app.AlertDialog => com.android.email.activity.setup.AccountSetupCheckSettings
        final View v_1 = solo.getView(0x1020019);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupBasics
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupBasics.class);
        // com.android.email.activity.setup.AccountSetupBasics => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_4 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_4 = solo.getView(R.id.account_imap_folder_sent);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_4 = solo.getView(R.id.account_imap_folder_trash);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_4 = solo.getView(R.id.account_imap_folder_outbox);
        assertTrue("Button: Not Enabled", v_4.isEnabled());
        solo.clickOnView(v_4); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        int SPINNER_IDX = 0; // MAKE SURE IT'S THE BAR EXPECTED
        int SPINNER_ITEM_IDX = 1; // MAKE SURE IT'S THE VALUE EXPECTED
        solo.pressSpinnerItem(SPINNER_IDX, SPINNER_ITEM_IDX);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_1 = solo.getView(R.id.account_imap_folder_drafts);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_1 = solo.getView(R.id.account_imap_folder_sent);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_1 = solo.getView(R.id.account_imap_folder_trash);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        final View v_1 = solo.getView(R.id.account_imap_folder_outbox);
        assertTrue("Button: Not Enabled", v_1.isEnabled());
        solo.clickOnView(v_1); 
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        // TODO
        int SPINNER_IDX = 0; // MAKE SURE IT'S THE BAR EXPECTED
        int SPINNER_ITEM_IDX = 1; // MAKE SURE IT'S THE VALUE EXPECTED
        solo.pressSpinnerItem(SPINNER_IDX, SPINNER_ITEM_IDX);
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupIncoming
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupIncoming
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupIncoming.class);
        // com.android.email.activity.setup.AccountSetupIncoming => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        int SPINNER_IDX = 0; // MAKE SURE IT'S THE BAR EXPECTED
        int SPINNER_ITEM_IDX = 1; // MAKE SURE IT'S THE VALUE EXPECTED
        solo.pressSpinnerItem(SPINNER_IDX, SPINNER_ITEM_IDX);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        solo.clickOnView(solo.getView(R.id.account_require_login));
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Cancel");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        solo.enterText(0, "some text"); // MAKE SURE IT INDEXES THE CORRECT TEXT EDIT
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        // TODO
        int SPINNER_IDX = 0; // MAKE SURE IT'S THE BAR EXPECTED
        int SPINNER_ITEM_IDX = 1; // MAKE SURE IT'S THE VALUE EXPECTED
        solo.pressSpinnerItem(SPINNER_IDX, SPINNER_ITEM_IDX);
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        solo.clickOnView(solo.getView(R.id.account_require_login));
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupCheckSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupCheckSettings => com.android.email.activity.setup.AccountSetupOutgoing
        solo.goBack();
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupOutgoing
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupOutgoing.class);
        // com.android.email.activity.setup.AccountSetupOutgoing => com.android.email.activity.setup.AccountSetupCheckSettings
        solo.clickOnButton("Next");
        assertActivity(com.android.email.activity.setup.AccountSetupCheckSettings.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
