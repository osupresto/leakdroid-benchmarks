/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestAccountSetupComposition extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestAccountSetupComposition() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_S);
    solo.clickOnText("Composing messages");
    assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupComposition, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupComposition => com.android.email.activity.setup.AccountSetupComposition
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_S);
    solo.clickOnText("Composing messages");
    assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupComposition, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupComposition => com.android.email.activity.setup.AccountSetupComposition
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_S);
    solo.clickOnText("Composing messages");
    assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupComposition, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupComposition => com.android.email.activity.setup.AccountSetupComposition
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sendKey(KeyEvent.KEYCODE_S);
    solo.clickOnText("Composing messages");
    assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSetupComposition, of length 1
        // Priority: 0
        // com.android.email.activity.setup.AccountSetupComposition => com.android.email.activity.setup.AccountSetupComposition
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSetupComposition.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
