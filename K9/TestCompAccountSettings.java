/*
 * This file is automatically created by Gator.
 */

package com.fsck.k9.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompAccountSettings extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String email = "presto.test@yahoo.com";
  private final String password = "osupresto";
  private final String stmp = "stmp.mail.yahoo.com";
  private final String pop = "pop.mail.yahoo.com";
  private final String imap = "imap.mail.yahoo.com";
  public TestCompAccountSettings() {
    super("com.fsck.k9", com.android.email.activity.Welcome.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 2
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.FolderMessageList
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.FolderMessageList
        solo.goBack();
        assertActivity(com.android.email.activity.FolderMessageList.class);
        // com.android.email.activity.FolderMessageList => com.android.email.activity.setup.AccountSettings
        // TODO
        solo.sendKey(KeyEvent.KEYCODE_SEARCH); // See http://developer.android.com/reference/android/view/KeyEvent.html
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded
    solo.sleep(3000);
    solo.sendKey(KeyEvent.KEYCODE_S);
    assertActivity(com.android.email.activity.setup.AccountSettings.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.email.activity.setup.AccountSettings, of length 3
        // Priority: 0
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        Util.rotateOnce(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
        // com.android.email.activity.setup.AccountSettings => com.android.email.activity.setup.AccountSettings
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.email.activity.setup.AccountSettings.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

}
