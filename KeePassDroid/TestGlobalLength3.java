/*
 * This file is automatically created by Gator.
 */

package com.android.keepass.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.content.Intent;
import com.android.keepass.R;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password ="1";
  private final String group = "Internet";
  private final String eName = "gmail";
  private final String eUserName = "android.presto@gmail.com";
  private final String eUrl = "https://mail.google.com";
  private final String ePwd = "prestoosu";
  private final String eConfirmPwd = ePwd;
  private final String eComment = "test account for gmail";
  public TestGlobalLength3() {
    super("com.android.keepass", com.android.keepass.KeePass.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 2
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 2
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 2
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 2
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Util.rotateOnce(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Util.rotateOnce(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Util.rotateOnce(solo);
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Util.rotateOnce(solo);
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.GroupActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.EntryEditActivity
        // Dialog dismiss
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00203() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.EntryEditActivity
        // Dialog dismiss
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00204() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.PasswordActivity
        // Dialog dismiss
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00205() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.fileselect.FileSelectActivity
        // Dialog dismiss
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00206() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.search.SearchResults
        // Dialog dismiss
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00207() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00208() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.PasswordActivity
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00209() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00210() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.search.SearchResults
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00211() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00212() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00213() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00214() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00215() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00216() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00217() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00218() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00219() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00220() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00221() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00222() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00223() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00224() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00225() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00226() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00227() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00228() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00229() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Ok");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00230() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00231() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00232() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00233() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00234() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00235() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00236() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00237() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00238() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00239() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00240() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Create");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00241() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00242() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00243() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00244() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00245() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00246() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00247() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00248() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00249() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00250() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00251() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00252() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00253() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00254() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00255() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00256() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00257() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00258() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00259() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00260() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00261() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00262() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00263() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00264() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00265() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00266() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00267() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00268() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00269() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00270() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00271() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00272() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00273() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00274() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00275() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00276() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00277() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.GroupActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00278() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00279() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00280() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00281() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00282() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00283() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00284() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00285() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00286() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00287() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00288() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00289() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00290() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00291() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00292() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00293() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00294() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00295() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00296() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00297() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00298() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00299() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00300() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00301() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 3
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00302() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00303() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.PasswordActivity
        // Dialog dismiss
      }
    });
  }

  public void testNeutralCycle00304() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.PasswordActivity
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00305() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00306() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00307() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00308() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00309() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00310() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00311() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00312() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00313() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00314() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00315() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00316() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00317() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00318() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00319() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00320() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00321() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Ok");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00322() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Ok");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00323() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Ok");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00324() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00325() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00326() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00327() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00328() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00329() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00330() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00331() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00332() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00333() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00334() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00335() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00336() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00337() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00338() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00339() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00340() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00341() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00342() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00343() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00344() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00345() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00346() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00347() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00348() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00349() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00350() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00351() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00352() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00353() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00354() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00355() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00356() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00357() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00358() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00359() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00360() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00361() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00362() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00363() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00364() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00365() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00366() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00367() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00368() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00369() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00370() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00371() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00372() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00373() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00374() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00375() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00376() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00377() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00378() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00379() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00380() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00381() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00382() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00383() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00384() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00385() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00386() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00387() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00388() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00389() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00390() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00391() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00392() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00393() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 3
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00394() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00395() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.fileselect.FileSelectActivity
        // Dialog dismiss
      }
    });
  }

  public void testNeutralCycle00396() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00397() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00398() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00399() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00400() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00401() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00402() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00403() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00404() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00405() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00406() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00407() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00408() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00409() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00410() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Ok");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00411() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00412() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00413() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00414() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00415() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00416() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00417() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00418() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00419() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00420() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00421() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00422() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00423() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00424() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00425() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00426() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00427() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00428() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00429() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00430() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00431() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00432() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00433() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Create");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00434() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00435() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00436() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00437() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00438() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00439() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00440() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00441() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00442() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00443() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00444() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00445() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00446() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00447() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00448() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00449() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00450() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00451() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00452() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00453() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00454() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00455() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00456() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00457() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00458() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00459() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00460() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00461() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00462() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Ok");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00463() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00464() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00465() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00466() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00467() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00468() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00469() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00470() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00471() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00472() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00473() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00474() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00475() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00476() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00477() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00478() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00479() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00480() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00481() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00482() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00483() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00484() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.PasswordActivity
        solo.clickOnView(solo.getView(R.id.show_password));
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00485() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00486() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00487() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00488() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00489() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 3
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.fileselect.FileSelectActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00490() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00491() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00492() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00493() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00494() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00495() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00496() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00497() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00498() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00499() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00500() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00501() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00502() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00503() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00504() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00505() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00506() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00507() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00508() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupCreateDialog
        solo.clickOnButton("Add group");
        assertDialog();
        // com.keepassdroid.GroupCreateDialog => com.keepassdroid.search.SearchResults
        // Dialog dismiss
      }
    });
  }

  public void testNeutralCycle00509() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.search.SearchResults
        solo.clickOnMenuItem("Lock Database");
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00510() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00511() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00512() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00513() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00514() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00515() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00516() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00517() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00518() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // TODO
        solo.clickLongInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00519() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00520() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 3
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.search.SearchResults
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00521() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00522() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00523() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00524() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
  public void addEntryIfNotExist() {
    if (!solo.searchText(eName)) {
      solo.clickOnButton("Add entry");    
      solo.enterText(0, eName);
      solo.enterText(1, eUserName);
      solo.enterText(2, eUrl);
      solo.enterText(3, ePwd);
      solo.enterText(4, eConfirmPwd);
      solo.enterText(5, eComment);
      solo.clickOnButton("Save");
      solo.assertCurrentActivity(getName(), "GroupActivity");
    }
  }
}
