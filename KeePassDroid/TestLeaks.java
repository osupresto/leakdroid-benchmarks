package com.android.keepass.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.widget.TextView;
import android.view.KeyEvent;
import android.view.View;
import android.content.Intent;
import com.android.keepass.R;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestNeutralCyclesLength3 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password ="1";
  private final String group = "Internet";
  private final String eName = "gmail";
  private final String eUserName = "android.presto@gmail.com";
  private final String eUrl = "https://mail.google.com";
  private final String ePwd = "prestoosu";
  private final String eConfirmPwd = ePwd;
  private final String eComment = "test account for gmail";
  public TestNeutralCyclesLength3() {
    super("com.android.keepass", com.android.keepass.KeePass.class);
  }

  public void testNeutralCycle00529() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00542() throws Exception {

    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(2000);    
    /*
    assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));
    */
  }

  public void addEntryIfNotExist() {
    if (!solo.searchText(eName)) {
      solo.clickOnButton("Add entry");    
      solo.enterText(0, eName);
      solo.enterText(1, eUserName);
      solo.enterText(2, eUrl);
      solo.enterText(3, ePwd);
      solo.enterText(4, eConfirmPwd);
      solo.enterText(5, eComment);
      solo.clickOnButton("Save");
      solo.assertCurrentActivity(getName(), "GroupActivity");
    }
  }
}