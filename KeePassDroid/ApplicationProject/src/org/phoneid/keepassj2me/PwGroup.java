/*
 * Copyright 2009 Brian Pellin.

This file was derived from

Copyright 2007 Naomaru Itoi <nao@phoneid.org>

This file was derived from 

Java clone of KeePass - A KeePass file viewer for Java
Copyright 2006 Bill Zwicky <billzwicky@users.sourceforge.net>

 *     
 * This file is part of KeePassDroid.
 *
 *  KeePassDroid is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  KeePassDroid is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with KeePassDroid.  If not, see <http://www.gnu.org/licenses/>.
 *
*/

package org.phoneid.keepassj2me;

import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Vector;

import com.keepassdroid.keepasslib.PwDate;


/**
 * @author Brian Pellin <bpellin@gmail.com>
 * @author Naomaru Itoi <nao@phoneid.org>
 * @author Bill Zwicky <wrzwicky@pobox.com>
 * @author Dominik Reichl <dominik.reichl@t-online.de>
 */
public class PwGroup {
  public PwGroup() {
  }

	public String toString() {
		return name;
	}

	public static final Date NEVER_EXPIRE = PwEntry.NEVER_EXPIRE;
	
	/** Size of byte buffer needed to hold this struct. */
	public static final int BUF_SIZE = 124;

	// for tree traversing
	public Vector<PwGroup> childGroups = null;
	public Vector<PwEntry> childEntries = null;
	public PwGroup parent = null;

	public int groupId;
	public int imageId;
	public String name;

	public PwDate tCreation;
	public PwDate tLastMod;
	public PwDate tLastAccess;
	public PwDate tExpire;

	public int level; // short

	/** Used by KeePass internally, don't use */
	public int flags;
	
	public void sortGroupsByName() {
		Collections.sort(childGroups, new GroupNameComparator());
	}

	public void sortEntriesByName() {
		Collections.sort(childEntries, new EntryNameComparator());
	}
	
	private class GroupNameComparator implements Comparator<PwGroup> {

		@Override
		public int compare(PwGroup object1, PwGroup object2) {
			return object1.name.compareToIgnoreCase(object2.name);
		}
		
	}

	private class EntryNameComparator implements Comparator<PwEntry> {

		@Override
		public int compare(PwEntry object1, PwEntry object2) {
			return object1.title.compareToIgnoreCase(object2.title);
		}
		
	}
}
