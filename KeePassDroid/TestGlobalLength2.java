/*
 * This file is automatically created by Gator.
 */

package com.android.keepass.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestGlobalLength2 extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password ="1";
  private final String group = "Internet";
  private final String eName = "gmail";
  private final String eUserName = "android.presto@gmail.com";
  private final String eUrl = "https://mail.google.com";
  private final String ePwd = "prestoosu";
  private final String eConfirmPwd = ePwd;
  private final String eComment = "test account for gmail";
  public TestGlobalLength2() {
    super("com.android.keepass", com.android.keepass.KeePass.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 2
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 2
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.GroupActivity, of length 2
        // Priority: 0
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    assertActivity(com.keepassdroid.PasswordActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.PasswordActivity, of length 2
        // Priority: 0
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Create");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        solo.clickOnButton("Open");
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.fileselect.FileSelectActivity, of length 2
        // Priority: 0
        // com.keepassdroid.fileselect.FileSelectActivity => com.keepassdroid.PasswordActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => com.keepassdroid.fileselect.FileSelectActivity
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 2
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.EntryActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnMenuItem("Search");
    solo.typeText(0, "234");
    solo.clickOnButton(0);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.search.SearchResults, of length 2
        // Priority: 0
        // com.keepassdroid.search.SearchResults => com.keepassdroid.GroupActivity
        // TODO
        solo.clickInList(1, 0); // MAKE SURE IT INDEXES THE ITEM AND LIST EXPECTED
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public void addEntryIfNotExist() {
    if (!solo.searchText(eName)) {
      solo.clickOnButton("Add entry");    
      solo.enterText(0, eName);
      solo.enterText(1, eUserName);
      solo.enterText(2, eUrl);
      solo.enterText(3, ePwd);
      solo.enterText(4, eConfirmPwd);
      solo.enterText(5, eComment);
      solo.clickOnButton("Save");
      solo.assertCurrentActivity(getName(), "GroupActivity");
    }
  }
}
