/*
 * This file is automatically created by Gator.
 */

package com.android.keepass.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestAppSettingsActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password ="123123";
  private final String group = "Internet";
  private final String eName = "gmail";
  private final String eUserName = "android.presto@gmail.com";
  private final String eUrl = "https://mail.google.com";
  private final String ePwd = "prestoosu";
  private final String eConfirmPwd = ePwd;
  private final String eComment = "test account for gmail";
  public TestAppSettingsActivity() {
    super("com.android.keepass", com.android.keepass.KeePass.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 1
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.settings.AppSettingsActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 1
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.settings.AppSettingsActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 1
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.settings.AppSettingsActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.PasswordActivity
        solo.goBack();
        assertActivity(com.keepassdroid.PasswordActivity.class);
        // com.keepassdroid.PasswordActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.fileselect.FileSelectActivity
        solo.goBack();
        assertActivity(com.keepassdroid.fileselect.FileSelectActivity.class);
        // com.keepassdroid.fileselect.FileSelectActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnMenuItem("Settings");
    assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.settings.AppSettingsActivity, of length 3
        // Priority: 0
        // com.keepassdroid.settings.AppSettingsActivity => com.keepassdroid.search.SearchResults
        solo.goBack();
        assertActivity(com.keepassdroid.search.SearchResults.class);
        // com.keepassdroid.search.SearchResults => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.settings.AppSettingsActivity
        solo.clickOnMenuItem("Settings");
        assertActivity(com.keepassdroid.settings.AppSettingsActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
  public void addEntryIfNotExist() {
    if (!solo.searchText(eName)) {
      solo.clickOnButton("Add entry");    
      solo.enterText(0, eName);
      solo.enterText(1, eUserName);
      solo.enterText(2, eUrl);
      solo.enterText(3, ePwd);
      solo.enterText(4, eConfirmPwd);
      solo.enterText(5, eComment);
      solo.clickOnButton("Save");
      solo.assertCurrentActivity(getName(), "GroupActivity");
    }
  }
}
