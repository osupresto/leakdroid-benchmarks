/*
 * This file is automatically created by Gator.
 */

package com.android.keepass.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.view.KeyEvent;
import android.widget.TextView;
import android.view.View;
import android.app.Activity;
import android.view.ViewGroup;
import java.util.ArrayList;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestCompEntryEditActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password ="123123";
  private final String group = "Internet";
  private final String eName = "gmail";
  private final String eUserName = "android.presto@gmail.com";
  private final String eUrl = "https://mail.google.com";
  private final String ePwd = "prestoosu";
  private final String eConfirmPwd = ePwd;
  private final String eComment = "test account for gmail";
  public TestCompEntryEditActivity() {
    super("com.android.keepass", com.android.keepass.KeePass.class);
  }

  public void testNeutralCycle00001() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00011() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00012() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00013() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00014() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00015() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00016() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00017() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00018() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 2
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00019() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00020() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00021() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00022() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00023() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00024() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00025() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00026() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00027() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00028() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00029() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00030() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00031() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00032() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00033() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00034() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00035() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00036() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00037() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00038() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00039() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00040() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00041() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00042() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00043() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00044() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00045() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00046() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00047() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00048() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00049() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00050() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00051() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00052() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00053() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00054() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00055() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00056() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00057() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00058() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00059() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00060() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00061() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00062() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00063() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00064() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00065() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00066() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00067() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00068() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00069() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00070() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00071() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00072() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00073() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00074() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00075() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00076() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00077() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00078() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00079() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00080() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00081() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00082() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00083() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00084() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00085() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00086() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00087() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00088() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00089() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00090() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00091() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00092() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00093() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00094() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00095() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00096() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00097() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00098() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00099() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00100() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00101() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00102() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00103() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00104() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00105() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00106() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00107() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00108() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00109() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00110() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00111() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00112() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00113() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00114() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00115() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00116() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryActivity.class);
        // com.keepassdroid.EntryActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Edit");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00117() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00118() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00119() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00120() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00121() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00122() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00123() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00124() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00125() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00126() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00127() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00128() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00129() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00130() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00131() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00132() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00133() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00134() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00135() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00136() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00137() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00138() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00139() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00140() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00141() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00142() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00143() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00144() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00145() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00146() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00147() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00148() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00149() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00150() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00151() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00152() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00153() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00154() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00155() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00156() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.goBack();
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Add entry");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00157() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00158() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00159() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00160() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00161() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00162() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.GroupActivity
        solo.clickOnButton("Save");
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.GroupActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.GroupActivity.class);
        // com.keepassdroid.GroupActivity => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00163() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00164() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00165() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00166() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00167() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00168() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00169() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00170() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00171() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00172() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00173() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00174() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00175() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00176() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00177() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00178() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press HOME button
        Util.homeAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00179() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Ok");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00180() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.clickOnButton("Cancel");
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00181() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00182() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.SetPasswordDialog
        solo.clickOnButton("Save");
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.SetPasswordDialog
        // Press POWER button
        Util.powerAndBack(solo);
        assertDialog();
        // com.keepassdroid.SetPasswordDialog => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00183() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00184() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00185() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00186() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00187() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00188() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00189() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00190() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00191() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00192() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00193() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00194() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00195() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00196() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00197() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
        // com.keepassdroid.EntryEditActivity => com.keepassdroid.EntryEditActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00198() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00199() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // TODO
        solo.clickOnMenuItem("some text");
        // solo.clickOnActionBarItem(resource_id);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00200() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        solo.goBack();
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00201() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }

  public void testNeutralCycle00202() throws Exception {
    // TODO(hailong): hard coded.
    solo.clickOnButton("Open");
    solo.typeText(0, password);
    solo.clickOnButton("Ok");
    solo.sleep(4000);
    solo.clickOnText("Internet");
    solo.clickInList(1);
    solo.clickOnButton("Edit");
    assertActivity(com.keepassdroid.EntryEditActivity.class);
    
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.EntryEditActivity, of length 3
        // Priority: 0
        // com.keepassdroid.EntryEditActivity => android.view.Menu
        solo.sendKey(KeyEvent.KEYCODE_MENU);
        assertMenu();
        // android.view.Menu => android.view.Menu
        Util.rotateOnce(solo);
        assertMenu();
        // android.view.Menu => com.keepassdroid.EntryEditActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.EntryEditActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert dialog
  @SuppressWarnings("unchecked")
  public void assertDialog() {
    solo.sleep(500);
    /*assertTrue("Dialog not open", solo.waitForDialogToOpen());
    Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", views.isEmpty());*/
  }

  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public View findViewByText(String text) {
    boolean shown = solo.waitForText(text);
    if (!shown) return null;
    ArrayList<View> views = solo.getCurrentViews();
    for (View view : views) {
      if (view instanceof TextView) {
        TextView textView = (TextView) view;
        String textOnView = textView.getText().toString();
        if (textOnView.matches(text)) {
          Log.v(TAG, "Find View (By Text " + textOnView + "): " + view);
          return view;
        }
      }
    }
    return null;
  }

  // Assert menu
  @SuppressWarnings("unchecked")
  public void assertMenu() {
    solo.sleep(500);
    /*Class<? extends View> cls = null;
    try {
      cls = (Class<? extends View>) Class.forName("com.android.internal.view.menu.MenuView$ItemView");
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    }
    ArrayList<? extends View> views = solo.getCurrentViews(cls);
    assertTrue("Menu not open.", !views.isEmpty());*/
  }
  public void addEntryIfNotExist() {
    if (!solo.searchText(eName)) {
      solo.clickOnButton("Add entry");    
      solo.enterText(0, eName);
      solo.enterText(1, eUserName);
      solo.enterText(2, eUrl);
      solo.enterText(3, ePwd);
      solo.enterText(4, eConfirmPwd);
      solo.enterText(5, eComment);
      solo.clickOnButton("Save");
      solo.assertCurrentActivity(getName(), "GroupActivity");
    }
  }
}
