#!/bin/sh

ScriptDir=`dirname $0`
RunScript=$ScriptDir/../../tools/scripts/run.pl
N=5

if [ "$1" != "" ]; then
  N=$1
fi

# Trigger each activity, and rotate back'n'forth N times
$RunScript KeePassDroid com.android.keepass com.android.keepass.test MainTestCase testFileSelectActivity,testPasswordActivity,testGroupActivity,testEntryEditActivity,testEntryActivity,testSearchResults,testAppSettingsActivity rotate.$N.action

# Trigger each activity, and go to HOME and back for N times
$RunScript KeePassDroid com.android.keepass com.android.keepass.test MainTestCase testFileSelectActivity,testPasswordActivity,testGroupActivity,testEntryEditActivity,testEntryActivity,testSearchResults,testAppSettingsActivity home.$N.action

# Trigger each activity, and press POWER to dim the screen and get it back on for N times
$RunScript KeePassDroid com.android.keepass com.android.keepass.test MainTestCase testFileSelectActivity,testPasswordActivity,testGroupActivity,testEntryEditActivity,testEntryActivity,testSearchResults,testAppSettingsActivity power.$N.action

# Execute neutral cycles with BACK button transtions for N times
$RunScript KeePassDroid com.android.keepass com.android.keepass.test BackButtonTestCase testFileSelectActivity,testPasswordActivity,testGroupActivity,testEntryEditActivity,testEntryActivity,testSearchResults,testAppSettingsActivity,testOpenAndSettingBack,testGroupAndSettingBack,testSearchAndSearchBack,testSearchSettingBack,testGroupAndGroupBack,testEntryAndEditBack back.$N.action

# Execute neutral cycles with semantically neutralizing operations for N times
$RunScript KeePassDroid com.android.keepass com.android.keepass.test SemanticTestCase testLoginAndLock,testGroupChangePwd,testRepeatedSearch,testSearchAndChangePwd,testSearchAndLock,testEntryLock,testAddRemoveEntry,testAddRemoveGroup semantic.$N.action

