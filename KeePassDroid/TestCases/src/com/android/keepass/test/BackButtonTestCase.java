/* 
 * BackButtonTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.android.keepass.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.AmplifyUtils;
import pai.GenericFunctor;

public class BackButtonTestCase extends AmplifyTestCase {
	private static final String pwd = "1";
	private static final String group = "Internet";
	
	// sample entry
	private static final String eName = "gmail";
	private static final String eUserName = "android.presto@gmail.com";
	private static final String eUrl = "https://mail.google.com";
	private static final String ePwd = "connectbot";
	private static final String eConfirmPwd = ePwd;
	private static final String eComment = "test account for gmail";
	
	public BackButtonTestCase() {
		super("com.android.keepass", "com.android.keepass.KeePass");
	}
	
	// KeePass is a wrapper activity. The real thing here is FileSelectActivity
	// testKeePass()
	
	public void testFileSelectActivity() {
		solo.assertCurrentActivity(getName(), "KeePass");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				AmplifyUtils.leaveAndBack(solo);
				solo.assertCurrentActivity(getName(), "FileSelectActivity");		
			}			
		});
	}

	public void testPasswordActivity() {		
		solo.clickOnButton("Open");
		solo.assertCurrentActivity(getName(), "PasswordActivity");
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnButton("Open");
				solo.assertCurrentActivity(getName(), "PasswordActivity");
			}			
		});
	}
	
	public void testGroupActivity() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.assertCurrentActivity(getName(), "GroupActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.enterText(0, pwd);
				solo.clickOnButton("Ok");
				solo.assertCurrentActivity(getName(), "GroupActivity");
			}			
		});
	}

	public void testEntryEditActivity() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.clickOnText(group);
		solo.clickOnButton("Add entry");
		solo.assertCurrentActivity(getName(), "EntryEditActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnButton("Add entry");
				solo.assertCurrentActivity(getName(), "EntryEditActivity");
			}			
		});
	}
	
	public void testEntryActivity() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.clickOnText(group);
		
		addEntryIfNotExist();
		solo.clickOnText(eName);
		solo.assertCurrentActivity(getName(), "EntryActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				solo.clickOnText(eName);
				solo.assertCurrentActivity(getName(), "EntryActivity");
			}			
		});
	}
	
	// [Helper]
	private void addEntryIfNotExist() {
		if (!solo.searchText(eName)) {
			solo.clickOnButton("Add entry");		
			solo.enterText(0, eName);
			solo.enterText(1, eUserName);
			solo.enterText(2, eUrl);
			solo.enterText(3, ePwd);
			solo.enterText(4, eConfirmPwd);
			solo.enterText(5, eComment);
			solo.clickOnButton("Save");
			solo.assertCurrentActivity(getName(), "GroupActivity");
		}
	}
	
	// LockingActivity is a super class without GUI
	// testLockingActivity();
	
	public void testSearchResults() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.clickOnMenuItem("Search");
		solo.enterText(0, group);
		solo.sendKey(Solo.ENTER);
		solo.assertCurrentActivity(getName(), "SearchResults");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				
				solo.clickOnMenuItem("Search");
				solo.enterText(0, group);
				solo.sendKey(Solo.ENTER);
				solo.assertCurrentActivity(getName(), "SearchResults");
			}			
		});
	}
	
	public void testAppSettingsActivity() {
		solo.clickOnMenuItem("Settings");
		solo.assertCurrentActivity(getName(), "AppSettingsActivity");
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.goBack();
				
				solo.clickOnMenuItem("Settings");
				solo.assertCurrentActivity(getName(), "AppSettingsActivity");
			}			
		});
	}
	
	// open->setting/back
    public void testOpenAndSettingBack() {
        solo.clickOnButton("Open");
        specifyAmplifyFunctor(new GenericFunctor() {

            @Override
            public void doIt(Object arg) {
                solo.clickOnMenuItem("Settings");
                solo.goBack();
            }
        });
    }
    
    // group setting/back
    public void testGroupAndSettingBack() {
        solo.clickOnButton("Open");
        solo.enterText(0, pwd);
        solo.clickOnButton("Ok");
        solo.waitForIdleSync();
        specifyAmplifyFunctor(new GenericFunctor() {

            @Override
            public void doIt(Object arg) {
                solo.clickOnMenuItem("Settings");
                solo.goBack();
            }
        });
    }
    
    // search search/back
    public void testSearchAndSearchBack() {
        solo.clickOnButton("Open");
        solo.enterText(0, pwd);
        solo.clickOnButton("Ok");
        solo.waitForIdleSync();
        solo.clickOnMenuItem("Search");
        solo.enterText(0, group);
        solo.sendKey(Solo.ENTER);
        specifyAmplifyFunctor(new GenericFunctor() {

            @Override
            public void doIt(Object arg) {
                solo.clickOnMenuItem("Search");
                solo.enterText(0, group);
                solo.sendKey(Solo.ENTER);
                solo.goBack();
            }
        });
    }
    
    // search, setting/back
    public void testSearchSettingBack() {
        solo.clickOnButton("Open");
        solo.enterText(0, pwd);
        solo.clickOnButton("Ok");
        solo.waitForIdleSync();
        solo.clickOnMenuItem("Search");
        solo.enterText(0, group);
        solo.sendKey(Solo.ENTER);
        specifyAmplifyFunctor(new GenericFunctor() {

            @Override
            public void doIt(Object arg) {
                solo.clickOnMenuItem("Settings");
                solo.goBack();
            }
        });
    }

    // group, group/back
    public void testGroupAndGroupBack() {
        solo.clickOnButton("Open");
        solo.enterText(0, pwd);
        solo.clickOnButton("Ok");
        solo.waitForIdleSync();
        specifyAmplifyFunctor(new GenericFunctor() {

            @Override
            public void doIt(Object arg) {
                solo.clickOnText(group);
                solo.goBack();
            }
        });
    }
    
    // entry, edit/back
    public void testEntryAndEditBack() {
        solo.clickOnButton("Open");     
        solo.enterText(0, pwd);
        solo.clickOnButton("Ok");
        solo.clickOnText(group);
        
        addEntryIfNotExist();
        solo.clickOnText(eName);
        
        specifyAmplifyFunctor(new GenericFunctor() {

            @Override
            public void doIt(Object arg) {
                solo.clickOnButton("Edit");
                solo.goBack();
            }
        });
    }
}
