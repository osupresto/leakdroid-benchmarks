/* 
 * SemanticTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.android.keepass.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;
import pai.GenericFunctor;

public class SemanticTestCase extends AmplifyTestCase {
	private static final String pwd = "1";
	private static final String group = "Internet";
	
	// sample entry
	private static final String eName = "gmail";
	private static final String eUserName = "android.presto@gmail.com";
	private static final String eUrl = "https://mail.google.com";
	private static final String ePwd = "connectbot";
	private static final String eConfirmPwd = ePwd;
	private static final String eComment = "test account for gmail";
	
	public SemanticTestCase() {
		super("com.android.keepass", "com.android.keepass.KeePass");
	}
	
	// login/lock
	public void testLoginAndLock() {
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnButton("Open");
				solo.enterText(0, pwd);
				solo.clickOnButton("Ok");
				solo.waitForIdleSync();
				solo.clickOnMenuItem("Lock Database");
			}
		});
	}
	
	// group change pwd
	public void testGroupChangePwd() {
		solo.clickOnButton("Open");
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.waitForIdleSync();
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Change Master Key");
				solo.enterText(0, pwd);
				solo.enterText(1, pwd);
				solo.clickOnButton("Ok");
			}
		});
	}
	
	// search*
	public void testRepeatedSearch() {
		solo.clickOnButton("Open");
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.waitForIdleSync();
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Search");
				solo.enterText(0, group);
				solo.sendKey(Solo.ENTER);
			}
		});
	}

	// search, then change pwd
	public void testSearchAndChangePwd() {
		solo.clickOnButton("Open");
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.waitForIdleSync();
		solo.clickOnMenuItem("Search");
		solo.enterText(0, group);
		solo.sendKey(Solo.ENTER);
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Change Master Key");
				solo.enterText(0, pwd);
				solo.enterText(1, pwd);
				solo.clickOnButton("Ok");
			}
		});
		
	}

	// search, lock
	public void testSearchAndLock() {
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnButton("Open");
				solo.enterText(0, pwd);
				solo.clickOnButton("Ok");
				solo.waitForIdleSync();
				solo.clickOnMenuItem("Search");
				solo.enterText(0, group);
				solo.sendKey(Solo.ENTER);
				solo.clickOnMenuItem("Lock Database");
			}
		});
	}
	
	// entry/lock
	public void testEntryLock() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.clickOnText(group);
		
		addEntryIfNotExist();
		solo.clickOnText(eName);

		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickOnMenuItem("Lock Database");
				
				solo.clickOnButton("Open");		
				solo.enterText(0, pwd);
				solo.clickOnButton("Ok");
				solo.clickOnText(group);
				solo.clickOnText(eName);
			}
		});
	}

	// add/remove entry
	public void testAddRemoveEntry() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.sleep(2000);
		solo.clickOnText(group);
		
		addEntryIfNotExist();
		
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickLongInList(0);
				solo.clickOnText("Delete");
				

				solo.clickOnButton("Add entry");		
				solo.enterText(0, eName);
				solo.enterText(1, eUserName);
				solo.enterText(2, eUrl);
				solo.enterText(3, ePwd);
				solo.enterText(4, eConfirmPwd);
				solo.enterText(5, eComment);
				solo.clickOnButton("Save");
			}
		});
	}
	
	// add/remove group
	public void testAddRemoveGroup() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		addGroup(true);
		specifyAmplifyFunctor(new GenericFunctor() {

			@Override
			public void doIt(Object arg) {
				solo.clickLongOnText("abc");
				solo.clickOnText("Delete");
				addGroup(false);
			}
		});
	}

	public void addGroup(boolean search) {
		if (search && solo.searchText("abc")) {
			return;
		}
		solo.clickOnButton("Add group");
		solo.enterText(0, "abc");
		solo.clickOnButton("Ok");
	}
	public void addEntryIfNotExist() {
		if (!solo.searchText(eName)) {
			solo.clickOnButton("Add entry");		
			solo.enterText(0, eName);
			solo.enterText(1, eUserName);
			solo.enterText(2, eUrl);
			solo.enterText(3, ePwd);
			solo.enterText(4, eConfirmPwd);
			solo.enterText(5, eComment);
			solo.clickOnButton("Save");
			solo.assertCurrentActivity(getName(), "GroupActivity");
		}
	}
}
