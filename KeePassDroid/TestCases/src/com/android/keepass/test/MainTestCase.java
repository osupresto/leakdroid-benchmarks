/* 
 * MainTestCase.java - part of the LeakDroid project
 *
 * Copyright (c) 2013, The Ohio State University
 *
 * This file is distributed under the terms described in LICENSE in the root
 * directory.
 */

package com.android.keepass.test;

import com.jayway.android.robotium.solo.Solo;

import pai.AmplifyTestCase;

public class MainTestCase extends AmplifyTestCase {
	private static final String pwd = "1";
	private static final String group = "Internet";
	
	// sample entry
	private static final String eName = "gmail";
	private static final String eUserName = "android.presto@gmail.com";
	private static final String eUrl = "https://mail.google.com";
	private static final String ePwd = "connectbot";
	private static final String eConfirmPwd = ePwd;
	private static final String eComment = "test account for gmail";
	
	public MainTestCase() {
		super("com.android.keepass", "com.android.keepass.KeePass");
	}

	// KeePass is a wrapper activity. The real thing here is FileSelectActivity
	// testKeePass()	


	public void testFileSelectActivity() {
		solo.clickOnButton("Open");
		solo.goBack();
		solo.assertCurrentActivity(getName(), "FileSelectActivity");
	}
	
	public void testPasswordActivity() {		
		solo.clickOnButton("Open");
		solo.assertCurrentActivity(getName(), "PasswordActivity");		
	}
	
	public void testGroupActivity() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.assertCurrentActivity(getName(), "GroupActivity");
	}

	public void testEntryEditActivity() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.clickOnText(group);
		solo.clickOnButton("Add entry");
		solo.assertCurrentActivity(getName(), "EntryEditActivity");
	}
	
	public void testEntryActivity() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.clickOnText(group);
		
		addEntryIfNotExist();
		solo.clickOnText(eName);
		solo.assertCurrentActivity(getName(), "EntryActivity");
	}
	
	public void addEntryIfNotExist() {
		if (!solo.searchText(eName)) {
			solo.clickOnButton("Add entry");		
			solo.enterText(0, eName);
			solo.enterText(1, eUserName);
			solo.enterText(2, eUrl);
			solo.enterText(3, ePwd);
			solo.enterText(4, eConfirmPwd);
			solo.enterText(5, eComment);
			solo.clickOnButton("Save");
			solo.assertCurrentActivity(getName(), "GroupActivity");
		}
	}
	
	// LockingActivity is a super class without GUI
	// testLockingActivity();
	
	// LockCloseActivity same as above
	
	public void testSearchResults() {
		solo.clickOnButton("Open");		
		solo.enterText(0, pwd);
		solo.clickOnButton("Ok");
		solo.waitForActivity("GroupActivity");
		solo.clickOnMenuItem("Search");
		solo.enterText(0, group);
		solo.sendKey(Solo.ENTER);
		solo.assertCurrentActivity(getName(), "SearchResults");
	}
	
	public void testAppSettingsActivity() {
		solo.clickOnMenuItem("Settings");
		solo.assertCurrentActivity(getName(), "AppSettingsActivity");
	}	
}
