/*
 * This file is automatically created by Gator.
 */

package com.android.keepass.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.content.Intent;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestKeePass extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password ="1";
  private final String group = "Internet";
  private final String eName = "gmail";
  private final String eUserName = "android.presto@gmail.com";
  private final String eUrl = "https://mail.google.com";
  private final String ePwd = "prestoosu";
  private final String eConfirmPwd = ePwd;
  private final String eComment = "test account for gmail";
  public TestKeePass() {
    super("com.android.keepass", com.android.keepass.KeePass.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 1
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 1
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 1
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00004() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 1
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Util.rotateOnce(solo);
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00005() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 2
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00006() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 2
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00007() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00008() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00009() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        // exit the application
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_4 = solo.getCurrentActivity();
        final Intent intent_5 = new Intent();
        // TODO
        // intent_5.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_5.setAction(...);
        // intent_5.setData(...);
        // intent_5.setType(...);
        // intent_5.setFlags(...);
        int ReqCode_6 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_4.startActivityForResult(intent_5, ReqCode_6);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_4 = solo.getCurrentActivity();
        act_4.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }

  public void testNeutralCycle00010() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.android.keepass.KeePass, of length 3
        // Priority: 0
        // com.android.keepass.KeePass => presto.android.gui.stubs.PrestoFakeLauncherNodeClass
        // Leave the app and go back
        Util.leaveAndBack(solo);
        // presto.android.gui.stubs.PrestoFakeLauncherNodeClass => com.android.keepass.KeePass
        // Implicit Launch. BenchmarkName: KeePassDroid
        assertActivity(com.android.keepass.KeePass.class);
        // com.android.keepass.KeePass => com.android.keepass.KeePass
        Activity act_1 = solo.getCurrentActivity();
        final Intent intent_2 = new Intent();
        // TODO
        // intent_2.setClass(content, class); // MAKE SURE THIS IS CORRECT
        // MAKE SURE THIS IS THE INTENT EXPECTED
        // intent_2.setAction(...);
        // intent_2.setData(...);
        // intent_2.setType(...);
        // intent_2.setFlags(...);
        int ReqCode_3 = 1; // MAKE SURE IT IS THE REQUEST CODE WANTED
        act_1.startActivityForResult(intent_2, ReqCode_3);
        solo.sleep(5000);
        // assertTrue("Activity not match", solo.waitForActivity(class)); // wait for activity to start
        act_1 = solo.getCurrentActivity();
        act_1.finish(); // finish the activity
        assertActivity(com.android.keepass.KeePass.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public void addEntryIfNotExist() {
    if (!solo.searchText(eName)) {
      solo.clickOnButton("Add entry");    
      solo.enterText(0, eName);
      solo.enterText(1, eUserName);
      solo.enterText(2, eUrl);
      solo.enterText(3, ePwd);
      solo.enterText(4, eConfirmPwd);
      solo.enterText(5, eComment);
      solo.clickOnButton("Save");
      solo.assertCurrentActivity(getName(), "GroupActivity");
    }
  }
}
