/*
 * This file is automatically created by Gator.
 */

package com.android.keepass.tests;
import android.util.Log;
import com.robotium.solo.Solo;
import android.app.Activity;
import edu.osu.cse.presto.pai.AmplifyTestCase;
import edu.osu.cse.presto.pai.GenericFunctor;
import edu.osu.cse.presto.pai.Util;

public class TestLockCloseActivity extends AmplifyTestCase {

  private final static String TAG = "Gator.TestGenClient";
  private final String password ="1";
  private final String group = "Internet";
  private final String eName = "gmail";
  private final String eUserName = "android.presto@gmail.com";
  private final String eUrl = "https://mail.google.com";
  private final String ePwd = "prestoosu";
  private final String eConfirmPwd = ePwd;
  private final String eComment = "test account for gmail";
  public TestLockCloseActivity() {
    super("com.android.keepass", com.android.keepass.KeePass.class);
  }

  public void testNeutralCycle00001() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.LockCloseActivity, of length 1
        // Priority: 0
        // com.keepassdroid.LockCloseActivity => com.keepassdroid.LockCloseActivity
        // Press HOME button
        Util.homeAndBack(solo);
        assertActivity(com.keepassdroid.LockCloseActivity.class);
      }
    });
  }

  public void testNeutralCycle00002() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.LockCloseActivity, of length 1
        // Priority: 0
        // com.keepassdroid.LockCloseActivity => com.keepassdroid.LockCloseActivity
        // Press POWER button
        Util.powerAndBack(solo);
        assertActivity(com.keepassdroid.LockCloseActivity.class);
      }
    });
  }

  public void testNeutralCycle00003() throws Exception {
    
    specifyAmplifyFunctor(new GenericFunctor() {
      @Override
      public void doIt(Object arg) {    
        // ===> com.keepassdroid.LockCloseActivity, of length 1
        // Priority: 0
        // com.keepassdroid.LockCloseActivity => com.keepassdroid.LockCloseActivity
        Util.rotateOnce(solo);
        assertActivity(com.keepassdroid.LockCloseActivity.class);
      }
    });
  }


  /*
   * ============================== Helpers ==============================
   */
  // Assert activity
  public void assertActivity(Class<? extends Activity> cls) {
    solo.sleep(500);
    /*assertFalse("Dialog or Menu shows up.", solo.waitForDialogToOpen(2000));
    assertTrue("Activity does not match.", solo.waitForActivity(cls));*/
  }

  public void addEntryIfNotExist() {
    if (!solo.searchText(eName)) {
      solo.clickOnButton("Add entry");    
      solo.enterText(0, eName);
      solo.enterText(1, eUserName);
      solo.enterText(2, eUrl);
      solo.enterText(3, ePwd);
      solo.enterText(4, eConfirmPwd);
      solo.enterText(5, eComment);
      solo.clickOnButton("Save");
      solo.assertCurrentActivity(getName(), "GroupActivity");
    }
  }
}
